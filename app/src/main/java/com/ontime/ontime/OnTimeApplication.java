package com.ontime.ontime;

import android.app.Application;
import android.content.Context;
import android.content.res.Configuration;
import com.androidnetworking.AndroidNetworking;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.imagepipeline.core.ImagePipelineConfig;
import com.facebook.imagepipeline.decoder.SimpleProgressiveJpegConfig;
import com.marcinorlowski.fonty.Fonty;
import com.ontime.ontime.di.application.AppComponent;
import com.ontime.ontime.di.application.AppModule;
import com.ontime.ontime.di.application.DaggerAppComponent;
import com.ontime.ontime.di.scope.ApplicationScope;

public class OnTimeApplication extends Application {

    private final AppComponent appComponent = createAppComponent();
    private static Context context;

    public static Context getContext() {
        return context;
    }

    public static AppComponent getComponent(Context context) {
        return getApp(context).appComponent;
    }

    private static OnTimeApplication getApp(Context context) {
        return (OnTimeApplication) context.getApplicationContext();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        context = this;
        AndroidNetworking.initialize(getApplicationContext());
        Fonty
                .context(this)
                .regularTypeface("DIN NEXT™ ARABIC REGULAR_0.otf")
                .boldTypeface("DIN NEXT™ ARABIC BOLD_0.otf")
                .done();

        ImagePipelineConfig config = ImagePipelineConfig.newBuilder(this)
                .setProgressiveJpegConfig(new SimpleProgressiveJpegConfig())
                .setResizeAndRotateEnabledForNetwork(true)
                .setDownsampleEnabled(true)
                .build();
        Fresco.initialize(this, config);

    }

    private AppComponent createAppComponent() {
        return DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .build();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        Fonty
                .context(this)
                .regularTypeface("DIN NEXT™ ARABIC REGULAR_0.otf")
                .boldTypeface("DIN NEXT™ ARABIC BOLD_0.otf")
                .done();
    }
}
