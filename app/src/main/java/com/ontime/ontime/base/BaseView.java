package com.ontime.ontime.base;

public interface BaseView {

    void showErrorMessage(String msg);

    void showSuccessMessage(String msg);

    void showLoading();

    void hideLoading();

    void finishScreen();

}
