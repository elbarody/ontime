package com.ontime.ontime.store.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GetProfileResponse {

    @Expose
    @SerializedName("badges")
    private List<BadgesEntity> badges;
    @Expose
    @SerializedName("data")
    private DataEntity data;
    @Expose
    @SerializedName("msg")
    private String msg;
    @Expose
    @SerializedName("status")
    private int status;

    public List<BadgesEntity> getBadges() {
        return badges;
    }

    public DataEntity getData() {
        return data;
    }

    public String getMsg() {
        return msg;
    }

    public int getStatus() {
        return status;
    }
}
