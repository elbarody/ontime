package com.ontime.ontime.store.models.service;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class DataModel implements Serializable {
    @Expose
    @SerializedName("addons")
    private List<AddonModel> addons;
    @Expose
    @SerializedName("tax_percentage")
    private int taxPercentage;
    @Expose
    @SerializedName("average_time")
    private int averageTime;
    @Expose
    @SerializedName("has_time")
    private int hasTime;
    @Expose
    @SerializedName("has_contract")
    private int hasContract;
    @Expose
    @SerializedName("price")
    private int price;
    @Expose
    @SerializedName("has_price")
    private int hasPrice;
    @Expose
    @SerializedName("img")
    private String img;
    @Expose
    @SerializedName("name")
    private String name;
    @Expose
    @SerializedName("id")
    private int id;

    public List<AddonModel> getAddons() {
        return addons;
    }

    public int getTaxPercentage() {
        return taxPercentage;
    }

    public int getAverageTime() {
        return averageTime;
    }

    public int getHasTime() {
        return hasTime;
    }

    public int getHasContract() {
        return hasContract;
    }

    public int getPrice() {
        return price;
    }

    public int getHasPrice() {
        return hasPrice;
    }

    public String getImg() {
        return img;
    }

    public String getName() {
        return name;
    }

    public int getId() {
        return id;
    }
}
