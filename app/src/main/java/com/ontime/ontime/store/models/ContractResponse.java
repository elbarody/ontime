package com.ontime.ontime.store.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ContractResponse {

    @SerializedName("data")
    private ContractModel data;

    @SerializedName("msg")
    private String msg;

    @SerializedName("status")
    private int status;

    public ContractModel getData() {
        return data;
    }

    public String getMsg() {
        return msg;
    }

    public int getStatus() {
        return status;
    }
}

