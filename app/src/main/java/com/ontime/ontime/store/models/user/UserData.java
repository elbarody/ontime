package com.ontime.ontime.store.models.user;

import com.google.gson.annotations.SerializedName;

public class UserData {

    public String lang;
    @SerializedName("token")
    private String token;
    @SerializedName("name")
    private String name;
    @SerializedName("email")
    private String email;
    @SerializedName("phone")
    private String phone;
    @SerializedName("img")
    private String img;
    @SerializedName("user_type")
    private String userType;
    @SerializedName("company_name")
    private String companyName;

    public String userSesstion;

    public String getToken() {
        return token;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public String getPhone() {
        return phone;
    }

    public String getImg() {
        return img;
    }

    public String getUserType() {
        return userType;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }
}
