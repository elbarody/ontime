package com.ontime.ontime.store.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class WhoWeAreDataEntity {
    @Expose
    @SerializedName("ins")
    private String ins;
    @Expose
    @SerializedName("tw")
    private String tw;
    @Expose
    @SerializedName("fb")
    private String fb;
    @Expose
    @SerializedName("who_we_are")
    private String whoWeAre;
    @Expose
    @SerializedName("terms")
    private String terms;
    public String getIns() {
        return ins;
    }

    public String getTw() {
        return tw;
    }

    public String getFb() {
        return fb;
    }

    public String getWhoWeAre() {
        return whoWeAre;
    }

    public String getTerms() {
        return terms;
    }
}
