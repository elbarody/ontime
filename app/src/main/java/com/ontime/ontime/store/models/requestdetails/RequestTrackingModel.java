package com.ontime.ontime.store.models.requestdetails;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RequestTrackingModel {

    @Expose
    @SerializedName("date")
    private String date;
    @Expose
    @SerializedName("name_en")
    private String name_en;
    @Expose
    @SerializedName("name")
    private String name;
    @Expose
    @SerializedName("request_id")
    private int request_id;
    @Expose
    @SerializedName("id")
    private int id;

    public String getDate() {
        return date;
    }

    public String getName_en() {
        return name_en;
    }

    public String getName() {
        return name;
    }

    public int getRequest_id() {
        return request_id;
    }

    public int getId() {
        return id;
    }
}
