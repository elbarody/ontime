package com.ontime.ontime.store.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ContractModel {
    @Expose
    @SerializedName("text")
    private String text;
    @Expose
    @SerializedName("name")
    private String name;

    public String getText() {
        return text;
    }

    public String getName() {
        return name;
    }
}
