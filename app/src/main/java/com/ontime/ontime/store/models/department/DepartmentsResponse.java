package com.ontime.ontime.store.models.department;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DepartmentsResponse {

    @SerializedName("status")
    private int status;
    @SerializedName("msg")
    private String msg;
    @SerializedName("data")
    private List<DepartmentModel> departmentModels;

    public int getStatus() {
        return status;
    }

    public String getMsg() {
        return msg;
    }

    public List<DepartmentModel> getDepartmentModels() {
        return departmentModels;
    }
}
