package com.ontime.ontime.store.models.user;

import com.google.gson.annotations.SerializedName;

public class LoginResponse {

    @SerializedName("status")
    private int status;
    @SerializedName("msg")
    private String msg;

    @SerializedName("token")
    private String token;
    @SerializedName("data")
    private UserData data;

    public int getStatus() {
        return status;
    }

    public String getMsg() {
        return msg;
    }

    public UserData getUserData() {
        return data;
    }

    public String getToken() {
        return token;
    }
}
