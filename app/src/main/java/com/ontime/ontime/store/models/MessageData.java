package com.ontime.ontime.store.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MessageData {
    @Expose
    @SerializedName("read_at")
    private String read_at;
    @Expose
    @SerializedName("file_extension")
    private String file_extension;
    @Expose
    @SerializedName("file")
    private String file;
    @Expose
    @SerializedName("created_at")
    private String created_at;
    @Expose
    @SerializedName("model")
    private String model;
    @Expose
    @SerializedName("sender_id")
    private String sender_id;
    @Expose
    @SerializedName("msg_content")
    private String msg_content;
    @Expose
    @SerializedName("msg_type")
    private String msg_type;
    @Expose
    @SerializedName("request_id")
    private String request_id;
    @Expose
    @SerializedName("id")
    private String id;

    public String getRead_at() {
        return read_at;
    }

    public String getFile_extension() {
        return file_extension;
    }

    public String getFile() {
        return file;
    }

    public String getCreated_at() {
        return created_at;
    }

    public String getModel() {
        return model;
    }

    public String getSender_id() {
        return sender_id;
    }

    public String getMsg_content() {
        return msg_content;
    }

    public String getMsg_type() {
        return msg_type;
    }

    public String getRequest_id() {
        return request_id;
    }

    public String getId() {
        return id;
    }

    public void setRead_at(String read_at) {
        this.read_at = read_at;
    }

    public void setFile_extension(String file_extension) {
        this.file_extension = file_extension;
    }

    public void setFile(String file) {
        this.file = file;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public void setSender_id(String sender_id) {
        this.sender_id = sender_id;
    }

    public void setMsg_content(String msg_content) {
        this.msg_content = msg_content;
    }

    public void setMsg_type(String msg_type) {
        this.msg_type = msg_type;
    }

    public void setRequest_id(String request_id) {
        this.request_id = request_id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
