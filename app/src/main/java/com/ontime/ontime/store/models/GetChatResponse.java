package com.ontime.ontime.store.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GetChatResponse extends StatusResponse {


    @Expose
    @SerializedName("data")
    private List<MessageData> data;

    public List<MessageData> getData() {
        return data;
    }
}
