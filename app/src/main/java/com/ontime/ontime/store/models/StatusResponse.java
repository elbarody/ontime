package com.ontime.ontime.store.models;

public class StatusResponse {
    public String msg;
    public int status;

    public String getMsg() {
        return msg;
    }

    public int getStatus() {
        return status;
    }
}
