package com.ontime.ontime.store.network;

public class Endpoints {

    public static final String BASE_URL = "https://appontime.net/mobile/";
    public static final String REGISTER_COMPANY_URL = "register_company.php";
    public static final String REGISTER_USER_URL = "register_user.php";
    public static final String ADD_REQUEST_URL = "add_request.php";
    public static final String VCODE_URL = "send_vcode.php";
    public static final String LOGIN_URL = "login.php";
    public static final String REQUEST_URL = "get_user_requests.php";
    public static final String DEPARTMANTS_URL = "get_departments.php";
    public static final String SERVICES_URL = "get_service.php";
    public static final String FORGETPASS_URL = "forget_password.php";
    public static final String GET_REQUEST_CONTRACT = "get_request_contract.php";
    public static final String CONTRACT_ACTION_URL = "contract_action.php";
    public static final String CONTRACT_GET_CODE_URL = "send_contract_code.php";
    public static final String CONTRACT_CHECK_CODE_URL = "check_contract_code.php";
    public static final String RESEND_CODE_URL = "send_vcode.php";
    public static final String CHECK_CODE_URL = "check_vcode.php";
    public static final String CHECK_PCODE_URL = "check_pcode.php";
    public static final String ADD_PASSWORD_URL = "add_new_password.php";
    public static final String GET_PROFILE_DATA = "get_profile.php";
    public static final String UPDATE_PROFILE = "edit_profile.php";

    public static final String REQUEST_DETAILS_URL = "request_details.php";
    public static final String ADD_TOKEN = "add_token.php";
    public static final String WHO_WE_ARE_URL = "who_we_are.php";
    public static final String GET_TERMS = "get_terms.php";
    public static final String GET_USER_NOTIFICATION = "get_user_notification.php";
    public static final String READ_NOTIFICATION = "read_notification.php";
    public static final String GET_REQUEST_COMPONENT = "get_request_components.php";
    public static final String ACCEPT_REFUSE_COMPONENT = "accept_or_refuse_components.php";
    public static final String IMAGE_BASE_URL = "http://appontime.net/files/";

    public static final String GET_REQUEST_CHAT = "chat.php";
    public static final String SEND_MESSAGE_TEXT = "send_chat_msg.php";
    public static final String SEND_MESSAGE_FILE = "send_chat_file.php";
    public static final String GET_HELP_CHAT = "support.php";
    public static final String SEND_HELP_TEXT = "send_support_msg.php";
    public static final String SEND_HELP_FILE = "send_support_file.php";
}
