package com.ontime.ontime.store.repo;

import androidx.lifecycle.MutableLiveData;

import android.content.Context;

import com.google.firebase.iid.FirebaseInstanceId;
import com.jaiselrahman.filepicker.model.MediaFile;
import com.ontime.ontime.base.BaseView;
import com.ontime.ontime.di.qualifier.ForActivity;
import com.ontime.ontime.di.scope.ActivityScope;
import com.ontime.ontime.store.models.*;
import com.ontime.ontime.store.models.department.DepartmentModel;
import com.ontime.ontime.store.models.department.DepartmentsResponse;
import com.ontime.ontime.store.models.requestdetails.RequestDetailsData;
import com.ontime.ontime.store.models.requestdetails.RequestDetailsResponse;
import com.ontime.ontime.store.models.service.AddonModel;
import com.ontime.ontime.store.models.service.ServicesResponse;
import com.ontime.ontime.store.models.user.LoginResponse;
import com.ontime.ontime.store.models.user.RegisterResponse;
import com.ontime.ontime.store.models.user.UserData;
import com.ontime.ontime.store.models.user.VCodeResponse;
import com.ontime.ontime.store.network.Endpoints;
import com.ontime.ontime.store.network.NetworkManager;
import com.ontime.ontime.utils.SessionManager;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

import javax.inject.Inject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@ActivityScope
public class AppRepo {

    public MutableLiveData<Boolean> isActionSuccess = new MutableLiveData<>();
    public MutableLiveData<Boolean> isCodeSent = new MutableLiveData<>();
    public MutableLiveData<Boolean> isActivationSuccess = new MutableLiveData<>();
    public MutableLiveData<Boolean> forgetPasswordSuccess = new MutableLiveData<>();
    public MutableLiveData<Boolean> newPasswordSuccess = new MutableLiveData<>();
    public MutableLiveData<RequestDetailsData> requestDetailsLiveData = new MutableLiveData<>();
    public MutableLiveData<WhoAreYouResponse> whoAreYouResponseMutableLiveData = new MutableLiveData<>();

    public MutableLiveData<Boolean> isRequestSuccess = new MutableLiveData<>();
    public MutableLiveData<String> registerSuccess = new MutableLiveData<>();
    public MutableLiveData<RequestResponseModel> requestSuccess = new MutableLiveData<>();
    public MutableLiveData<GetNotificationResponseModel> notificationResponseModelMutableLiveData = new MutableLiveData<>();
    public MutableLiveData<List<DepartmentModel>> departmentsLiveData = new MutableLiveData<>();
    public MutableLiveData<ServicesResponse> servicesResponseLiveData = new MutableLiveData<>();
    public MutableLiveData<ContractResponse> getContractResponseLiveData = new MutableLiveData<>();
    public MutableLiveData<MakeRequestResponse> makeRequestResponseLiveData = new MutableLiveData<>();
    public MutableLiveData<ContractActionResponse> contractActionResponseMutableLiveData = new MutableLiveData<>();
    public MutableLiveData<GetProfileResponse> getProfileData = new MutableLiveData<>();
    public MutableLiveData<LoginResponse> loginLiveDataMutableLiveData = new MutableLiveData<>();
    public MutableLiveData<SendMessageResponse> sendMessageResponseMutableLiveData = new MutableLiveData<>();
    public MutableLiveData<GetComponentResponse> componentResponseMutableLiveData = new MutableLiveData<>();
    public MutableLiveData<GetChatResponse> chatResponseMutableLiveData = new MutableLiveData<>();


    private NetworkManager networkManager;
    private CompositeDisposable disposable;
    private BaseView view;
    private SessionManager sessionManager;
    private UserData user = new UserData();


    @Inject
    AppRepo(@ForActivity Context context) {
        this.view = (BaseView) context;
        this.disposable = new CompositeDisposable();
        this.networkManager = new NetworkManager();
        this.sessionManager = new SessionManager(context);

    }


    public void login(String phone, String pass) {
        HashMap<String, Object> params = new HashMap<>();
        params.put("phone", phone);
        params.put("password", pass);
        params.put("fcm_token", FirebaseInstanceId.getInstance().getToken());

        disposable.add(networkManager.postRequest(Endpoints.LOGIN_URL, params, LoginResponse.class)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(v -> view.showLoading())
                .doFinally(view::hideLoading)
                .subscribe(response -> {
                    if (response.getStatus() != 0 && response.getStatus() != 213) {
                        view.showSuccessMessage(response.getMsg());
                    } else {

                        loginLiveDataMutableLiveData.setValue(response);
                    }
                }, this::processError));
    }

//                .filter(loginResponse -> {
//                    if (loginResponse.getStatus() != 0) {
//                        view.showSuccessMessage(loginResponse.getName());
//                    } else {
//                        user.setToken(loginResponse.getUserData().getToken());
//                        sessionManager.createLoginSession(user);
//                        loginLiveDataMutableLiveData.setValue(loginResponse);
//                    }
//                    return loginResponse.getStatus() == 0;
//                })
//                .concatMap(response -> {
//                    HashMap<String, Object> params2 = new HashMap<>();
//                    params2.put("token", response.getUserData().getToken());
//                    params2.put("fcm_token", FirebaseInstanceId.getInstance().getToken());
//                    params2.put("device_type", "android");
//                    return networkManager.postRequest(Endpoints.ADD_TOKEN, params2, LoginResponse.class);
//                })

    public void getRequests(String type) {
        HashMap<String, Object> params = new HashMap<>();
        user = sessionManager.getUserDetails();
        params.put("type", type);
        params.put("token", user.getToken());
        disposable.add(networkManager.postRequest(Endpoints.REQUEST_URL, params, RequestResponseModel.class)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(v -> view.showLoading())
                .doFinally(view::hideLoading)
                .subscribe(response -> {

                    if (response.status != 0) {
                        view.showSuccessMessage(response.msg);
                    } else {
                        requestSuccess.setValue(response);
                    }
                }, this::processError));
    }

    public void getUserNotification() {
        HashMap<String, Object> params = new HashMap<>();
        user = sessionManager.getUserDetails();
        params.put("token", user.getToken());
        disposable.add(networkManager.postRequest(Endpoints.GET_USER_NOTIFICATION, params, GetNotificationResponseModel.class)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(v -> view.showLoading())
                .doFinally(view::hideLoading)
                .subscribe(response -> {

                    if (response.getStatus() != 0) {
                        view.showSuccessMessage(response.getMsg());
                    } else {
                        notificationResponseModelMutableLiveData.setValue(response);
                    }
                }, this::processError));
    }


    public void readNotification(String type, int notification_id) {
        HashMap<String, Object> params = new HashMap<>();
        user = sessionManager.getUserDetails();
        params.put("token", user.getToken());
        params.put("reading_type", type);
        params.put("notification_id", notification_id);
        disposable.add(networkManager.postRequest(Endpoints.READ_NOTIFICATION, params, StatusResponse.class)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(v -> view.showLoading())
                .doFinally(view::hideLoading)
                .subscribe(response -> {

                    if (response.getStatus() != 0) {
                        view.showSuccessMessage(response.getMsg());
                    } else {
                        isActionSuccess.setValue(true);
                        view.showSuccessMessage(response.getMsg());

                    }
                }, this::processError));
    }

    public void getAcceptOrRefuseComponent(String decision, int request_id) {
        HashMap<String, Object> params = new HashMap<>();
        user = sessionManager.getUserDetails();
        params.put("token", user.getToken());
        params.put("decision", decision);
        params.put("request_id", request_id);
        disposable.add(networkManager.postRequest(Endpoints.ACCEPT_REFUSE_COMPONENT, params, StatusResponse.class)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(v -> view.showLoading())
                .doFinally(view::hideLoading)
                .subscribe(response -> {

                    if (response.getStatus() != 0) {
                        view.showSuccessMessage(response.getMsg());
                    } else {
                        isActionSuccess.setValue(true);
                        view.showSuccessMessage(response.getMsg());

                    }
                }, this::processError));
    }

    public void getDepartments() {
        HashMap<String, Object> params = new HashMap<>();
        user = sessionManager.getUserDetails();
        params.put("token", user.getToken());
        disposable.add(networkManager.postRequest(Endpoints.DEPARTMANTS_URL, params, DepartmentsResponse.class)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(v -> view.showLoading())
                .doFinally(view::hideLoading)
                .subscribe(response -> {

                    if (response.getStatus() != 0) {
                        view.showSuccessMessage(response.getMsg());
                    } else {
                        departmentsLiveData.setValue(response.getDepartmentModels());
                    }
                }, this::processError));
    }

    public void getServices(int depId, int serviceId) {
        HashMap<String, Object> params = new HashMap<>();
        user = sessionManager.getUserDetails();
        params.put("token", user.getToken());
        params.put("department_id", depId);
        params.put("service_id", serviceId);
        disposable.add(networkManager.postRequest(Endpoints.SERVICES_URL, params, ServicesResponse.class)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(v -> view.showLoading())
                .doFinally(view::hideLoading)
                .subscribe(response -> {

                    if (response.getStatus() != 0) {
                        view.showSuccessMessage(response.getMsg());
                    } else {
                        servicesResponseLiveData.setValue(response);
                    }
                }, this::processError));
    }

    public void registerCompany(String companyName, String username, String email, String phone, String pass) {
        HashMap<String, Object> params = new HashMap<>();
        params.put("ability", "1");
        params.put("company_name", companyName);
        params.put("password", pass);
        params.put("phone", phone);
        params.put("email", email);
        params.put("name", username);
        disposable.add(networkManager.postRequest(Endpoints.REGISTER_COMPANY_URL, params, RegisterResponse.class)
                .observeOn(AndroidSchedulers.mainThread())
                .filter(registerResponse -> {
                    if (registerResponse.getStatus() != 0) {
                        view.showSuccessMessage(registerResponse.getMsg());
                        user.setToken(registerResponse.getToken());
                        sessionManager.createLoginSession(user);
                    }
                    return registerResponse.getStatus() == 0;
                })
                .concatMap(registerResponse -> {
                    HashMap<String, Object> params2 = new HashMap<>();
                    params2.put("token", registerResponse.getToken());
                    return networkManager.postRequest(Endpoints.VCODE_URL, params2, VCodeResponse.class);
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(v -> view.showLoading())
                .doFinally(view::hideLoading)
                .subscribe(response -> {
                    if (response.getStatus() != 0) {
                        view.showSuccessMessage(response.getMsg());
                    } else {
                        registerSuccess.setValue(response.getVcode());
                    }
                }, this::processError));
    }

    public void registerUser(String username, String email, String phone, String pass) {
        HashMap<String, Object> params = new HashMap<>();
        params.put("password", pass);
        params.put("phone", phone);
        params.put("email", email);
        params.put("name", username);
        disposable.add(networkManager.postRequest(Endpoints.REGISTER_USER_URL, params, RegisterResponse.class)
                .observeOn(AndroidSchedulers.mainThread())
                .filter(registerResponse -> {
                    if (registerResponse.getStatus() != 0) {
                        view.showSuccessMessage(registerResponse.getMsg());
                    }
                    return registerResponse.getStatus() == 0;
                })
                .concatMap(registerResponse -> {
                    HashMap<String, Object> params2 = new HashMap<>();
                    params2.put("token", registerResponse.getToken());
                    return networkManager.postRequest(Endpoints.VCODE_URL, params2, VCodeResponse.class);
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(v -> view.showLoading())
                .doFinally(view::hideLoading)
                .subscribe(response -> {
                    if (response.getStatus() != 0) {
                        view.showSuccessMessage(response.getMsg());
                    } else {
                        user.setToken(response.getToken());
                        sessionManager.createLoginSession(user);
                        registerSuccess.setValue(response.getVcode());
                    }
                }, this::processError));
    }

    public void forgetPassword(String email) {
        HashMap<String, Object> params = new HashMap<>();
        params.put("email", email);
        disposable.add(networkManager.postRequest(Endpoints.FORGETPASS_URL, params, ForgetPasswordResponse.class)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(v -> view.showLoading())
                .doFinally(view::hideLoading)
                .subscribe(response -> {
                    if (response.getStatus() != 0) {
                        view.showSuccessMessage(response.getMsg());
                    } else {
                        view.showSuccessMessage(response.getPcode());
                        forgetPasswordSuccess.setValue(true);


                    }
                }, this::processError));
    }

    public void getAboutUs() {
        disposable.add(networkManager.getRequest(Endpoints.WHO_WE_ARE_URL, null, WhoAreYouResponse.class)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(v -> view.showLoading())
                .doFinally(view::hideLoading)
                .subscribe(response -> {
                    if (response.getStatus() != 0) {
                        view.showSuccessMessage(response.getMsg());
                    } else {
                        view.showSuccessMessage(response.getMsg());
                        whoAreYouResponseMutableLiveData.setValue(response);

                    }
                }, this::processError));
    }

    public void getTerms() {
        disposable.add(networkManager.getRequest(Endpoints.GET_TERMS, null, WhoAreYouResponse.class)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(v -> view.showLoading())
                .doFinally(view::hideLoading)
                .subscribe(response -> {
                    if (response.getStatus() != 0) {
                        view.showSuccessMessage(response.getMsg());
                    } else {
                        view.showSuccessMessage(response.getMsg());
                        whoAreYouResponseMutableLiveData.setValue(response);

                    }
                }, this::processError));
    }

    public void getComponent(int request_id) {
        HashMap<String, Object> params = new HashMap<>();
        user = sessionManager.getUserDetails();
        params.put("token", user.getToken());
        params.put("request_id", request_id);

        disposable.add(networkManager.postRequest(Endpoints.GET_REQUEST_COMPONENT, params, GetComponentResponse.class)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(v -> view.showLoading())
                .doFinally(view::hideLoading)
                .subscribe(response -> {
                    if (response.getStatus() != 0) {
                        view.showSuccessMessage(response.getMsg());
                    } else {
                        view.showSuccessMessage(response.getMsg());
                        componentResponseMutableLiveData.setValue(response);

                    }
                }, this::processError));
    }

    public void makeRequest(int depId, int serviceId, String name, String description, List<AddonModel> selectedAddons, ArrayList<MediaFile> files) {

        HashMap<String, RequestBody> params = new HashMap<>();
        user = sessionManager.getUserDetails();

        params.put("token", RequestBody.create(MediaType.parse("text/plain"), user.getToken()));
        params.put("department_id", RequestBody.create(MediaType.parse("text/plain"), String.valueOf(depId)));
        params.put("service_id", RequestBody.create(MediaType.parse("text/plain"), String.valueOf(serviceId)));
        params.put("name", RequestBody.create(MediaType.parse("text/plain"), name));
        params.put("descr", RequestBody.create(MediaType.parse("text/plain"), description));

        List<AddonModel> addonsIds = selectedAddons;
        List<Integer> ret = new ArrayList<>();
        for (int i = 0; i < addonsIds.size(); i++) {
            ret.add(addonsIds.get(i).getId());
        }

        if (addonsIds.size() != 0) {
            for (int i = 0; i < addonsIds.size(); i++) {
                params.put("addons[" + i + "]", RequestBody.create(MediaType.parse("text/plain"), String.valueOf(ret.get(i))));
            }
        }

        List<MultipartBody.Part> mpFiles = new ArrayList<>();
        for (MediaFile file : files) {
            RequestBody reqFile = RequestBody.create(MediaType.parse("multipart/form-data"), new File(file.getPath()));
            mpFiles.add(MultipartBody.Part.createFormData("attachments[]", file.getName(), reqFile));
        }


        disposable.add(networkManager.multiPartRequest(Endpoints.ADD_REQUEST_URL, params, mpFiles, MakeRequestResponse.class)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(v -> view.showLoading())
                .doFinally(view::hideLoading)
                .subscribe(response -> {
                    if (response.status != 0) {
                        view.showSuccessMessage(response.msg);
                    } else {
                        makeRequestResponseLiveData.setValue(response);
                    }
                }, this::processError));
    }

/*
    public void makeRequest(int depId, int serviceId, String name, String description, List<AddonModel> selectedAddons, ArrayList<MediaFile> files) {

        view.showLoading();
        List<File> files = new ArrayList<>();
        HashMap<String, String> docs = new HashMap<>();
        for (String key : selectedImagesHashMap.keySet()) {
            File f = new File(selectedImagesHashMap.get(key).getPath());
            files.add(f);
            docs.put(key, f.getName());
        }
        user = sessionManager.getUserDetails();


        AndroidNetworking.upload(Endpoints.ADD_REQUEST_URL)
                .addMultipartFileList("file[]", files)
                .addMultipartParameter("token", user.getToken())
                .addMultipartParameter("ServiceId", String.valueOf(serviceId))
                .addMultipartParameter("department_id", String.valueOf(depId))
                .addMultipartParameter("name", name)
                .addMultipartParameter("descr", description)
                //.addMultipartParameter(docs)
                .setPriority(Priority.HIGH)
                .setTag("uploadTest")
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {

                        view.hideLoading();
                        if (response.has("status")) {
                            try {
                                if (response.getString("status").equals("success")) {
                                    view.showSuccessMessage("تم الطلب بنجاح");
                                    view.finishScreen();
                                    //makeRequestResponseLiveData.setValue(response);

                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                    }

                    @Override
                    public void onError(ANError error) {
                        processError(error);
                    }
                });
    }
*/


    public void contracttAction(int request_id, String national_id, String name, String email, String phone, String img) {

        HashMap<String, RequestBody> params = new HashMap<>();
        HashMap<String, MultipartBody.Part> files = new HashMap<>();

        user = sessionManager.getUserDetails();

        RequestBody token = RequestBody.create(MediaType.parse("text/plain"), user.getToken());
        RequestBody nameReq = RequestBody.create(MediaType.parse("text/plain"), name);
        RequestBody phoneReq = RequestBody.create(MediaType.parse("text/plain"), phone);
        RequestBody emailReq = RequestBody.create(MediaType.parse("text/plain"), email);
        RequestBody nationalIdReq = RequestBody.create(MediaType.parse("text/plain"), national_id);
        RequestBody requestIdReq = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(request_id));

        File imageFile = new File(img);
        RequestBody reqFile = RequestBody.create(MediaType.parse("image/*"), imageFile);
        MultipartBody.Part image = MultipartBody.Part.createFormData("img", imageFile.getName(), reqFile);


        params.put("token", token);
        params.put("request_id", requestIdReq);
        params.put("national_id", nationalIdReq);
        params.put("name", nameReq);
        params.put("email", emailReq);
        params.put("phone", phoneReq);

        // params.put("img", img);

        disposable.add(networkManager.multiPartRequestJustFile(Endpoints.CONTRACT_ACTION_URL, params, image, ContractActionResponse.class)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(v -> view.showLoading())
                .doFinally(view::hideLoading)
                .subscribe(response -> {
                    if (response.status != 0) {
                        view.showSuccessMessage(response.msg);
                    } else {
                        contractActionResponseMutableLiveData.setValue(response);
                    }
                }, this::processError));
    }

    public void getRequestContract(int request_id) {
        HashMap<String, Object> params = new HashMap<>();
        user = sessionManager.getUserDetails();
        params.put("token", user.getToken());
        params.put("request_id", request_id);
        disposable.add(networkManager.postRequest(Endpoints.GET_REQUEST_CONTRACT, params, ContractResponse.class)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(v -> view.showLoading())
                .doFinally(view::hideLoading)
                .subscribe(response -> {
                    if (response.getStatus() != 0 && response.getStatus() != 211) {
                        view.showSuccessMessage(response.getMsg());

                    } else if (response.getStatus() == 0 || response.getStatus() == 211) {
                        getContractResponseLiveData.setValue(response);
                    }
                }, this::processError));
    }


    public void signatureCode(String code, int contract_id) {
        HashMap<String, Object> params = new HashMap<>();
        user = sessionManager.getUserDetails();
        params.put("token", user.getToken());
        params.put("contract_id", contract_id);
        params.put("code", code);
        disposable.add(networkManager.postRequest(Endpoints.CONTRACT_CHECK_CODE_URL, params, LoginResponse.class)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(v -> view.showLoading())
                .doFinally(view::hideLoading)
                .subscribe(response -> {
                    if (response.getStatus() != 0) {
                        view.showSuccessMessage(response.getMsg());
                    } else {
                        isActionSuccess.setValue(true);
                        view.showSuccessMessage(response.getMsg());

                    }
                }, this::processError));
    }

    public void getSignatureCode(int contract_id) {
        HashMap<String, Object> params = new HashMap<>();
        user = sessionManager.getUserDetails();
        params.put("token", user.getToken());
        params.put("contract_id", contract_id);
        disposable.add(networkManager.postRequest(Endpoints.CONTRACT_GET_CODE_URL, params, LoginResponse.class)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(v -> view.showLoading())
                .doFinally(view::hideLoading)
                .subscribe(response -> {
                    if (response.getStatus() != 0) {
                        view.showSuccessMessage(response.getMsg());
                    } else {
                        isCodeSent.setValue(true);
                    }
                }, this::processError));
    }

    public void checkCoded(String code) {
        HashMap<String, Object> params = new HashMap<>();
        user = sessionManager.getUserDetails();
        params.put("token", user.getToken());
        params.put("code", code);
        disposable.add(networkManager.postRequest(Endpoints.CHECK_CODE_URL, params, StatusResponse.class)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(v -> view.showLoading())
                .doFinally(view::hideLoading)
                .subscribe(response -> {
                    if (response.getStatus() != 0) {
                        view.showSuccessMessage(response.getMsg());
                    } else {
                        isActivationSuccess.setValue(true);
                    }
                }, this::processError));

    }

    public void checkPCoded(String code) {
        HashMap<String, Object> params = new HashMap<>();
        user = sessionManager.getUserDetails();
        params.put("token", user.getToken());
        params.put("code", code);
        disposable.add(networkManager.postRequest(Endpoints.CHECK_PCODE_URL, params, StatusResponse.class)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(v -> view.showLoading())
                .doFinally(view::hideLoading)
                .subscribe(response -> {
                    if (response.getStatus() != 0) {
                        view.showSuccessMessage(response.getMsg());
                    } else {
                        isActivationSuccess.setValue(true);
                    }
                }, this::processError));

    }


    public void newPassword(String pass) {

        HashMap<String, Object> params = new HashMap<>();
        user = sessionManager.getUserDetails();
        params.put("token", user.getToken());
        params.put("new_password", pass);
        disposable.add(networkManager.postRequest(Endpoints.ADD_PASSWORD_URL, params, StatusResponse.class)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(v -> view.showLoading())
                .doFinally(view::hideLoading)
                .subscribe(response -> {
                    if (response.getStatus() != 0) {
                        view.showSuccessMessage(response.getMsg());
                    } else {
                        newPasswordSuccess.setValue(true);
                    }
                }, this::processError));
    }

    public void resendCodeCoded() {
        HashMap<String, Object> params = new HashMap<>();
        user = sessionManager.getUserDetails();
        params.put("token", user.getToken());
        disposable.add(networkManager.postRequest(Endpoints.RESEND_CODE_URL, params, VCodeResponse.class)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(v -> view.showLoading())
                .doFinally(view::hideLoading)
                .subscribe(response -> {
                    if (response.getStatus() != 0) {
                        view.showSuccessMessage(response.getMsg());
                    } else {
                        user.setToken(response.getToken());
                        sessionManager.createLoginSession(user);
                        isCodeSent.setValue(true);
                    }
                }, this::processError));

    }

    public void editProfile(int ability, String companyName, String username, String email, String phone, String img) {

        HashMap<String, RequestBody> params = new HashMap<>();

        user = sessionManager.getUserDetails();

        RequestBody token = RequestBody.create(MediaType.parse("text/plain"), user.getToken());
        RequestBody nameReq = RequestBody.create(MediaType.parse("text/plain"), username);
        RequestBody phoneReq = RequestBody.create(MediaType.parse("text/plain"), phone);
        RequestBody emailReq = RequestBody.create(MediaType.parse("text/plain"), email);
        RequestBody abilityReq = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(ability));
        RequestBody companyNameReq = RequestBody.create(MediaType.parse("text/plain"), companyName);
        RequestBody userTypeReq = RequestBody.create(MediaType.parse("text/plain"), user.getUserType());


        File imageFile = new File(img);
        RequestBody reqFile = RequestBody.create(MediaType.parse("image/*"), imageFile);
        MultipartBody.Part image = MultipartBody.Part.createFormData("img", imageFile.getName(), reqFile);
        if (img.equals("")) {
            image = null;
        }
        params.put("token", token);
        params.put("name", nameReq);
        params.put("email", emailReq);
        params.put("phone", phoneReq);
        params.put("user_type", userTypeReq);

        if (ability == 1)
            params.put("ability", abilityReq);
        if (user.getUserType().equals("company"))
            params.put("company_name", companyNameReq);


        disposable.add(networkManager.multiPartRequestJustFile(Endpoints.UPDATE_PROFILE, params, image, LoginResponse.class)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .doOnSubscribe(v -> view.showLoading())
                .doFinally(view::hideLoading)
                .subscribe(response -> {
                    if (response.getStatus() != 0) {
                        view.showSuccessMessage(response.getMsg());
                    } else {
                        loginLiveDataMutableLiveData.setValue(response);
                        sessionManager.createLoginSession(response.getUserData());
                    }
                }, this::processError));
    }


    public void getProfileData() {
        HashMap<String, Object> params = new HashMap<>();
        user = sessionManager.getUserDetails();
        params.put("token", user.getToken());
        disposable.add(networkManager.postRequest(Endpoints.GET_PROFILE_DATA, params, GetProfileResponse.class)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(v -> view.showLoading())
                .doFinally(view::hideLoading)
                .subscribe(response -> {
                    if (response.getStatus() != 0) {
                        view.showSuccessMessage(response.getMsg());
                    } else {
                        getProfileData.setValue(response);
                    }
                }, this::processError));

    }


    public void getRequestDetails(int requestId) {
        HashMap<String, Object> params = new HashMap<>();
        user = sessionManager.getUserDetails();
        params.put("token", user.getToken());
        params.put("request_id", requestId);
        disposable.add(networkManager.postRequest(Endpoints.REQUEST_DETAILS_URL, params, RequestDetailsResponse.class)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(v -> view.showLoading())
                .doFinally(view::hideLoading)
                .subscribe(response -> {
                    if (response.getStatus() != 0) {
                        view.showSuccessMessage(response.getMsg());
                    } else {
                        requestDetailsLiveData.setValue(response.getData());
                        view.showSuccessMessage("Done");
                    }
                }, this::processError));
    }

    public void getMessageChat(int request_id) {
        HashMap<String, Object> params = new HashMap<>();
        user = sessionManager.getUserDetails();
        params.put("token", user.getToken());
        params.put("request_id", request_id);

        disposable.add(networkManager.getRequest(Endpoints.GET_REQUEST_CHAT, params, GetChatResponse.class)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(v -> view.showLoading())
                .doFinally(view::hideLoading)
                .subscribe(response -> {
                    if (response.getStatus() != 0) {
                        view.showSuccessMessage(response.getMsg());
                    } else {
                        view.showSuccessMessage(response.getMsg());
                        chatResponseMutableLiveData.setValue(response);

                    }
                }, this::processError));
    }

    public void getHelpChat(int department_id) {
        HashMap<String, Object> params = new HashMap<>();
        user = sessionManager.getUserDetails();
        params.put("token", user.getToken());
        params.put("department_id", department_id);

        disposable.add(networkManager.getRequest(Endpoints.GET_HELP_CHAT, params, GetChatResponse.class)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(v -> view.showLoading())
                .doFinally(view::hideLoading)
                .subscribe(response -> {
                    if (response.getStatus() != 0) {
                        view.showSuccessMessage(response.getMsg());
                    } else {
                        view.showSuccessMessage(response.getMsg());
                        chatResponseMutableLiveData.setValue(response);

                    }
                }, this::processError));
    }

    public void sendMessage(int request_id, String msg_text, String file_type, String file) {
        HashMap<String, RequestBody> params = new HashMap<>();

        user = sessionManager.getUserDetails();
        String endPoint;

        RequestBody token = RequestBody.create(MediaType.parse("text/plain"), user.getToken());
        RequestBody requestId = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(request_id));

        MultipartBody.Part image = null;
        if (file_type.equals("no_file")) {
            RequestBody message_text = RequestBody.create(MediaType.parse("text/plain"), msg_text);
            params.put("msg_content", message_text);
            endPoint = Endpoints.SEND_MESSAGE_TEXT;
        } else {
            File imageFile = new File(file);
            RequestBody reqFile = RequestBody.create(MediaType.parse("multipart/form-data"), imageFile);
            image = MultipartBody.Part.createFormData("file", imageFile.getName(), reqFile);
            if (file.equals("")) {
                image = null;
            }
            endPoint = Endpoints.SEND_MESSAGE_FILE;
        }

        //RequestBody fileType = RequestBody.create(MediaType.parse("text/plain"), file_type);


        params.put("token", token);
        params.put("request_id", requestId);


        disposable.add(networkManager.multiPartRequestJustFile(endPoint, params, image, SendMessageResponse.class)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .doOnSubscribe(v -> view.showLoading())
                .doFinally(view::hideLoading)
                .subscribe(response -> {
                    if (response.getStatus() != 0) {
                        view.showSuccessMessage(response.getMsg());
                    } else {
                        sendMessageResponseMutableLiveData.setValue(response);
                    }
                }, this::processError));
    }

    public void sendHelpMessage(int dept_id, String msg_text, String file_type, String file) {
        HashMap<String, RequestBody> params = new HashMap<>();

        user = sessionManager.getUserDetails();
        String endPoint;

        RequestBody token = RequestBody.create(MediaType.parse("text/plain"), user.getToken());
        RequestBody deptId = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(dept_id));

        MultipartBody.Part image = null;
        if (file_type.equals("no_file")) {
            RequestBody message_text = RequestBody.create(MediaType.parse("text/plain"), msg_text);
            params.put("msg_content", message_text);
            endPoint = Endpoints.SEND_HELP_TEXT;
        } else {
            File imageFile = new File(file);
            RequestBody reqFile = RequestBody.create(MediaType.parse("multipart/form-data"), imageFile);
            image = MultipartBody.Part.createFormData("file", imageFile.getName(), reqFile);
            if (file.equals("")) {
                image = null;
            }
            endPoint = Endpoints.SEND_HELP_FILE;
        }

        //RequestBody fileType = RequestBody.create(MediaType.parse("text/plain"), file_type);


        params.put("token", token);
        params.put("department_id", deptId);


        disposable.add(networkManager.multiPartRequestJustFile(endPoint, params, image, SendMessageResponse.class)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .doOnSubscribe(v -> view.showLoading())
                .doFinally(view::hideLoading)
                .subscribe(response -> {
                    if (response.getStatus() != 0) {
                        view.showSuccessMessage(response.getMsg());
                    } else {
                        sendMessageResponseMutableLiveData.setValue(response);
                    }
                }, this::processError));
    }


    private void processError(Throwable throwable) {
        view.hideLoading();
        if (throwable instanceof IOException)
            view.showErrorMessage("No internet connection !!");
        else
            view.showErrorMessage("An error ");
    }

    public void dispose() {
        disposable.dispose();
    }


}



