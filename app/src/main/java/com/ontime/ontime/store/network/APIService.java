package com.ontime.ontime.store.network;

import com.google.gson.JsonElement;

import java.util.List;
import java.util.Map;

import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.Single;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.http.*;

public interface APIService {

    @GET
    Observable<JsonElement> getRequest(@Url String api, @HeaderMap Map<String, Object> headers, @QueryMap Map<String, Object> param);

//    @GET
//    Observable<JsonElement> getPathRequest(@Url String api, @HeaderMap Map<String, String> headers, @Path("searchCriteria[filter_groups][0][filters][0][field]") String par1);

    @PUT
    Observable<JsonElement> putRequest(@Url String api, @HeaderMap Map<String, Object> headers, @Body Map<String, Object> body);

    @PUT
    Observable<JsonElement> putFieldRequest(@Url String api, @HeaderMap Map<String, Object> headers, @FieldMap Map<String, Object> body);

    @FormUrlEncoded
    @POST
    Observable<JsonElement> postRequest(@Url String api, @HeaderMap Map<String, Object> headers, @FieldMap Map<String, Object> body);

    @Multipart
    @POST
    Observable<JsonElement> multipartRequest(@Url String api, @HeaderMap Map<String, Object> headers, @PartMap Map<String, RequestBody> data,
                                             @Part MultipartBody.Part[] attachments);

    @Multipart
    @POST
    Observable<JsonElement> multipartRequestJustFile(@Url String api, @HeaderMap Map<String, Object> headers, @PartMap Map<String, RequestBody> data,
                                             @Part MultipartBody.Part img);

    @PUT
    Completable completablePutRequest(@Url String api, @HeaderMap Map<String, Object> headers, @Body Map<String, Object> body);

    @POST
    Completable completablePostRequest(@Url String api, @HeaderMap Map<String, Object> headers, @Body Map<String, Object> body);
}