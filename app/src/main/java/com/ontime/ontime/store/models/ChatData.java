package com.ontime.ontime.store.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ChatData {
    @Expose
    @SerializedName("readed")
    private int readed;
    @Expose
    @SerializedName("msg_type")
    private String msgType;
    @Expose
    @SerializedName("time")
    private String time;
    @Expose
    @SerializedName("file_url")
    private String fileUrl;
    @Expose
    @SerializedName("file_type")
    private String fileType;
    @Expose
    @SerializedName("msg_text")
    private String msgText;
    @Expose
    @SerializedName("id")
    private int id;

    public int getReaded() {
        return readed;
    }

    public String getMsgType() {
        return msgType;
    }

    public String getTime() {
        return time;
    }

    public String getFileUrl() {
        return fileUrl;
    }

    public String getFileType() {
        return fileType;
    }

    public String getMsgText() {
        return msgText;
    }

    public int getId() {
        return id;
    }

    public void setReaded(int readed) {
        this.readed = readed;
    }

    public void setMsgType(String msgType) {
        this.msgType = msgType;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public void setFileUrl(String fileUrl) {
        this.fileUrl = fileUrl;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    public void setMsgText(String msgText) {
        this.msgText = msgText;
    }

    public void setId(int id) {
        this.id = id;
    }
}
