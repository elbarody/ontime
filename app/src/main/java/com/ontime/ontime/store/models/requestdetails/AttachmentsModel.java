package com.ontime.ontime.store.models.requestdetails;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AttachmentsModel {


    @Expose
    @SerializedName("upload_date")
    private String upload_date;
    @Expose
    @SerializedName("size")
    private String size;
    @Expose
    @SerializedName("file_extension")
    private String file_extension;
    @Expose
    @SerializedName("file_name")
    private String file_name;
    @Expose
    @SerializedName("file")
    private String file;

    public String getUpload_date() {
        return upload_date;
    }

    public String getSize() {
        return size;
    }

    public String getFile_extension() {
        return file_extension;
    }

    public String getFile_name() {
        return file_name;
    }

    public String getFile() {
        return file;
    }
}
