package com.ontime.ontime.store.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public  class ForgetPasswordResponse {


    @Expose
    @SerializedName("email")
    private String email;
    @Expose
    @SerializedName("pcode")
    private String pcode;
    @Expose
    @SerializedName("token")
    private String token;
    @Expose
    @SerializedName("msg")
    private String msg;
    @Expose
    @SerializedName("status")
    private int status;

    public String getEmail() {
        return email;
    }

    public String getPcode() {
        return pcode;
    }

    public String getToken() {
        return token;
    }

    public String getMsg() {
        return msg;
    }

    public int getStatus() {
        return status;
    }
}
