package com.ontime.ontime.store.models.service;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public  class ServicesResponse implements Serializable {


    @Expose
    @SerializedName("data")
    private DataModel data;
    @Expose
    @SerializedName("services")
    private List<ServicesModel> services;
    @Expose
    @SerializedName("msg")
    private String msg;
    @Expose
    @SerializedName("status")
    private int status;

    public DataModel getData() {
        return data;
    }

    public List<ServicesModel> getServices() {
        return services;
    }

    public String getMsg() {
        return msg;
    }

    public int getStatus() {
        return status;
    }
}
