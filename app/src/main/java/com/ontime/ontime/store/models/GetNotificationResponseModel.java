package com.ontime.ontime.store.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GetNotificationResponseModel {

    @Expose
    @SerializedName("data")
    private List<NotificationData> data;
    @Expose
    @SerializedName("unread_num")
    private String unreadNum;
    @Expose
    @SerializedName("msg")
    private String msg;
    @Expose
    @SerializedName("status")
    private int status;

    public List<NotificationData> getData() {
        return data;
    }

    public String getUnreadNum() {
        return unreadNum;
    }

    public String getMsg() {
        return msg;
    }

    public int getStatus() {
        return status;
    }
}
