package com.ontime.ontime.store.models.user;

import com.google.gson.annotations.SerializedName;

public class RegisterResponse {

    @SerializedName("lastid")
    private int lastid;
    @SerializedName("status")
    private int status;
    @SerializedName("msg")
    private String msg;
    @SerializedName("token")
    private String token;

    public int getStatus() {
        return status;
    }

    public String getMsg() {
        return msg;
    }

    public int getLastid() {
        return lastid;
    }

    public String getToken() {
        return token;
    }
}
