package com.ontime.ontime.store.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GetComponentResponse {

    @Expose
    @SerializedName("data")
    private List<ComponentData> data;
    @Expose
    @SerializedName("tax")
    private String tax;
    @Expose
    @SerializedName("total")
    private float total;
    @Expose
    @SerializedName("status")
    private int status;
    @Expose
    @SerializedName("msg")
    private String msg;

    public List<ComponentData> getData() {
        return data;
    }

    public String getTax() {
        return tax;
    }

    public float getTotal() {
        return total;
    }

    public int getStatus() {
        return status;
    }

    public String getMsg() {
        return msg;
    }
}
