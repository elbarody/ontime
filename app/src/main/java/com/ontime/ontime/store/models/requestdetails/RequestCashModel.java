package com.ontime.ontime.store.models.requestdetails;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RequestCashModel {

    @Expose
    @SerializedName("created_at")
    private String created_at;
    @Expose
    @SerializedName("file_size")
    private String file_size;
    @Expose
    @SerializedName("pdf")
    private String pdf;
    @Expose
    @SerializedName("name")
    private String name;
    @Expose
    @SerializedName("request_id")
    private int request_id;
    @Expose
    @SerializedName("id")
    private int id;

    public String getCreated_at() {
        return created_at;
    }

    public String getFile_size() {
        return file_size;
    }

    public String getPdf() {
        return pdf;
    }

    public String getName() {
        return name;
    }

    public int getRequest_id() {
        return request_id;
    }

    public int getId() {
        return id;
    }
}
