package com.ontime.ontime.store.models.requestdetails;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ContractsModel {
    @Expose
    @SerializedName("pdf")
    private String pdf;
    @Expose
    @SerializedName("name")
    private String name;

    public String getPdf() {
        return pdf;
    }

    public String getName() {
        return name;
    }
}
