package com.ontime.ontime.store.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ComponentData {
    @Expose
    @SerializedName("duration")
    private int duration;
    @Expose
    @SerializedName("price")
    private float price;
    @Expose
    @SerializedName("component_name")
    private String componentName;

    public int getDuration() {
        return duration;
    }

    public float getPrice() {
        return price;
    }

    public String getComponentName() {
        return componentName;
    }
}
