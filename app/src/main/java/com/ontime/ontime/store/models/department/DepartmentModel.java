package com.ontime.ontime.store.models.department;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class DepartmentModel implements Serializable {

    @SerializedName("id")
    private int id;
    @SerializedName("name")
    private String name;
    @SerializedName("descr")
    private String descr;
    @SerializedName("img")
    private String img;

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescr() {
        return descr;
    }

    public String getImg() {
        return img;
    }
}
