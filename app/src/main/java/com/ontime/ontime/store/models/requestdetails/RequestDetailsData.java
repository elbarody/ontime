package com.ontime.ontime.store.models.requestdetails;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.ontime.ontime.store.models.service.AddonModel;

import java.util.List;

public class RequestDetailsData {
    @Expose
    @SerializedName("contracts")
    private List<RequestCashModel> contracts;

    @Expose
    @SerializedName("attachments")
    private List<AttachmentsModel> attachments;

    @Expose
    @SerializedName("request_cash")
    private List<RequestCashModel> request_cash;

    @Expose
    @SerializedName("request_tracking")
    private List<RequestTrackingModel> request_tracking;

    @Expose
    @SerializedName("terms")
    private String terms;
    @Expose
    @SerializedName("addons")
    private List<AddonModel> addons;
    @Expose
    @SerializedName("total time")
    private String totalTime;
    @Expose
    @SerializedName("start_from")
    private String startFrom;
    @Expose
    @SerializedName("service_id")
    private String serviceId;
    @Expose
    @SerializedName("service_name")
    private String serviceName;
    @Expose
    @SerializedName("img")
    private String img;
    @Expose
    @SerializedName("has_contract")
    private int hasContract;
    @Expose
    @SerializedName("has_price")
    private int hasPrice;
    @Expose
    @SerializedName("new_message")
    private int newMessage;
    @Expose
    @SerializedName("end_time")
    private String endTime;
    @Expose
    @SerializedName("stop_time")
    private String stopTime;
    @Expose
    @SerializedName("start_time")
    private String startTime;
    @Expose
    @SerializedName("componants_ready")
    private int componantsReady;
    @Expose
    @SerializedName("status")
    private String status;
    @Expose
    @SerializedName("create_time")
    private String createTime;
    @Expose
    @SerializedName("request_descr")
    private String requestDescr;
    @Expose
    @SerializedName("request_name")
    private String requestName;
    @Expose
    @SerializedName("id")
    private int id;

    public List<AttachmentsModel> getAttachments() {
        return attachments;
    }

    public String getTotalTime() {
        return totalTime;
    }

    public List<RequestCashModel> getContracts() {
        return contracts;
    }

    public String getTerms() {
        return terms;
    }

    public List<AddonModel> getAddons() {
        return addons;
    }


    public String getStartFrom() {
        return startFrom;
    }

    public String getServiceId() {
        return serviceId;
    }

    public String getServiceName() {
        return serviceName;
    }

    public String getImg() {
        return img;
    }

    public int getHasContract() {
        return hasContract;
    }

    public int getHasPrice() {
        return hasPrice;
    }

    public int getNewMessage() {
        return newMessage;
    }

    public String getEndTime() {
        return endTime;
    }

    public String getStopTime() {
        return stopTime;
    }

    public String getStartTime() {
        return startTime;
    }

    public int getComponantsReady() {
        return componantsReady;
    }

    public String getStatus() {
        return status;
    }

    public String getCreateTime() {
        return createTime;
    }

    public String getRequestDescr() {
        return requestDescr;
    }

    public String getRequestName() {
        return requestName;
    }

    public int getId() {
        return id;
    }

    public List<RequestCashModel> getRequest_cash() {
        return request_cash;
    }

    public List<RequestTrackingModel> getRequest_tracking() {
        return request_tracking;
    }
}
