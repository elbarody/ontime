package com.ontime.ontime.store.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BadgesEntity {
    @Expose
    @SerializedName("img")
    private String img;
    @Expose
    @SerializedName("title")
    private String title;
    @Expose
    @SerializedName("id")
    private int id;

    public String getImg() {
        return img;
    }

    public String getTitle() {
        return title;
    }

    public int getId() {
        return id;
    }
}
