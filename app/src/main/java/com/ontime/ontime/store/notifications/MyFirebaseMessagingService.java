package com.ontime.ontime.store.notifications;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import androidx.core.app.ComponentActivity;
import androidx.core.app.NotificationCompat;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.ontime.ontime.R;
import com.ontime.ontime.base.BaseView;
import com.ontime.ontime.store.models.StatusResponse;
import com.ontime.ontime.store.models.user.UserData;
import com.ontime.ontime.store.network.Endpoints;
import com.ontime.ontime.store.network.NetworkManager;
import com.ontime.ontime.store.repo.AppRepo;
import com.ontime.ontime.utils.SessionManager;
import com.ontime.ontime.views.activities.home.HomeActivity;
import com.xwady.core.models.User;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;;import java.io.IOException;
import java.util.HashMap;

/**
 * Created by eng on 06/01/18.
 */

public class MyFirebaseMessagingService extends FirebaseMessagingService {
    int i = 0;
    private Intent intent;
    private String request_id;
    private SessionManager sessionManager;
    private UserData user;
    private String type;
    private CompositeDisposable disposable;
    private NetworkManager networkManager;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        sessionManager = new SessionManager(this);
        user = sessionManager.getUserDetails();
        this.disposable = new CompositeDisposable();
        this.networkManager = new NetworkManager();
        //this.view = (BaseView) this;

        if (!user.getToken().equals(remoteMessage.getData().get("token"))) {
            return;
        }
        String message = remoteMessage.getData().get("message");
        String notification_id = remoteMessage.getData().get("notification_id");
        request_id = remoteMessage.getData().get("request_id");
        type = remoteMessage.getData().get("type");
        if (type.equals("view_components")) {
            intent = new Intent(getApplicationContext(), ComponentActivity.class);
        } else
            intent = new Intent(getApplicationContext(), HomeActivity.class);
        intent.putExtra("request_id", request_id);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        readWebService(notification_id);

        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this);
        notificationBuilder.setContentTitle(getString(R.string.app_name));


        notificationBuilder.setContentText(message);
        notificationBuilder.setAutoCancel(true);
        notificationBuilder.setSmallIcon(R.drawable.android_icon);
        notificationBuilder.setContentIntent(pendingIntent);
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(i++, notificationBuilder.build());


    }


    private void readWebService(String id) {
        HashMap<String, Object> params = new HashMap<>();
        user = sessionManager.getUserDetails();
        params.put("token", user.getToken());
        params.put("reading_type", "single");
        params.put("notification_id", id);
        disposable.add(networkManager.postRequest(Endpoints.READ_NOTIFICATION, params, StatusResponse.class)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(response -> {

                    Log.d("readWebService: ", response.getMsg());
                    dispose();
                }));
    }



    public void dispose() {
        disposable.dispose();
    }


}
