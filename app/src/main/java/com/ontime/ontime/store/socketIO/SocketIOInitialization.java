package com.ontime.ontime.store.socketIO;

import android.util.Log;
import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Socket;

import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;

public class SocketIOInitialization {

    public Socket socket;
    private static String SOCKET_URL = "https://appontime.net:49160";
    private static final Map<String, SocketIOInitialization> socketIOInitialization = new HashMap<>();
    private static final Map<String, Socket> sockets = new HashMap<>();
    private String token;

    private SocketIOInitialization(String token) {
        this.token = token;

        socketIOInitialization.put(token, connectSocket(token));
        sockets.put(token, this.socket);

    }

    public static SocketIOInitialization getSocketInstance(String token) {
        if (null == socketIOInitialization.get(token)) {
            new SocketIOInitialization(token);
        }
        return socketIOInitialization.get(token);
    }

    public Socket getSocket() {
        return sockets.get(this.token);
    }

    public void removeSocket(){
        socket.disconnect();
        sockets.remove(token);
        socketIOInitialization.remove(token);
    }
    private SocketIOInitialization connectSocket(String token) {
        try {


            //if you are using a phone device you should connect to same local network as your laptop and disable your pubic firewall as well
            socket = IO.socket(SOCKET_URL);
            socket.emit("initialize_user",token);

            //create connection

            socket.connect();


// emit the event join along side with the nickname
            Log.d("onCreate socket ", "on");
            return this;


        } catch (URISyntaxException e) {
            e.printStackTrace();

        }
        return null;
    }



}
