package com.ontime.ontime.store.models.user;

import com.google.gson.annotations.SerializedName;

public class VCodeResponse {

    @SerializedName("status")
    private int status;
    @SerializedName("msg")
    private String msg;
    @SerializedName("token")
    private String token;
    @SerializedName("vcode")
    private String vcode;

    public int getStatus() {
        return status;
    }

    public String getMsg() {
        return msg;
    }

    public String getToken() {
        return token;
    }

    public String getVcode() {
        return vcode;
    }
}
