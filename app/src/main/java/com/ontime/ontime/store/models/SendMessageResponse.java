package com.ontime.ontime.store.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SendMessageResponse extends StatusResponse {



    @Expose
    @SerializedName("data")
    private MessageData data;

    private int status_code;

    public String getMsg() {
        return String.valueOf(status_code);
    }


    public MessageData getSocket() {
        return data;
    }
}