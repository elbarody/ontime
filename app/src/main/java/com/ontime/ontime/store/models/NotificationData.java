package com.ontime.ontime.store.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class NotificationData {
    @Expose
    @SerializedName("read_status")
    private int readStatus;
    @Expose
    @SerializedName("request_id")
    private int requestId;
    @Expose
    @SerializedName("icon")
    private String icon;
    @Expose
    @SerializedName("time")
    private String time;
    @Expose
    @SerializedName("msg_body")
    private String msgBody;
    @Expose
    @SerializedName("msg_header")
    private String msgHeader;
    @Expose
    @SerializedName("type")
    private String type;
    @Expose
    @SerializedName("id")
    private int id;

    public int getReadStatus() {
        return readStatus;
    }

    public int getRequestId() {
        return requestId;
    }

    public String getIcon() {
        return icon;
    }

    public String getTime() {
        return time;
    }

    public String getMsgBody() {
        return msgBody;
    }

    public String getMsgHeader() {
        return msgHeader;
    }

    public String getType() {
        return type;
    }

    public int getId() {
        return id;
    }
}
