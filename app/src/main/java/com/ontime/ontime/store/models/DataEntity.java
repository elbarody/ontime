package com.ontime.ontime.store.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DataEntity {
    @Expose
    @SerializedName("projects")
    private int projects;
    @Expose
    @SerializedName("total_paid")
    private int totalPaid;
    @Expose
    @SerializedName("points")
    private int points;
    @Expose
    @SerializedName("percentage")
    private int percentage;
    @Expose
    @SerializedName("company_name")
    private String companyName;
    @Expose
    @SerializedName("email")
    private String email;
    @Expose
    @SerializedName("phone")
    private String phone;
    @Expose
    @SerializedName("user_type")
    private String userType;
    @Expose
    @SerializedName("img")
    private String img;
    @Expose
    @SerializedName("name")
    private String name;
    @Expose
    @SerializedName("id")
    private int id;

    public int getProjects() {
        return projects;
    }

    public int getTotalPaid() {
        return totalPaid;
    }

    public int getPoints() {
        return points;
    }

    public int getPercentage() {
        return percentage;
    }

    public String getCompanyName() {
        return companyName;
    }

    public String getEmail() {
        return email;
    }

    public String getPhone() {
        return phone;
    }

    public String getUserType() {
        return userType;
    }

    public String getImg() {
        return img;
    }

    public String getName() {
        return name;
    }

    public int getId() {
        return id;
    }
}
