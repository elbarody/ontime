package com.ontime.ontime.di.application;

import android.app.Application;
import android.content.Context;
import com.ontime.ontime.OnTimeApplication;
import com.ontime.ontime.di.qualifier.ForApplication;
import com.ontime.ontime.di.scope.ApplicationScope;
import dagger.Module;
import dagger.Provides;

@Module
public class AppModule {

    private final OnTimeApplication application;

    public AppModule(OnTimeApplication application) {
        this.application = application;
    }

    @ApplicationScope
    @Provides
    Application providesApplication() {
        return application;
    }

    @ApplicationScope
    @Provides
    @ForApplication
    Context providesApplicationContext() {
        return application;
    }

}
