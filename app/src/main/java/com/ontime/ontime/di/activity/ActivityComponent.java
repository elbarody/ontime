package com.ontime.ontime.di.activity;

import com.ontime.ontime.di.scope.ActivityScope;
import com.ontime.ontime.views.activities.HelpChatBox.HelpChatBoxActivity;
import com.ontime.ontime.views.activities.aboutActivity.AboutActivity;
import com.ontime.ontime.views.activities.activation.ActivationActivity;
import com.ontime.ontime.views.activities.addproject.AddProjectActivity;
import com.ontime.ontime.views.activities.chatBox.MessageBoxActivity;
import com.ontime.ontime.views.activities.component.ComponentActivity;
import com.ontime.ontime.views.activities.contractSignature.ContractSignatureActivity;
import com.ontime.ontime.views.activities.departments.DepartmentsActivity;
import com.ontime.ontime.views.activities.editProfile.EditProfileActivity;
import com.ontime.ontime.views.activities.forgetpassword.ForgetPasswordActivity;
import com.ontime.ontime.views.activities.helpCategories.HelpCategoriesActivity;
import com.ontime.ontime.views.activities.home.HomeActivity;
import com.ontime.ontime.views.activities.login.LoginActivity;
import com.ontime.ontime.views.activities.newpassword.NewPasswordActivity;
import com.ontime.ontime.views.activities.notification.NotificationActivity;
import com.ontime.ontime.views.activities.profileData.ProfileActivity;
import com.ontime.ontime.views.activities.register.RegistrationActivity;
import com.ontime.ontime.views.activities.requestconfirmation.RequestConfirmationActivity;
import com.ontime.ontime.views.activities.requestdetails.ProductDetailsActivity;
import com.ontime.ontime.views.activities.signatureCode.SignatureCodeActivity;
import com.ontime.ontime.views.activities.termsAndCondations.TermsActivity;
import dagger.Subcomponent;

/**
 * This interface is used by dagger to generate the code that defines the connection between the provider of objects
 * (i.e. {@link ActivityModule}), and the object which expresses a dependency.
 */

@ActivityScope
@Subcomponent(modules = {ActivityModule.class})
public interface ActivityComponent {

//    FragmentComponent plus(FragmentModule fragmentModule);

    void inject(LoginActivity loginActivity);

    void inject(RegistrationActivity loginActivity);

    void inject(ActivationActivity loginActivity);

    void inject(ForgetPasswordActivity loginActivity);

    void inject(HomeActivity homeActivity);

    void inject(DepartmentsActivity homeActivity);

    void inject(AddProjectActivity homeActivity);

    void inject(RequestConfirmationActivity requestConfirmationActivity);

    void inject(ContractSignatureActivity contractSignatureActivity);

    void inject(SignatureCodeActivity signatureCodeActivity);

    void inject(NewPasswordActivity signatureCodeActivity);

    void inject(EditProfileActivity editProfileActivity);

    void inject(ProfileActivity profileActivity);

    void inject(ProductDetailsActivity productDetailsActivity);

    void inject(AboutActivity aboutActivity);

    void inject(NotificationActivity notificationActivity);

    void inject(TermsActivity termsActivity);

    void inject(ComponentActivity componentActivity);

    void inject(MessageBoxActivity openMssageActivity);

    void inject(HelpCategoriesActivity helpCategoriesActivity);

    void inject(HelpChatBoxActivity helpChatBoxActivity);

}
