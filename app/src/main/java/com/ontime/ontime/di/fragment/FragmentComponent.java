package com.ontime.ontime.di.fragment;

import com.ontime.ontime.di.scope.FragmentScope;
import com.ontime.ontime.views.activities.login.LoginActivity;
import com.ontime.ontime.views.fragments.HomeFragment;
import dagger.Subcomponent;

@FragmentScope
@Subcomponent(modules = {FragmentModule.class})
public interface FragmentComponent {

    void inject(HomeFragment homeFragment);

}
