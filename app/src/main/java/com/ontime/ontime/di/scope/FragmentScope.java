package com.ontime.ontime.di.scope;

import javax.inject.Scope;

/**
 This annotation identifies an Fragment-level scope to be used for dependencies injection.
 */

@Scope
public @interface FragmentScope {
}