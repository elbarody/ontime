package com.ontime.ontime.di.fragment;

import android.content.Context;
import com.ontime.ontime.base.BaseActivity;
import com.ontime.ontime.base.BaseFragment;
import com.ontime.ontime.di.qualifier.ForActivity;
import com.ontime.ontime.di.qualifier.ForFragment;
import com.ontime.ontime.di.scope.ActivityScope;
import com.ontime.ontime.di.scope.FragmentScope;
import dagger.Module;
import dagger.Provides;

@Module
public class FragmentModule {
    private final BaseFragment fragment;

    public FragmentModule(BaseFragment fragment) {
        this.fragment = fragment;
    }

    @FragmentScope
    @Provides
    BaseFragment provideActivity() {
        return fragment;
    }

    @FragmentScope
    @Provides
    @ForFragment
    Context provideActivityContext() {
        return fragment.getActivity();
    }
}
