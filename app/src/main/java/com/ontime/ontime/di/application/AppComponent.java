package com.ontime.ontime.di.application;

import com.ontime.ontime.OnTimeApplication;
import com.ontime.ontime.di.activity.ActivityComponent;
import com.ontime.ontime.di.activity.ActivityModule;
import com.ontime.ontime.di.fragment.FragmentComponent;
import com.ontime.ontime.di.fragment.FragmentModule;
import com.ontime.ontime.di.scope.ApplicationScope;
import dagger.Component;

@ApplicationScope
@Component(modules = {AppModule.class})
public interface AppComponent {

    ActivityComponent plus(ActivityModule activityModule);

    FragmentComponent plus(FragmentModule fragmentModule);
    void inject(OnTimeApplication onTimeApplication);
}
