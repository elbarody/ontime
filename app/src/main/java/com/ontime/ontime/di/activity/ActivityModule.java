package com.ontime.ontime.di.activity;

import android.content.Context;


import androidx.fragment.app.FragmentManager;

import com.ontime.ontime.base.BaseActivity;
import com.ontime.ontime.di.qualifier.ForActivity;
import com.ontime.ontime.di.scope.ActivityScope;
import dagger.Module;
import dagger.Provides;

/**
 * This class is responsible for providing the requested objects to {@link ActivityScope} annotated classes
 */

@Module
public class
ActivityModule {

    private final BaseActivity activity;

    public ActivityModule(BaseActivity activity) {
        this.activity = activity;
    }

    @ActivityScope
    @Provides
    BaseActivity provideActivity() {
        return activity;
    }

    @ActivityScope
    @Provides
    @ForActivity
    Context provideActivityContext() {
        return activity;
    }

    @ActivityScope
    @Provides
    FragmentManager provideFragmentManager() {
        return activity.getSupportFragmentManager();
    }



}