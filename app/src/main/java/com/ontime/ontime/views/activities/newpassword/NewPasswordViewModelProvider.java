package com.ontime.ontime.views.activities.newpassword;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import com.ontime.ontime.di.scope.ActivityScope;
import com.ontime.ontime.store.repo.AppRepo;
import com.ontime.ontime.views.activities.forgetpassword.ForgetPasswordViewModel;

import javax.inject.Inject;

@ActivityScope
public class NewPasswordViewModelProvider extends ViewModelProvider.NewInstanceFactory{

    private final AppRepo repo;

    @Inject
    public NewPasswordViewModelProvider(AppRepo repo) {
        this.repo = repo;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        return (T) new NewPasswordViewModel(repo);
    }
}
