package com.ontime.ontime.views.activities.signatureCode;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import com.ontime.ontime.store.repo.AppRepo;

public class SignatureCodeViewModel extends ViewModel {

    public MutableLiveData<Boolean> isActionSuccess;
    private final AppRepo repo;
    public MutableLiveData<Boolean> isCodeSent;

    public SignatureCodeViewModel(AppRepo repo) {
        this.repo = repo;
        this.isActionSuccess = repo.isActionSuccess;
        this.isCodeSent=repo.isCodeSent;
    }


    @Override
    protected void onCleared() {
        repo.dispose();
        super.onCleared();
    }

    public void signatureCode(String code, int contract_id) {
        repo.signatureCode(code, contract_id);
    }

    public void signatureGetCode(int contract_id) {
        repo.getSignatureCode(contract_id);
    }
}
