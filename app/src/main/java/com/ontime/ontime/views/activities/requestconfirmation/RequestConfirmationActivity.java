package com.ontime.ontime.views.activities.requestconfirmation;

import android.content.Intent;
import android.text.Html;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.jaiselrahman.filepicker.model.MediaFile;
import com.ontime.ontime.OnTimeApplication;
import com.ontime.ontime.R;
import com.ontime.ontime.base.BaseActivity;
import com.ontime.ontime.di.activity.ActivityModule;
import com.ontime.ontime.di.scope.ActivityScope;
import com.ontime.ontime.store.models.service.AddonModel;
import com.ontime.ontime.store.models.service.ServicesResponse;
import com.ontime.ontime.utils.DialogBuilder;
import com.ontime.ontime.views.activities.contractSignature.ContractSignatureActivity;
import com.ontime.ontime.views.activities.home.HomeActivity;
import com.ontime.ontime.views.adapters.SelectedAddonsAdapter;
import de.hdodenhof.circleimageview.CircleImageView;

import javax.inject.Inject;
import java.util.ArrayList;

@ActivityScope
public class RequestConfirmationActivity extends BaseActivity {

    @Inject
    ConfirmRequestViewModelProvider provider;

    @BindView(R.id.priceTv)
    TextView priceTv;
    @BindView(R.id.totalPriceTextView)
    TextView totalPriceTextView;
    @BindView(R.id.taxTv)
    TextView taxTv;
    @BindView(R.id.servicesRecycler)
    RecyclerView servicesRecycler;
    @BindView(R.id.termsContainer)
    LinearLayout termsContainer;
    @BindView(R.id.doneImageView)
    ImageView doneImageView;
    @BindView(R.id.nextButton)
    TextView nextButton;
    @BindView(R.id.nameTextview)
    TextView nameTextview;
    @BindView(R.id.descTextview)
    TextView descTextview;
    @BindView(R.id.userImageView)
    CircleImageView selectedServiceImageView;
    @BindView(R.id.selectedServiceTextView)
    TextView selectedServiceTextView;

    @BindView(R.id.addonsTextview)
    TextView addonsTextview;

    private ArrayList<MediaFile> files = new ArrayList<>();
    private ArrayList<AddonModel> addons = new ArrayList<>();

    private boolean termsAccepted = false;
    private ConfirmRequestViewModel viewModel;
    private int depId;
    private ServicesResponse response;
    private int request_id;

    @Override
    protected int getLayout() {
        return R.layout.activity_project_confirmation;
    }

    @Override
    protected void onCreateActivityComponent() {
        OnTimeApplication.getComponent(this)
                .plus(new ActivityModule(this))
                .inject(this);
        viewModel = ViewModelProviders.of(this, provider).get(ConfirmRequestViewModel.class);

        initUI();

        initObservers();
    }

    private void initObservers() {

        viewModel.makeRequestResponseLiveData.observe(this, result -> {
            if (result.status == 0) {
                new DialogBuilder(this, R.layout.layout_confirmation_dialog)
                        .text(R.id.contentTextView, Html.fromHtml("<u>طلبك قيد المراجعه<u/>"))
                        .clickListener(R.id.confirmButton, ((dialog, view) -> {

                            request_id = result.id;
                            viewModel.getContractDtails(request_id);
                            dialog.dismiss();

                        }))
                        .gravity(Gravity.CENTER)
                        .background(R.drawable.inset_bottomsheet_background)
                        .build()
                        .show();
            }
        });

        viewModel.getContractResponseLiveData.observe(this, result -> {
            if (result.getStatus() == 0)
                new DialogBuilder(this, R.layout.dailog_condational)
                        .text(R.id.conditional_content, result.getData().getText())
                        .text(R.id.title, result.getData().getName())
                        .clickListener(R.id.accept, ((dialog, view) -> {
                            dialog.dismiss();
                            startActivity(new Intent(this, ContractSignatureActivity.class).putExtra("request_id", request_id));
                        }))
                        .build()
                        .show();
            else if (result.getStatus()==211){
                startActivity(new Intent(this, HomeActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                this.finish();

            }
        });
    }

    private void initUI() {
        if (getIntent().getExtras() != null) {
            response = (ServicesResponse) getIntent().getExtras().getSerializable("servicesResponse");
            files = getIntent().getExtras().getParcelableArrayList("files");
            addons = new Gson().fromJson(getIntent().getExtras().getString("addons"),
                    new TypeToken<ArrayList<AddonModel>>() {
                    }.getType());

            depId = getIntent().getExtras().getInt("depId");
            nameTextview.setText(getIntent().getExtras().getString("name"));
            descTextview.setText(getIntent().getExtras().getString("desc"));

            taxTv.setText(String.valueOf(response.getData().getTaxPercentage()).concat("%"));

            int price = 0;
            for (AddonModel model : addons) {
                price = price + model.getPrice();
            }

            priceTv.setText("ر.س ".concat(String.valueOf(price)));

            int total = ((price / 10) * response.getData().getTaxPercentage()) + price;

            totalPriceTextView.setText("ر.س ".concat(String.valueOf(total)));

            selectedServiceTextView.setText(response.getData().getName());
            Glide.with(this).load(response.getData().getImg()).into(selectedServiceImageView);

            if (addons.size() != 0) {

                servicesRecycler.setLayoutManager(new LinearLayoutManager(this));
                servicesRecycler.setAdapter(new SelectedAddonsAdapter(this, addons));
            } else addonsTextview.setVisibility(View.GONE);


        }

        termsContainer.setOnClickListener(v -> {
            if (termsAccepted) {
                termsAccepted = false;
                doneImageView.setVisibility(View.GONE);
            } else {
                termsAccepted = true;
                doneImageView.setVisibility(View.VISIBLE);
            }

        });


        nextButton.setOnClickListener(v -> {

            if (!termsAccepted) {
                showErrorMessage("please accept our terms and conditions.");
                return;
            }

            viewModel.makeRequest(depId, response.getData().getId(), getIntent().getExtras().getString("name"),
                    getIntent().getExtras().getString("desc"), addons, files);
        });
//
//
//
//
//        });

    }

}
