package com.ontime.ontime.views.adapters;

import android.content.Context;
import android.content.Intent;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.bumptech.glide.Glide;
import com.makeramen.roundedimageview.RoundedImageView;
import com.marcinorlowski.fonty.Fonty;
import com.ontime.ontime.R;
import com.ontime.ontime.di.qualifier.ForActivity;
import com.ontime.ontime.store.models.NotificationData;
import com.ontime.ontime.views.activities.component.ComponentActivity;
import com.ontime.ontime.views.activities.home.HomeActivity;

import javax.inject.Inject;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

@ForActivity
public class NotificationRecyclerViewAdapter extends RecyclerView.Adapter<NotificationRecyclerViewAdapter.ViewHolder> {

    private List<NotificationData> mData=new ArrayList<>();
    private LayoutInflater mInflater;
    Context context;

    // data is passed into the constructor
    @Inject
    public NotificationRecyclerViewAdapter(@ForActivity Context context) {
        this.mInflater = LayoutInflater.from(context);
        this.context = context;
    }

    public void pushData(List<NotificationData> mData){
            this.mData.clear();
            this.mData.addAll(mData);
            notifyDataSetChanged();
    }
    // inflates the row layout from xml when needed
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.item_notifications, parent, false);
        Fonty.setFonts((ViewGroup) view);

        return new ViewHolder(view);
    }

    // binds the data to the TextView in each row
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        //String animal = mData.get(position)

        holder.bind(mData.get(position));
        holder.itemView.setOnClickListener(v -> {
            Intent intent;
            if (mData.get(position).getType().equals("view_components")) {
                intent = new Intent(context, ComponentActivity.class);
            } else
                intent = new Intent(context, HomeActivity.class);
            intent.putExtra("request_id", mData.get(position).getRequestId());
            context.startActivity(intent);
        });
    }

    // total number of rows
    @Override
    public int getItemCount() {
        return mData.size();
    }


    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.requestImageView)
        RoundedImageView requestImageView;
        @BindView(R.id.notification_title)
        TextView notificationTitle;
        @BindView(R.id.notification_body)
        TextView notificationBody;
        @BindView(R.id.notification_time)
        TextView notificationTime;
        ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }

        public void bind(NotificationData item) {
            Glide.with(context).load(item.getIcon()).placeholder(R.drawable.add_icon).into(requestImageView);
            notificationTitle.setText(item.getMsgHeader());
            notificationBody.setText(item.getMsgBody());
            notificationTime.setText(getAgoTime(item.getTime()));
        }
    }

    private String getAgoTime(String created_at) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        //sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
        long time = 0;
        try {
            time = sdf.parse(created_at).getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        long now = System.currentTimeMillis();

        CharSequence ago =
                DateUtils.getRelativeTimeSpanString(time, now, DateUtils.MINUTE_IN_MILLIS);
        return (String) ago;
    }
}

