package com.ontime.ontime.views.activities.chatBox;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import com.ontime.ontime.di.scope.ActivityScope;
import com.ontime.ontime.store.repo.AppRepo;

import javax.inject.Inject;

@ActivityScope
public class MessageBoxViewModelProvider extends ViewModelProvider.NewInstanceFactory{

    private final AppRepo repo;

    @Inject
    public MessageBoxViewModelProvider(AppRepo repo) {
        this.repo = repo;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        return (T) new MessageBoxViewModel(repo);
    }
}
