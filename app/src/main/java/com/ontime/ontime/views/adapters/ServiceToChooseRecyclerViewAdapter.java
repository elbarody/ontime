package com.ontime.ontime.views.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.bumptech.glide.Glide;
import com.marcinorlowski.fonty.Fonty;
import com.ontime.ontime.R;
import com.ontime.ontime.di.qualifier.ForActivity;
import com.ontime.ontime.store.models.ComponentData;
import com.ontime.ontime.store.models.service.ServicesModel;
import de.hdodenhof.circleimageview.CircleImageView;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

@ForActivity
public class ServiceToChooseRecyclerViewAdapter extends RecyclerView.Adapter<ServiceToChooseRecyclerViewAdapter.ViewHolder> {
    private List<ServicesModel> mData = new ArrayList<>();
    Context context;
    private OnServiceClickedListener onServiceClickedListener;

    // data is passed into the constructor
    @Inject
    public ServiceToChooseRecyclerViewAdapter(@ForActivity Context context) {
        this.context = context;
    }

    public void pushData(List<ServicesModel> mData) {
        this.mData.clear();
        this.mData.addAll(mData);
        notifyDataSetChanged();
    }

    // inflates the row layout from xml when needed
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_rv_services, parent, false);
        Fonty.setFonts((ViewGroup) view);

        return new ViewHolder(view);
    }

    // binds the data to the TextView in each row
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        //String animal = mData.get(position)

        holder.bind(mData.get(position));
    }

    // total number of rows
    public void setOnServiceClickedListener(OnServiceClickedListener onServiceClickedListener) {
        this.onServiceClickedListener = onServiceClickedListener;
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }


    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.selectedServiceTextView)
        TextView selectedServiceTextView;
        @BindView(R.id.userImageView)
        CircleImageView userImageView;

        ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }

        public void bind(ServicesModel item) {
            selectedServiceTextView.setText(item.getName());
            Glide.with(context).load(item.getImg()).into(userImageView);
            itemView.setOnClickListener(v ->
                    onServiceClickedListener.onSelelectedChanged(item)
            );
        }
    }

    public interface OnServiceClickedListener {
        public void onSelelectedChanged(ServicesModel servicesModel);

    }
}

