package com.ontime.ontime.views.adapters;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import com.ontime.ontime.R;
import com.ontime.ontime.views.fragments.HomeFragment;

import java.util.HashMap;
import java.util.Map;

public class ViewPagerAdapter extends FragmentStatePagerAdapter {

    private String[] counter;
    private String[] titles = new String[]{"المشاريع", "قيد المراجعة", "فيد التنفيذ", "مكتمل"};
    private Context context;
    private Map<Integer, String> mFragmentTags=new HashMap<>();
    private final FragmentManager mFragmentManager;
    private HomeFragment homeFragment;

    public ViewPagerAdapter(FragmentManager fm, Context context,String[] counter) {
        super(fm);
        this.context = context;
        this.counter=counter;
        mFragmentManager=fm;
    }

    @Override
    public Fragment getItem(int i) {
        homeFragment=new HomeFragment();
        String tag = homeFragment.getTag();
        mFragmentTags.put(i, tag);

        return homeFragment;
    }

    @Override
    public int getCount() {
        return 4;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return null;
    }

    public View getTabView(int position) {
        View v = LayoutInflater.from(context).inflate(R.layout.item_tab, null);
        TextView counerTv=v.findViewById(R.id.counter);
        TextView title1 = v.findViewById(R.id.title);
        counerTv.setText(counter[position]);
        title1.setText(titles[position]);
        return v;
    }



    public Fragment getFragment(int position) {
        String tag = mFragmentTags.get(position);
        if (tag == null)
            return null;
        return mFragmentManager.findFragmentByTag(tag);
    }

    @Override
    public int getItemPosition(@NonNull Object object) {
        return 0;
    }
}
