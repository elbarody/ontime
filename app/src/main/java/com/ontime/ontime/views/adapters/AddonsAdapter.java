package com.ontime.ontime.views.adapters;

import android.content.Context;
import android.util.Log;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.marcinorlowski.fonty.Fonty;
import com.ontime.ontime.R;
import com.ontime.ontime.di.qualifier.ForActivity;
import com.ontime.ontime.store.models.service.AddonModel;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

@ForActivity
public class AddonsAdapter extends RecyclerView.Adapter<AddonsAdapter.ViewHolder> {

    private final Context context;

    private List<AddonModel> addonModels = new ArrayList<>();
    private List<AddonModel> selectedAddons = new ArrayList<>();
    private OnAddonsCheckedListener onAddonsCheckedListener;

    @Inject
    public AddonsAdapter(@ForActivity Context context) {
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v=(LayoutInflater.from(context).inflate(R.layout.item_service, viewGroup, false));
        Fonty.setFonts((ViewGroup) v);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        viewHolder.bind(addonModels.get(i));
    }

    @Override
    public int getItemCount() {
        return addonModels.size();
    }

    public void pushData(List<AddonModel> addonModels) {
        this.addonModels.clear();
        this.addonModels.addAll(addonModels);
        notifyDataSetChanged();
    }

    public void setOnAddonsCheckedListener(OnAddonsCheckedListener onAddonsCheckedListener){
        this.onAddonsCheckedListener = onAddonsCheckedListener;
    }

    public List<AddonModel> getSelectedAddons() {
        return selectedAddons;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.nameTextView)
        TextView nameTextView;

        private AddonModel addonModel;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            ImageView selectedImageView = itemView.findViewById(R.id.doneImageView);

            itemView.setOnClickListener(v -> {
                addonModel.setChecked(!addonModel.isChecked());
                selectedImageView.setVisibility(addonModel.isChecked() ? View.VISIBLE : View.GONE);
                if (addonModel.isChecked())
                    selectedAddons.add(addonModel);
                else selectedAddons.remove(addonModel);
                onAddonsCheckedListener.onSelelectedChanged(selectedAddons);
            });
        }

        public void bind(AddonModel addonModel) {
            this.addonModel = addonModel;
            nameTextView.setText(addonModel.getName());
        }
    }

    public interface OnAddonsCheckedListener {

        public void onSelelectedChanged(List<AddonModel> addonModels);
    }
}
