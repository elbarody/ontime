package com.ontime.ontime.views.activities.chatBox;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.LinearSmoothScroller;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.OnClick;

import com.github.nkzawa.socketio.client.Socket;
import com.jaiselrahman.filepicker.activity.FilePickerActivity;
import com.jaiselrahman.filepicker.config.Configurations;
import com.jaiselrahman.filepicker.model.MediaFile;
import com.ontime.ontime.OnTimeApplication;
import com.ontime.ontime.R;
import com.ontime.ontime.base.BaseActivity;
import com.ontime.ontime.di.activity.ActivityModule;
import com.ontime.ontime.store.models.MessageData;
import com.ontime.ontime.store.models.user.UserData;
import com.ontime.ontime.store.socketIO.SocketIOInitialization;
import com.ontime.ontime.utils.DialogBuilder;
import com.ontime.ontime.utils.SessionManager;
import com.ontime.ontime.views.activities.CongratsActivity;
import com.ontime.ontime.views.activities.PaymentActivity;
import com.ontime.ontime.views.activities.contractSignature.ContractSignatureActivity;
import com.ontime.ontime.views.activities.home.HomeActivity;
import com.ontime.ontime.views.activities.requestdetails.ProductDetailsActivity;
import com.ontime.ontime.views.adapters.MessageBoxRecyclerViewAdapter;
import org.json.JSONException;
import org.json.JSONObject;

import javax.inject.Inject;
import java.io.File;
import java.util.ArrayList;

import static android.widget.LinearLayout.VERTICAL;

public class MessageBoxActivity extends BaseActivity {
    private static final int READ_STORAGE_CODE = 1001;
    private static final int WRITE_STORAGE_CODE = 1002;
    private static final int PICK_IMAGE = 1;
    private static final int FILE_REQUEST_CODE = 400;

    @Inject
    MessageBoxViewModelProvider provider;
    @Inject
    MessageBoxRecyclerViewAdapter adapter;

    @BindView(R.id.recyclerView_chat)
    RecyclerView recyclerViewChat;
    @BindView(R.id.send)
    ImageView send;
    @BindView(R.id.write_message)
    TextView writeMessage;
    @BindView(R.id.voice)
    ImageView voice;
    @BindView(R.id.add_message)
    ImageView addMessage;
    @BindView(R.id.signature_container)
    ConstraintLayout signatureContainer;
    Socket socket;
    UserData user;
    private MessageBoxViewModel viewModel;
    private int request_id;
    private String uploadFilePath = "";
    private ArrayList<MediaFile> files = new ArrayList<>();

    @Override
    protected int getLayout() {
        return R.layout.activity_open_message;
    }

    @Override
    protected void onCreateActivityComponent() {
        OnTimeApplication.getComponent(this)
                .plus(new ActivityModule(this))
                .inject(this);

        viewModel = ViewModelProviders.of(this, provider).get(MessageBoxViewModel.class);
        initHomeUi();

        initObservers();

        viewModel.getMessages(request_id);
    }

    private void initHomeUi() {
        user = new SessionManager(this).getUserDetails();
        socket = SocketIOInitialization.getSocketInstance(user.getToken()).getSocket();
        request_id = getIntent().getIntExtra("requestId", 0);
        recyclerViewChat.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, VERTICAL, true) {

            @Override
            public void smoothScrollToPosition(RecyclerView recyclerView, RecyclerView.State state, int position) {
                LinearSmoothScroller smoothScroller = new LinearSmoothScroller(MessageBoxActivity.this) {

                    private static final float SPEED = 3000f;

                    @Override
                    protected float calculateSpeedPerPixel(DisplayMetrics displayMetrics) {
                        return SPEED / displayMetrics.densityDpi;
                    }

                };
                smoothScroller.setTargetPosition(position);
                startSmoothScroll(smoothScroller);
            }

        };
        recyclerViewChat.setLayoutManager(linearLayoutManager);
        recyclerViewChat.setAdapter(adapter);
    }


    private void initObservers() {
        viewModel.chatResponseMutableLiveData.observe(this, getChatMessageResponse -> {
            assert getChatMessageResponse != null;
            if (null != getChatMessageResponse.getData() && getChatMessageResponse.getData().size() > 0) {
                //emptyView.setVisibility(View.GONE);
                adapter.pushData(getChatMessageResponse.getData());
                signatureContainer.setVisibility(View.GONE);
            } else
                signatureContainer.setVisibility(View.VISIBLE);

        });

        viewModel.sendMessageResponseMutableLiveData.observe(this, sendMessageResponse -> {
            if (sendMessageResponse.status == 0) {
                adapter.setMessage(sendMessageResponse.getSocket());
                adapter.notifyItemInserted(0);
                recyclerViewChat.scrollToPosition(0);
                writeMessage.setText("");
                recyclerViewChat.setVisibility(View.VISIBLE);
                signatureContainer.setVisibility(View.GONE);

                /*MessageData data=sendMessageResponse.getSocket();
                JSONObject obj = new JSONObject();
                try {
                    obj.put("user_id", data.getUserId());
                    obj.put("room_id", data.getRoomId());
                    obj.put("time", data.getTime());
                    obj.put("file_type", data.getFileType());
                    obj.put("msg_type", data.getMsgType());
                    obj.put("msg_text", data.getMsgText());
                    obj.put("file_url", data.getFileUrl());
                    obj.put("request_id", data.getRequestId());


                } catch (JSONException e) {
                    e.printStackTrace();
                }
                socket.emit("user_chat_msg",obj);*/
            }
        });

        viewModel.getContractResponseLiveData.observe(this, result -> {
            if (result.getStatus() == 0)
                new DialogBuilder(this, R.layout.dailog_condational)
                        .text(R.id.conditional_content, result.getData().getText())
                        .text(R.id.title, result.getData().getName())
                        .clickListener(R.id.accept, ((dialog, view) -> {
                            dialog.dismiss();
                            startActivity(new Intent(this, ContractSignatureActivity.class).putExtra("request_id", request_id));
                        }))
                        .build()
                        .show();
            else if (result.getStatus() == 211) {
                signatureContainer.setVisibility(View.GONE);
            }
        });
    }

    @SuppressLint("WrongConstant")
    @OnClick(R.id.signature)
    public void onViewClicked() {
        viewModel.getContractDtails(request_id);

    }

    @OnClick({R.id.details, R.id.payment, R.id.call, R.id.receive_btn,
            R.id.send, R.id.write_message, R.id.voice, R.id.add_message})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.details:
                Intent intent = new Intent(this, ProductDetailsActivity.class);
                intent.putExtra("requestId", request_id);
                startActivity(intent);
                break;
            case R.id.payment:
                startActivity(new Intent(this, PaymentActivity.class));
                break;
            case R.id.call:
                break;
            case R.id.receive_btn:
                startActivity(new Intent(this, CongratsActivity.class));
                break;
            case R.id.send:

                if (writeMessage.getText().toString().equals("")) {
                    showErrorMessage("please type your message");
                    return;
                }
                viewModel.sendMessage(request_id, writeMessage.getText().toString(), "no_file", "");
                break;
            case R.id.write_message:
                break;
            case R.id.voice:
                break;
            case R.id.add_message:
                openImagePickerIntent();

                break;
        }
    }

    private void openImagePickerIntent() {
        Intent intent = new Intent(this, FilePickerActivity.class);
        intent.putExtra(FilePickerActivity.CONFIGS, new Configurations.Builder()
                .setCheckPermission(true)
                .setShowImages(true)
                .enableVideoCapture(true)
                .setShowFiles(true)
                .setShowVideos(true)
                .enableImageCapture(true)
                .setMaxSelection(1)

                .setSkipZeroSizeFiles(true)
                .build());
        startActivityForResult(intent, FILE_REQUEST_CODE);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            if (requestCode == FILE_REQUEST_CODE) {
                files = data.getParcelableArrayListExtra(FilePickerActivity.MEDIA_FILES);
                viewModel.sendMessage(request_id, "", "img", files.get(0).getPath());

                Log.d("minaLog", String.valueOf(files.size()));
            }
        }
    }

}