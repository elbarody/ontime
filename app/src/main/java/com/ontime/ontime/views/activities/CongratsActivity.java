package com.ontime.ontime.views.activities;

import com.ontime.ontime.R;
import com.ontime.ontime.base.BaseActivity;

public class CongratsActivity extends BaseActivity {
    @Override
    protected int getLayout() {
        return R.layout.activity_congrats;
    }

    @Override
    protected void onCreateActivityComponent() {

    }
}