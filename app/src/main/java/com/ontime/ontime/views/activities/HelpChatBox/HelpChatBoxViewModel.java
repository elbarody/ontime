package com.ontime.ontime.views.activities.HelpChatBox;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import com.ontime.ontime.store.models.GetChatResponse;
import com.ontime.ontime.store.models.SendMessageResponse;
import com.ontime.ontime.store.repo.AppRepo;

public class HelpChatBoxViewModel extends ViewModel {

    private final AppRepo repo;
    MutableLiveData<GetChatResponse> chatResponseMutableLiveData;
    MutableLiveData<SendMessageResponse> sendMessageResponseMutableLiveData;

    HelpChatBoxViewModel(AppRepo repo) {
        this.repo = repo;
        this.sendMessageResponseMutableLiveData = repo.sendMessageResponseMutableLiveData;
        this.chatResponseMutableLiveData = repo.chatResponseMutableLiveData;
    }


    @Override
    protected void onCleared() {
        repo.dispose();
        super.onCleared();
    }

    void getMessages(int dept_id) {
        repo.getHelpChat(dept_id);
    }

    void sendMessage(int dept_id, String msgTxt, String file_type, String file) {
        repo.sendHelpMessage(dept_id, msgTxt, file_type, file);
    }


}
