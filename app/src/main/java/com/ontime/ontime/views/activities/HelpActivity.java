package com.ontime.ontime.views.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.ontime.ontime.R;
import com.ontime.ontime.base.BaseActivity;
import com.ontime.ontime.store.models.department.DepartmentModel;

import butterknife.ButterKnife;
import butterknife.OnClick;
import com.ontime.ontime.views.activities.HelpChatBox.HelpChatBoxActivity;

import java.io.Serializable;

public class HelpActivity extends BaseActivity {

    private DepartmentModel departmentModel;

    @Override
    protected int getLayout() {
        return R.layout.activity_help;
    }

    @Override
    protected void onCreateActivityComponent() {
        departmentModel = (DepartmentModel) getIntent().getSerializableExtra("dept_item");

    }

    @OnClick({R.id.backBtn, R.id.direct_call, R.id.help_chat})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.backBtn:
                this.finish();
                break;
            case R.id.direct_call:
                break;
            case R.id.help_chat:
                startActivity(new Intent(this, HelpChatBoxActivity.class).
                        putExtra("dept_item", (Serializable) departmentModel));
                break;
        }
    }
}
