package com.ontime.ontime.views.activities.newpassword;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import com.ontime.ontime.store.repo.AppRepo;

public class NewPasswordViewModel extends ViewModel {

    public MutableLiveData<Boolean> NewPasswordSuccess;
    private final AppRepo repo;

    public NewPasswordViewModel(AppRepo repo) {
        this.repo = repo;
        this.NewPasswordSuccess = repo.newPasswordSuccess;
    }

    public void newPassword(String pass) {
        repo.newPassword(pass);
    }

    @Override
    protected void onCleared() {
        repo.dispose();
        super.onCleared();
    }


}
