package com.ontime.ontime.views.activities.home;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import androidx.annotation.NonNull;
import com.ontime.ontime.store.repo.AppRepo;

import javax.inject.Inject;

public class HomeViewModelProvider extends ViewModelProvider.NewInstanceFactory {
    private final AppRepo repo;

    @Inject
    public HomeViewModelProvider(AppRepo repo) {
        this.repo = repo;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        return (T) new HomeViewModel(repo);
    }
}
