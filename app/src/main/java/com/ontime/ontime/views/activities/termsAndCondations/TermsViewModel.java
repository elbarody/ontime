package com.ontime.ontime.views.activities.termsAndCondations;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import com.ontime.ontime.store.models.WhoAreYouResponse;
import com.ontime.ontime.store.repo.AppRepo;

public class TermsViewModel extends ViewModel {

    public MutableLiveData<WhoAreYouResponse> whoAreYouResponseMutableLiveData;

    private final AppRepo repo;

    public TermsViewModel(AppRepo repo) {
        this.repo = repo;
        this.whoAreYouResponseMutableLiveData=repo.whoAreYouResponseMutableLiveData;
    }




    @Override
    protected void onCleared() {
        repo.dispose();
        super.onCleared();
    }


    public void getAboutUs() {
        repo.getTerms();

    }
}
