package com.ontime.ontime.views.adapters;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.marcinorlowski.fonty.Fonty;
import com.ontime.ontime.R;

public class PaymentServicesAdapter extends RecyclerView.Adapter<PaymentServicesAdapter.ViewHolder> {

    private final Context context;
    private final LayoutInflater mInflater;

    public PaymentServicesAdapter(Context context) {
        this.context = context;
        this.mInflater = LayoutInflater.from(context);

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = mInflater.inflate(R.layout.item_payment_service, viewGroup, false);
        Fonty.setFonts((ViewGroup) view);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {

    }

    @Override
    public int getItemCount() {
        return 3;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ImageView selectedImageView = itemView.findViewById(R.id.doneImageView);

            itemView.setOnClickListener(v -> selectedImageView.setVisibility(itemView.VISIBLE));
        }
    }
}
