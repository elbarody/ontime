package com.ontime.ontime.views.activities.notification;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import com.ontime.ontime.store.models.GetNotificationResponseModel;
import com.ontime.ontime.store.repo.AppRepo;

public class NotificationViewModel extends ViewModel {

    public MutableLiveData<GetNotificationResponseModel> notificationResponseModelMutableLiveData;
    private final AppRepo repo;


    public NotificationViewModel(AppRepo repo) {
        this.repo = repo;
        this.notificationResponseModelMutableLiveData = repo.notificationResponseModelMutableLiveData;
    }

    public void getNotification() {
        repo.getUserNotification();
    }

    @Override
    protected void onCleared() {
        repo.dispose();
        super.onCleared();
    }


}
