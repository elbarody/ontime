package com.ontime.ontime.views.activities.aboutActivity;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import com.ontime.ontime.store.models.WhoAreYouResponse;
import com.ontime.ontime.store.repo.AppRepo;

public class AboutUsViewModel extends ViewModel {

    public MutableLiveData<WhoAreYouResponse> whoAreYouResponseMutableLiveData;

    private final AppRepo repo;

    public AboutUsViewModel(AppRepo repo) {
        this.repo = repo;
        this.whoAreYouResponseMutableLiveData=repo.whoAreYouResponseMutableLiveData;
    }




    @Override
    protected void onCleared() {
        repo.dispose();
        super.onCleared();
    }


    public void getAboutUs() {
        repo.getAboutUs();

    }
}
