package com.ontime.ontime.views.activities.register;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;
import androidx.lifecycle.ViewModelProviders;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.ontime.ontime.OnTimeApplication;
import com.ontime.ontime.R;
import com.ontime.ontime.base.BaseActivity;
import com.ontime.ontime.di.activity.ActivityModule;
import com.ontime.ontime.di.scope.ActivityScope;
import com.ontime.ontime.views.activities.activation.ActivationActivity;
import com.ontime.ontime.views.activities.termsAndCondations.TermsActivity;
import com.rilixtech.widget.countrycodepicker.CountryCodePicker;

import javax.inject.Inject;

@ActivityScope
public class RegistrationActivity extends BaseActivity {

    @Inject
    RegisterViewModelProvider provider;

    @BindView(R.id.company_name_et)
    EditText companyNameEditText;
    @BindView(R.id.user_email_et)
    EditText emailEditText;
    @BindView(R.id.user_name_et)
    EditText userNameEditText;
    @BindView(R.id.user_phone_et)
    EditText phoneEditText;
    @BindView(R.id.password_et)
    EditText passwordEditText;
    @BindView(R.id.confirm_password_et)
    EditText conPasswordEditText;
    @BindView(R.id.radioButton2)
    RadioButton userRadioButton;
    @BindView(R.id.radioButton)
    RadioButton companyRadioButton;
    @BindView(R.id.circleView)
    View circleView;
    @BindView(R.id.circleView2)
    View circleView2;
    @BindView(R.id.doneImageView)
    ImageView doneImageView;
    @BindView(R.id.doneImageView2)
    ImageView doneImageView2;
    @BindView(R.id.terms)
    TextView terms;
    @BindView(R.id.ccp)
    CountryCodePicker ccp;


    private int ability = 1;
    private RegisterViewModel viewModel;
    private boolean termsAccepted = false;
    private boolean companyPill = false;

    @Override
    protected int getLayout() {
        return R.layout.activity_registration;
    }

    @Override
    protected void onCreateActivityComponent() {

        OnTimeApplication.getComponent(this)
                .plus(new ActivityModule(this))
                .inject(this);

        viewModel = ViewModelProviders.of(this, provider).get(RegisterViewModel.class);

        initUi();

        initObservers();

    }


    private void initObservers() {
        viewModel.registerSuccess.observe(this, code -> {
            if (!TextUtils.isEmpty(code)) {
                showSuccessMessage(code);
                Intent intent = new Intent(this, ActivationActivity.class);
//                intent.putExtra("code", code);
                startActivity(intent);
            }
        });

    }

    void initUi() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            terms.setText(Html.fromHtml(getString(R.string.i_agree_to_the_site_policy_and_terms_of_use), Html.FROM_HTML_MODE_COMPACT));
        } else {
            terms.setText(Html.fromHtml(getString(R.string.i_agree_to_the_site_policy_and_terms_of_use)));
        }

        userRadioButton.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                ability = 0;
                companyRadioButton.setChecked(false);
                companyNameEditText.setFocusable(false);
            } else {
                ability = 1;
                companyRadioButton.setChecked(true);
                companyNameEditText.setFocusable(true);
            }
        });

        companyRadioButton.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                ability = 1;
                userRadioButton.setChecked(false);
                companyNameEditText.setFocusable(true);
            } else {
                ability = 0;
                userRadioButton.setChecked(true);
                companyNameEditText.setFocusable(false);
            }
        });

        doneImageView.setVisibility(View.GONE);

        circleView.setOnClickListener(v -> {
            if (companyPill) {
                companyPill = false;
                doneImageView.setVisibility(View.GONE);
            } else {
                companyPill = true;
                doneImageView.setVisibility(View.VISIBLE);
            }
        });

        doneImageView2.setVisibility(View.GONE);

        circleView2.setOnClickListener(v -> {
            if (termsAccepted) {
                termsAccepted = false;
                doneImageView2.setVisibility(View.GONE);
            } else {
                termsAccepted = true;
                doneImageView2.setVisibility(View.VISIBLE);
            }
        });


    }

    @OnClick(R.id.button_login)
    void onRegisterClicked() {
        if (ability == 1 && TextUtils.isEmpty(companyNameEditText.getText().toString())) {
            showErrorMessage("please type company name");
            return;
        }

        if (TextUtils.isEmpty(userNameEditText.getText().toString())) {
            showErrorMessage("please type username");
            return;
        }

        if (TextUtils.isEmpty(userNameEditText.getText().toString())) {
            showErrorMessage("please type username");
            return;
        }

        if (!com.ontime.ontime.utils.TextUtils.isValidEmail(emailEditText.getText().toString())) {
            showErrorMessage("Invalid Email ");
            return;
        }

        if (!com.ontime.ontime.utils.TextUtils.isValidPhone(phoneEditText.getText().toString())) {
            showErrorMessage("Invalid phone number ");
            return;
        }

        if (!com.ontime.ontime.utils.TextUtils.isValidPassword(passwordEditText.getText().toString())) {
            showErrorMessage("Invalid password ");
            return;
        }

        if (!conPasswordEditText.getText().toString().equals(passwordEditText.getText().toString())) {
            showErrorMessage("passwords is mismatching");
            return;
        }

        if (!termsAccepted) {
            showErrorMessage("please check terms and conditional");
            return;
        }

        viewModel.register(ability, companyNameEditText.getText().toString(),
                userNameEditText.getText().toString(),
                emailEditText.getText().toString(),
                ccp.getDefaultCountryCode()+phoneEditText.getText().toString(),
                passwordEditText.getText().toString());
    }

    @OnClick(R.id.terms)
    public void onViewClicked() {
        startActivity(new Intent(this, TermsActivity.class));
    }
}
