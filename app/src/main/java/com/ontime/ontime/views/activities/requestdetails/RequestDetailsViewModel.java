package com.ontime.ontime.views.activities.requestdetails;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import com.ontime.ontime.store.models.RequestResponseModel;
import com.ontime.ontime.store.models.requestdetails.RequestDetailsData;
import com.ontime.ontime.store.repo.AppRepo;

public class RequestDetailsViewModel extends ViewModel {

    private final AppRepo repo;
    public MutableLiveData<RequestDetailsData> requestDetailsLiveData;

    public RequestDetailsViewModel(AppRepo repo) {
        this.repo = repo;
        this.requestDetailsLiveData = repo.requestDetailsLiveData;
    }

    public void getDetails(int requestId) {
        repo.getRequestDetails(requestId);
    }

    @Override
    protected void onCleared() {
        repo.dispose();
        super.onCleared();
    }


}
