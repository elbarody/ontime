package com.ontime.ontime.views.activities.editProfile;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.ViewModelProviders;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.bumptech.glide.Glide;
import com.ontime.ontime.OnTimeApplication;
import com.ontime.ontime.R;
import com.ontime.ontime.base.BaseActivity;
import com.ontime.ontime.di.activity.ActivityModule;
import com.ontime.ontime.store.models.user.UserData;
import com.ontime.ontime.utils.SessionManager;
import com.ontime.ontime.views.activities.activation.ActivationActivity;
import com.ontime.ontime.views.activities.contractSignature.ContractSignatureActivity;
import com.ontime.ontime.views.activities.profileData.ProfileActivity;
import com.ontime.ontime.views.activities.register.RegisterViewModel;
import com.ontime.ontime.views.activities.register.RegisterViewModelProvider;
import com.xwady.core.models.User;
import de.hdodenhof.circleimageview.CircleImageView;

import javax.inject.Inject;

public class EditProfileActivity extends BaseActivity {
    private static final int READ_STORAGE_CODE = 1001;
    private static final int WRITE_STORAGE_CODE = 1002;
    private static final int PICK_IMAGE = 1;

    @Inject
    EditProfileViewModelProvider provider;

    @BindView(R.id.userImageView)
    CircleImageView userImageView;
    @BindView(R.id.radioButton)
    RadioButton companyRadioButton;
    @BindView(R.id.company_name_et)
    EditText companyNameEt;
    @BindView(R.id.radioButton2)
    RadioButton userRadioButton;
    @BindView(R.id.user_name_et)
    EditText userNameEt;
    @BindView(R.id.user_email_et)
    EditText userEmailEt;
    @BindView(R.id.user_phone_et)
    EditText userPhoneEt;
    @BindView(R.id.password_et)
    EditText passwordEt;
    @BindView(R.id.confirm_password_et)
    EditText confirmPasswordEt;
    @BindView(R.id.rgDefault)
    RadioGroup rgDefault;

    private int ability = 1;
    private EditProfileViewModel viewModel;
    private UserData user;

    private String uploadFilePath="";


    @Override
    protected int getLayout() {
        return R.layout.activity_edit_profile;
    }

    @Override
    protected void onCreateActivityComponent() {

        OnTimeApplication.getComponent(this)
                .plus(new ActivityModule(this))
                .inject(this);

        viewModel = ViewModelProviders.of(this, provider).get(EditProfileViewModel.class);

        initUi();

        initObservers();

    }


    private void initObservers() {
        viewModel.editProfileResponse.observe(this, code -> {
            if (!(code.getStatus()==0)) {
                showSuccessMessage(code.getMsg());

            }else {
                showSuccessMessage(code.getMsg());
                EditProfileActivity.this.finish();
            }

        });

    }

    void initUi() {

        user=new SessionManager(this).getUserDetails();

        if (user.getCompanyName()!=null&&!user.getCompanyName().equals("")){
            companyNameEt.setText(user.getCompanyName());
        }if (user.getUserType().equals("company")){
            ability = 1;
            userRadioButton.setChecked(false);
            companyNameEt.setFocusable(true);
            user.setUserType("company");
        }else {
            ability = 0;
            userRadioButton.setChecked(true);
            companyNameEt.setFocusable(false);
            user.setUserType("single");

        }
        userEmailEt.setText(user.getEmail());
        userPhoneEt.setText(user.getPhone());
        userNameEt.setText(user.getName());

        userRadioButton.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                ability = 0;
                companyRadioButton.setChecked(false);
                companyNameEt.setFocusable(false);
                user.setUserType("single");

            } else {
                ability = 1;
                companyRadioButton.setChecked(true);
                companyNameEt.setFocusable(true);
                user.setUserType("company");

            }
        });

        companyRadioButton.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                ability = 1;
                userRadioButton.setChecked(false);
                companyNameEt.setFocusable(true);
            } else {
                ability = 0;
                userRadioButton.setChecked(true);
                companyNameEt.setFocusable(false);
            }
        });


        Glide.with(this).load(user.getImg()).error(R.drawable.person_icon).into(userImageView);


    }

    private void openImagePickerIntent() {
        if (isPermissionGranted(Manifest.permission.READ_EXTERNAL_STORAGE)) {
            Intent intent = new Intent(Intent.ACTION_PICK,
                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(intent, PICK_IMAGE);
        } else
            requestPermission(Manifest.permission.READ_EXTERNAL_STORAGE, READ_STORAGE_CODE);

    }

    @OnClick({R.id.backBtn, R.id.pick_img, R.id.edit_profile})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.backBtn:
                break;
            case R.id.pick_img:
                openImagePickerIntent();

                break;
            case R.id.edit_profile:
                if (ability == 1 && TextUtils.isEmpty(companyNameEt.getText().toString())) {
                    showErrorMessage("please type company name");
                    return;
                }

                if (TextUtils.isEmpty(userNameEt.getText().toString())) {
                    showErrorMessage("please type username");
                    return;
                }

                if (!com.ontime.ontime.utils.TextUtils.isValidEmail(userEmailEt.getText().toString())) {
                    showErrorMessage("Invalid Email ");
                    return;
                }

                if (!com.ontime.ontime.utils.TextUtils.isValidPhone(userPhoneEt.getText().toString())) {
                    showErrorMessage("Invalid phone number ");
                    return;
                }

                if (!com.ontime.ontime.utils.TextUtils.isValidPassword(passwordEt.getText().toString())) {
                    showErrorMessage("Invalid password ");
                    return;
                }

                if (!confirmPasswordEt.getText().toString().equals(passwordEt.getText().toString())) {
                    showErrorMessage("passwords is mismatching");
                    return;
                }

                viewModel.editProfile(ability, companyNameEt.getText().toString(),
                        userNameEt.getText().toString(),
                        userEmailEt.getText().toString(),
                        userPhoneEt.getText().toString(),
                        uploadFilePath);
                break;

        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_IMAGE) {
            Uri selectedImage = data.getData();
            String[] filePathColumn = {MediaStore.Images.Media.DATA};
            Cursor cursor = getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            cursor.moveToFirst();
            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            uploadFilePath = cursor.getString(columnIndex);
            Glide.with(this)
                    .load(uploadFilePath)
                    .centerCrop()
                    .into(userImageView);
            cursor.close();

        } else {
            Toast.makeText(EditProfileActivity.this, "You haven't picked Image", Toast.LENGTH_LONG).show();
        }


    }

    private boolean isPermissionGranted(String permission) {
        //Getting the permission status
        int result = ContextCompat.checkSelfPermission(this, permission);

        //If permission is granted returning true
        if (result == PackageManager.PERMISSION_GRANTED)
            return true;

        //If permission is not granted returning false
        return false;
    }


    private void requestPermission(String permission, int code) {

        if (ActivityCompat.shouldShowRequestPermissionRationale((Activity) this, permission)) {
            //If the user has denied the permission previously your code will come to this block
            //Here you can explain why you need this permission
            //Explain here why you need this permission
        }
        //And finally ask for the permission
        ActivityCompat.requestPermissions((Activity) this, new String[]{permission}, code);
    }

    //This method will be called when the user will tap on allow or deny
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        //Checking the request code of our request
        if (requestCode == READ_STORAGE_CODE) {

            //If permission is granted
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {


                openImagePickerIntent();


            } else {

                //mContext.get.finish();
            }
        } else if (requestCode == WRITE_STORAGE_CODE) {


            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

            } else {

                //mContext.finish();
            }
        }
    }
}
