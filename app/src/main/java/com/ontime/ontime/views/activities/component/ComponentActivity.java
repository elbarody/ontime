package com.ontime.ontime.views.activities.component;

import android.view.View;
import android.widget.TextView;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.OnClick;
import com.ontime.ontime.OnTimeApplication;
import com.ontime.ontime.R;
import com.ontime.ontime.base.BaseActivity;
import com.ontime.ontime.di.activity.ActivityModule;
import com.ontime.ontime.views.adapters.ComponentItemsRecyclerViewAdapter;

import javax.inject.Inject;

public class ComponentActivity extends BaseActivity {
    @Inject
    ComponentItemsRecyclerViewAdapter adapter;
    @Inject
    ComponentViewModelProvider provider;
    @BindView(R.id.total_price_et)
    TextView totalPriceEt;
    @BindView(R.id.tex_tv)
    TextView texTv;
    @BindView(R.id.rv)
    RecyclerView rv;

    private ComponentViewModel viewModel;
    private int request_id;

    @Override
    protected int getLayout() {
        return R.layout.activity_component;
    }

    @Override
    protected void onCreateActivityComponent() {
        OnTimeApplication.getComponent(this)
                .plus(new ActivityModule(this))
                .inject(this);
        viewModel = ViewModelProviders.of(this, provider).get(ComponentViewModel.class);
        initHomeUi();

        initObservers();

        viewModel.getComponent(request_id);
    }

    private void initHomeUi() {
        request_id = getIntent().getIntExtra("request_id", 0);
        rv.setLayoutManager(new LinearLayoutManager(this));
        rv.setAdapter(adapter);
    }


    private void initObservers() {
        viewModel.componentResponseMutableLiveData.observe(this, getComponentResponse -> {
            assert getComponentResponse != null;
            if (getComponentResponse.getData().size() > 0)
                //emptyView.setVisibility(View.GONE);
                adapter.pushData(getComponentResponse.getData());
            texTv.setText(getComponentResponse.getTax());
            totalPriceEt.setText(getComponentResponse.getTotal() + "");

        });

        viewModel.actionSuccess.observe(this, success ->
                {
                    if (success) {
                        this.finish();
                    }
                }
        );
    }

    @OnClick({R.id.backBtn, R.id.accept, R.id.reject})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.backBtn:
                break;
            case R.id.accept:
                viewModel.getAcceptOrRefuseComponent(request_id, "accepted");
                break;
            case R.id.reject:
                viewModel.getAcceptOrRefuseComponent(request_id, "refused");

                break;
        }
    }
}
