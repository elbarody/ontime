package com.ontime.ontime.views.activities;

import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Html;
import android.widget.TextView;
import com.ontime.ontime.R;
import com.ontime.ontime.views.adapters.PaymentServicesAdapter;

public class PaymentActivity extends AppCompatActivity {

    //for payment
    TextView dateOne;
    TextView dateTwo;
    TextView datethree;
    TextView dateFour;
    TextView payButton;
    RecyclerView servicesRecycler;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);
        initPaymentUi();
    }

    private void initPaymentUi() {
        dateOne = findViewById(R.id.date_one);
        dateTwo = findViewById(R.id.date_two);
        dateFour = findViewById(R.id.date_four);
        datethree = findViewById(R.id.date_three);
        payButton = findViewById(R.id.payButton);
        servicesRecycler = findViewById(R.id.servicesRecycler);

        servicesRecycler.setLayoutManager(new LinearLayoutManager(this));
        servicesRecycler.setAdapter(new PaymentServicesAdapter(this));

        payButton.setOnClickListener(v -> {
            startActivity(new Intent(this, PaymentOptionsActivity.class));
        });
        datethree.setText(Html.fromHtml("الاولى".concat("<br/>")
                .concat("11 يناير 2019")));
        dateTwo.setText(Html.fromHtml("الثانية".concat("<br/>")
                .concat("11 يناير 2019")));
        dateFour.setText(Html.fromHtml("الثالثة".concat("<br/>")
                .concat("11 يناير 2019")));
        dateOne.setText(Html.fromHtml("الاخيرة".concat("<br/>")
                .concat("11 يناير 2019")));
    }
}
