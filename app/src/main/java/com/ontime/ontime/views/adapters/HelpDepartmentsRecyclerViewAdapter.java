package com.ontime.ontime.views.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.marcinorlowski.fonty.Fonty;
import com.ontime.ontime.R;
import com.ontime.ontime.di.qualifier.ForActivity;
import com.ontime.ontime.store.models.department.DepartmentModel;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

@ForActivity
public class HelpDepartmentsRecyclerViewAdapter extends RecyclerView.Adapter<HelpDepartmentsRecyclerViewAdapter.ViewHolder> {

    private List<DepartmentModel> mData = new ArrayList<>();
    Context context;
    private OnDepartmentClickedListener onDepartmentClickedListener;

    // data is passed into the constructor
    @Inject
    public HelpDepartmentsRecyclerViewAdapter(@ForActivity Context context) {
        this.context = context;
    }

    public void pushData(List<DepartmentModel> mData) {
        this.mData.clear();
        this.mData.addAll(mData);
        notifyDataSetChanged();
    }


    // inflates the row layout from xml when needed
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_help_dept, parent, false);
        Fonty.setFonts((ViewGroup) view);

        return new ViewHolder(view);
    }

    // binds the data to the TextView in each row
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        //String animal = mData.get(position)

        holder.bind(mData.get(position));
    }

    // total number of rows
    public void setOnDeptClickedListener(OnDepartmentClickedListener onDepartmentClickedListener) {
        this.onDepartmentClickedListener = onDepartmentClickedListener;
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }


    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.imageView10)
        ImageView dept_img;
        @BindView(R.id.textView7)
        TextView dept_name;

        ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }

        public void bind(DepartmentModel item) {
            dept_name.setText(item.getName());
            Glide.with(context).load(item.getImg()).into(dept_img);
            itemView.setOnClickListener(v ->
                    onDepartmentClickedListener.onSelelectedChanged(item)
            );
        }
    }


    public interface OnDepartmentClickedListener {
        public void onSelelectedChanged(DepartmentModel departmentModel);

    }
}

