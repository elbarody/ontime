package com.ontime.ontime.views.activities.helpCategories;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.ontime.ontime.OnTimeApplication;
import com.ontime.ontime.R;
import com.ontime.ontime.base.BaseActivity;
import com.ontime.ontime.di.activity.ActivityModule;
import com.ontime.ontime.store.models.department.DepartmentModel;
import com.ontime.ontime.views.activities.HelpActivity;
import com.ontime.ontime.views.activities.departments.DepartmentsViewModel;
import com.ontime.ontime.views.activities.departments.DepartmentsViewModelProvider;
import com.ontime.ontime.views.adapters.HelpDepartmentsRecyclerViewAdapter;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class HelpCategoriesActivity extends BaseActivity {

    @Inject
    HelpDepartmentsViewModelProvider provider;

    @Inject
    HelpDepartmentsRecyclerViewAdapter adapter;


    @BindView(R.id.rv_department)
    RecyclerView rvDepartment;

    private HelpDepartmentsViewModel viewModel;

    @Override
    protected int getLayout() {
        return R.layout.activity_help_two;
    }

    @Override
    protected void onCreateActivityComponent() {
        OnTimeApplication.getComponent(this)
                .plus(new ActivityModule(this))
                .inject(this);
        viewModel = ViewModelProviders.of(this, provider).get(HelpDepartmentsViewModel.class);

        initUi();
        initObservers();

        viewModel.getDepartments();
    }

    private void initObservers() {
        viewModel.departmentsLiveData.observe(this, departmentModels -> {

            if (departmentModels != null) {
                if (departmentModels.size() == 0) {
                    //TODO inflate emptyView
                } else {
                    adapter.pushData(departmentModels);
                }
            }
        });
    }

    private void initUi() {
        rvDepartment.setHasFixedSize(true);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(this, 2);
        rvDepartment.setLayoutManager(gridLayoutManager);
        rvDepartment.setAdapter(adapter);
        adapter.setOnDeptClickedListener(departmentModel ->
                startActivity(new Intent(this, HelpActivity.class).putExtra("dept_item", (Serializable) departmentModel))
        );
    }


    @OnClick(R.id.backBtn)
    public void onViewClicked() {
        this.finish();
    }
}
