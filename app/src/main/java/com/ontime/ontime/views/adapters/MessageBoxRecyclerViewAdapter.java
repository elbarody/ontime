package com.ontime.ontime.views.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.bumptech.glide.Glide;
import com.github.nkzawa.socketio.client.Url;
import com.marcinorlowski.fonty.Fonty;
import com.ontime.ontime.R;
import com.ontime.ontime.di.qualifier.ForActivity;
import com.ontime.ontime.store.models.ChatData;
import com.ontime.ontime.store.models.MessageData;
import com.ontime.ontime.store.models.service.ServicesModel;
import com.ontime.ontime.store.models.user.UserData;
import com.ontime.ontime.utils.SessionManager;
import com.ontime.ontime.views.activities.VideoActivity;
import com.ontime.ontime.views.activities.WebViewActivity;
import com.stfalcon.frescoimageviewer.ImageViewer;

import javax.inject.Inject;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@ForActivity
public class MessageBoxRecyclerViewAdapter extends RecyclerView.Adapter<MessageBoxRecyclerViewAdapter.ViewHolder> {
    private static int SENDER_TYPE = 1, RECIEVER_TYPE = 2;
    private final int itemWidth;
    Context context;
    private List<MessageData> mData = new ArrayList<>();
    private OnServiceClickedListener onServiceClickedListener;


    // data is passed into the constructor
    @Inject
    public MessageBoxRecyclerViewAdapter(@ForActivity Context context) {
        this.context = context;
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        itemWidth = (displayMetrics.widthPixels) - ((displayMetrics.widthPixels) / 5);
    }

    public void pushData(List<MessageData> mData) {
        this.mData.clear();
        this.mData.addAll(mData);
        Collections.reverse(this.mData);

        notifyDataSetChanged();
    }

    public void setMessage(MessageData socket) {
        MessageData chatData = new MessageData();
        chatData.setMsg_content(socket.getMsg_content());
        chatData.setFile_extension(socket.getFile_extension());
        chatData.setFile(socket.getFile());
        chatData.setMsg_type(socket.getMsg_type());
        chatData.setCreated_at(socket.getCreated_at());
        chatData.setModel(socket.getModel());
        mData.add(0, chatData);
    }


    @Override
    public int getItemViewType(int position) {
        if (mData.get(position).getModel().equals("user"))
            return SENDER_TYPE;
        else
            return RECIEVER_TYPE;

        //return super.getItemViewType(position);
    }

    // inflates the row layout from xml when needed
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = null;
        if (viewType == RECIEVER_TYPE) {
            view = LayoutInflater.from(context).inflate(R.layout.item_chat_left, parent, false);
            Fonty.setFonts((ViewGroup) view);

            return new ViewHolder(view);

        }
        view = LayoutInflater.from(context).inflate(R.layout.item_chat_right, parent, false);
        Fonty.setFonts((ViewGroup) view);

        return new ViewHolder(view);
    }

    // binds the data to the TextView in each row
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bind(mData.get(position));

        /*holder.userChat.setMaxWidth(itemWidth);
        holder.chatImgId.setX(itemWidth);
        holder.chatImgId.setY(itemWidth);*/

    }

    // total number of rows
    public void setOnServiceClickedListener(OnServiceClickedListener onServiceClickedListener) {
        this.onServiceClickedListener = onServiceClickedListener;
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }


    public interface OnServiceClickedListener {
        public void onSelelectedChanged(ServicesModel servicesModel);

    }

    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.user_chat)
        TextView userChat;
        @BindView(R.id.time)
        TextView time;
        @BindView(R.id.chat_img_id)
        ImageView chatImgId;
        @BindView(R.id.play_view)
        ImageView play_view;

        ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void bind(MessageData item) {
            if (item.getMsg_type().equals("text")) {
                chatImgId.setVisibility(View.GONE);
                userChat.setVisibility(View.VISIBLE);
                userChat.setText(item.getMsg_content());
            } else if (item.getMsg_type().equals("file")) {
                URL url = null;
                try {
                    url = Url.parse(item.getFile());
                } catch (URISyntaxException e) {
                    e.printStackTrace();
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                }
                userChat.setVisibility(View.GONE);
                chatImgId.setVisibility(View.VISIBLE);
                Glide.with(context).load(url.toString()).error(R.drawable.add).centerCrop().into(chatImgId);

                if (item.getFile_extension().equals("png") || item.getFile_extension().equals("jpeg") || item.getFile_extension().equals("jpg")) {
                    play_view.setVisibility(View.GONE);
                    chatImgId.setOnClickListener(v -> {
                        new ImageViewer.Builder(context, Collections.singletonList(item.getFile()))
                                .show();
                    });

                } else if (item.getFile_extension().equals("mp4") || item.getFile_extension().equals("m4a")) {
                    play_view.setVisibility(View.VISIBLE);
                    String finalUrl = String.valueOf(url);
                    play_view.setOnClickListener(v ->
                            context.startActivity(new Intent(context, VideoActivity.class).putExtra("file", finalUrl))
                    );
                }else{
                    userChat.setVisibility(View.VISIBLE);
                    chatImgId.setVisibility(View.GONE);
                    play_view.setVisibility(View.GONE);
                    userChat.setText(url.getFile().subSequence(url.getFile().lastIndexOf("/")+1,url.getFile().length()));
                    String finalUrl1 = url.toString();
                    userChat.setOnClickListener(v ->
                            context.startActivity(new Intent(context, WebViewActivity.class).putExtra("file", finalUrl1))
                    );
                }


            }
            time.setText(item.getCreated_at());

            /*itemView.setOnClickListener(v ->
                    onServiceClickedListener.onSelelectedChanged(item)
            );*/
        }
    }
}

