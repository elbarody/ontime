package com.ontime.ontime.views.activities.signatureCode;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProviders;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.google.android.material.textfield.TextInputEditText;
import com.ontime.ontime.OnTimeApplication;
import com.ontime.ontime.R;
import com.ontime.ontime.base.BaseActivity;
import com.ontime.ontime.di.activity.ActivityModule;
import com.ontime.ontime.views.activities.PaymentActivity;
import com.ontime.ontime.views.activities.home.HomeActivity;
import com.ontime.ontime.views.activities.login.LoginViewModel;
import com.ontime.ontime.views.activities.login.LoginViewModelProvider;
import com.xwady.core.helpers.Utils;

import javax.inject.Inject;

public class SignatureCodeActivity extends BaseActivity {
    @Inject
    SignatureViewModelProvider provider;

    @BindView(R.id.id)
    TextInputEditText code_edittext;

    private SignatureCodeViewModel viewModel;
    private int contract_id;


    @Override
    protected int getLayout() {
        return R.layout.activity_signature_code;
    }

    @Override

    protected void onCreateActivityComponent() {
        OnTimeApplication.getComponent(this)
                .plus(new ActivityModule(this))
                .inject(this);
        viewModel = ViewModelProviders.of(this, provider).get(SignatureCodeViewModel.class);

        contract_id = getIntent().getIntExtra("contract_id", 0);
        processGetCode();
        initObservers();
    }


    private void initObservers() {
        viewModel.isActionSuccess.observe(this, state -> {
            if (state) {
                startActivity(new Intent(this, HomeActivity.class));
                this.finish();
            }
        });

        viewModel.isCodeSent.observe(this, state -> {
            if (state) {
                Toast.makeText(this, "code send successfully", Toast.LENGTH_SHORT).show();
                viewModel.isCodeSent.setValue(false);
            }
        });

    }


    private void processCheckCode() {
        String code = code_edittext.getText().toString().trim();
        if (code.equals("")) {
            showErrorMessage("Invalid code ");
            return;
        }
        viewModel.signatureCode(code, contract_id);
    }

    private void processGetCode() {
        viewModel.signatureGetCode(contract_id);
    }

    @OnClick({R.id.backBtn, R.id.resend_code, R.id.accept})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.backBtn:
                break;
            case R.id.resend_code:
                processGetCode();
                break;
            case R.id.accept:
                processCheckCode();
                break;
        }
    }
}
