package com.ontime.ontime.views.activities.login;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import com.ontime.ontime.store.models.user.LoginResponse;
import com.ontime.ontime.store.repo.AppRepo;

public class LoginViewModel extends ViewModel {

    public MutableLiveData<LoginResponse> loginResponseMutableLiveData;
    private final AppRepo repo;

    public LoginViewModel(AppRepo repo) {
        this.repo = repo;
        this.loginResponseMutableLiveData = repo.loginLiveDataMutableLiveData;
    }


    @Override
    protected void onCleared() {
        repo.dispose();
        super.onCleared();
    }

    public void login(String phone, String pass) {
        repo.login(phone, pass);
    }
}
