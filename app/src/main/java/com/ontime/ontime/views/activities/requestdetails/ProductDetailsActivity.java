package com.ontime.ontime.views.activities.requestdetails;

import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.viewpager.widget.ViewPager;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.google.android.material.tabs.TabLayout;
import com.marcinorlowski.fonty.Fonty;
import com.ontime.ontime.OnTimeApplication;
import com.ontime.ontime.R;
import com.ontime.ontime.base.BaseActivity;
import com.ontime.ontime.di.activity.ActivityModule;
import com.ontime.ontime.di.scope.ActivityScope;
import com.ontime.ontime.store.models.service.AddonModel;
import com.ontime.ontime.views.activities.requestdetails.fragments.BelongFragment;
import com.ontime.ontime.views.activities.requestdetails.fragments.BillsFragment;
import com.ontime.ontime.views.activities.requestdetails.fragments.ContractsFragment;
import com.ontime.ontime.views.activities.requestdetails.fragments.InfoFragment;
import com.ontime.ontime.views.adapters.SelectedAddonsAdapter;
import com.warkiz.tickseekbar.TickSeekBar;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

@ActivityScope
public class ProductDetailsActivity extends BaseActivity {

    @Inject
    RequestDetailsViewModelProvider provider;
    @BindView(R.id.project_title)
    TextView projectTitle;
    @BindView(R.id.day)
    TextView day;
    @BindView(R.id.start)
    TextView start;
    @BindView(R.id.listener)
    TickSeekBar seekBar;
    @BindView(R.id.start_date)
    TextView startDate;
    @BindView(R.id.end_date)
    TextView endDate;

    private FragmentManager mGetSupportFragmentManager;
    private Home mHome;
    private RequestDetailsViewModel viewModel;

    @Override
    protected int getLayout() {
        return R.layout.activity_project_details;
    }

    @Override
    protected void onCreateActivityComponent() {

        OnTimeApplication.getComponent(this)
                .plus(new ActivityModule(this))
                .inject(this);
        viewModel = ViewModelProviders.of(this, provider).get(RequestDetailsViewModel.class);

        initUi();

        initObservers();

        viewModel.getDetails(getIntent().getIntExtra("requestId", 0));

    }

    private void initObservers() {
        viewModel.requestDetailsLiveData.observe(this, requestDetailsData -> {
            if (requestDetailsData != null) {
                projectTitle.setText(requestDetailsData.getServiceName());
                start.setText(requestDetailsData.getCreateTime());
                startDate.setText(requestDetailsData.getCreateTime());
                endDate.setText(requestDetailsData.getEndTime());
                day.setText(requestDetailsData.getStartFrom());
            }
        });
    }

    private void initUi() {
        mGetSupportFragmentManager = this.getSupportFragmentManager();
        ViewPager viewPager = findViewById(R.id.view_pager);
        TabLayout tab = findViewById(R.id.tableLayout);
        viewPager.setAdapter(getAdapter());
        tab.setupWithViewPager(viewPager);

        final int[] tabSelected = {
                R.drawable.info_sign_on,
                R.drawable.attachment_icon_on,
                R.drawable.profiles_1_on,
                R.drawable.credit_card_1_on};
        final int[] tabsUnselected = {
                R.drawable.info_sign,
                R.drawable.attachment_icon,
                R.drawable.profiles,
                R.drawable.credit_card_1};
        final View tabOne = LayoutInflater.from(this).inflate(R.layout.tab_custom_layout, null);
        final View tabTwo = LayoutInflater.from(this).inflate(R.layout.tab_custom_layout, null);
        final View tabThree = LayoutInflater.from(this).inflate(R.layout.tab_custom_layout, null);
        final View tabFour = LayoutInflater.from(this).inflate(R.layout.tab_custom_layout, null);
        TextView textViewOne = tabOne.findViewById(R.id.tab);
        TextView textViewTwo = tabTwo.findViewById(R.id.tab);
        TextView textViewThree = tabThree.findViewById(R.id.tab);
        TextView textViewFour = tabFour.findViewById(R.id.tab);
        final TextView[] tabTextViews = {
                textViewOne, textViewTwo, textViewThree, textViewFour
        };
        tab.post(() -> {
            tabTextViews[0].setCompoundDrawablesWithIntrinsicBounds(0, tabSelected[0], 0, 0);
            tabTextViews[0].setText("نظرة عامة");
            tabTextViews[1].setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.attachment_icon, 0, 0);
            tabTextViews[1].setText("مرفقات المشروع");
            tabTextViews[2].setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.profiles, 0, 0);
            tabTextViews[2].setText("العقود");
            tabTextViews[3].setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.credit_card_1, 0, 0);
            tabTextViews[3].setText("فاتورة المشروع");


            tab.setupWithViewPager(viewPager);
            tab.getTabAt(0).setCustomView(tabTextViews[0]);
            tab.getTabAt(1).setCustomView(tabTextViews[1]);
            tab.getTabAt(2).setCustomView(tabTextViews[2]);
            tab.getTabAt(3).setCustomView(tabTextViews[3]);

            Fonty.setFonts(tab);

        });

        tab.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab selectedTab) {
                for (int i = 0; i < 4; i++) {
                    if (i == selectedTab.getPosition()) {
                        tabTextViews[i].setCompoundDrawablesWithIntrinsicBounds(0, tabSelected[i], 0, 0);
                    }
                    else
                        tabTextViews[i].setCompoundDrawablesWithIntrinsicBounds(0, tabsUnselected[i], 0, 0);

                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        viewPager.getAdapter().notifyDataSetChanged();
        tab.setupWithViewPager(viewPager);
    }


    private Home getAdapter() {
        ArrayList listFragments = new ArrayList<Fragment>();
        listFragments.add(new InfoFragment());
        listFragments.add(new BelongFragment());
        listFragments.add(new ContractsFragment());
        listFragments.add(new BillsFragment());

        mHome = new Home(mGetSupportFragmentManager, listFragments);
        return mHome;
    }

    public class Home extends FragmentPagerAdapter {
        List<Fragment> mListFragment;


        public Home(FragmentManager fm, List<Fragment> listFragment) {
            super(fm);
            this.mListFragment = listFragment;
        }

        @Override
        public int getItemPosition(Object object) {
            return POSITION_NONE;
        }

        @Override
        public Fragment getItem(int position) {
            return mListFragment.get(position);
        }

        @Override
        public int getCount() {
            return mListFragment.size();
        }

    }

}