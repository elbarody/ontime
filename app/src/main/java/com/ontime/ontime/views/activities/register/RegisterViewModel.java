package com.ontime.ontime.views.activities.register;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import com.ontime.ontime.store.repo.AppRepo;

public class RegisterViewModel extends ViewModel {

    public MutableLiveData<String> registerSuccess;
    private final AppRepo repo;

    public RegisterViewModel(AppRepo repo) {
        this.repo = repo;
        this.registerSuccess = repo.registerSuccess;
    }


    @Override
    protected void onCleared() {
        repo.dispose();
        super.onCleared();
    }

    public void register(int ability, String companyName, String username, String email, String phone, String pass) {
        if (ability == 1) {
            repo.registerCompany(companyName,username,email,phone,pass);
        }else repo.registerUser(username,email,phone,pass);
    }
}
