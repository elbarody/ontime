package com.ontime.ontime.views.activities.requestconfirmation;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import com.jaiselrahman.filepicker.model.MediaFile;
import com.ontime.ontime.store.models.ContractResponse;
import com.ontime.ontime.store.models.MakeRequestResponse;
import com.ontime.ontime.store.models.service.AddonModel;
import com.ontime.ontime.store.models.service.ServicesResponse;
import com.ontime.ontime.store.repo.AppRepo;

import java.util.ArrayList;
import java.util.List;

public class ConfirmRequestViewModel extends ViewModel {

    public MutableLiveData<MakeRequestResponse> makeRequestResponseLiveData = new MutableLiveData<>();
    public MutableLiveData<ContractResponse> getContractResponseLiveData = new MutableLiveData<>();
    private final AppRepo repo;

    public ConfirmRequestViewModel(AppRepo repo) {
        this.repo = repo;
        this.makeRequestResponseLiveData = repo.makeRequestResponseLiveData;
        this.getContractResponseLiveData=repo.getContractResponseLiveData;
    }

    public void makeRequest(int depId, int serviceId, String name, String description, List<AddonModel> selectedAddons, ArrayList<MediaFile> files) {
        repo.makeRequest(depId,serviceId,name,description,selectedAddons,files);
    }

    public void getContractDtails(int request_id){
        repo.getRequestContract(request_id);
    }

    @Override
    protected void onCleared() {
        repo.dispose();
        super.onCleared();
    }

}
