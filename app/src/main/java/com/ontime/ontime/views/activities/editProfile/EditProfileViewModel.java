package com.ontime.ontime.views.activities.editProfile;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import com.ontime.ontime.store.models.GetProfileResponse;
import com.ontime.ontime.store.models.user.LoginResponse;
import com.ontime.ontime.store.repo.AppRepo;

public class EditProfileViewModel extends ViewModel {

    private final AppRepo repo;
    public final MutableLiveData<LoginResponse> editProfileResponse;

    public EditProfileViewModel(AppRepo repo) {
        this.repo = repo;
        this.editProfileResponse = repo.loginLiveDataMutableLiveData;
    }


    @Override
    protected void onCleared() {
        repo.dispose();
        super.onCleared();
    }

    public void editProfile(int ability, String companyName,
                         String username, String email,
                         String phone,String image) {
            repo.editProfile(ability,companyName,username,email,phone,image);

    }
}
