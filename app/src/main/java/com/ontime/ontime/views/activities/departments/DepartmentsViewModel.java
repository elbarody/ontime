package com.ontime.ontime.views.activities.departments;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import com.ontime.ontime.store.models.department.DepartmentModel;
import com.ontime.ontime.store.repo.AppRepo;

import java.util.List;

public class DepartmentsViewModel extends ViewModel {

    public MutableLiveData<List<DepartmentModel>> departmentsLiveData;
    private final AppRepo repo;

    public DepartmentsViewModel(AppRepo repo) {
        this.repo = repo;
        this.departmentsLiveData = repo.departmentsLiveData;
    }

    public void getDepartments() {
        repo.getDepartments();
    }


    @Override
    protected void onCleared() {
        repo.dispose();
        super.onCleared();
    }

}
