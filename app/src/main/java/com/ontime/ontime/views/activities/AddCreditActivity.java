package com.ontime.ontime.views.activities;

import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import android.text.Html;
import android.widget.TextView;
import com.ontime.ontime.R;

public class AddCreditActivity extends AppCompatActivity {

    TextView timeTextView;
    TextView priceTextView;
    TextView percentageTextView;
    TextView payButton;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_credit);

        payButton = findViewById(R.id.payButton);
        priceTextView = findViewById(R.id.priceTextView);
        timeTextView = findViewById(R.id.timeTextView);
        percentageTextView = findViewById(R.id.percentageTextView);

        payButton.setOnClickListener(v -> {
            startActivity(new Intent(this,SavedCreditsActivity.class));
        });

        priceTextView.setText(Html.fromHtml("الضريبة".concat("<br/>").concat("<br/>")
                .concat("<fonts color=#DAD93A>").concat("%5").concat("</fonts>")));

        timeTextView.setText(Html.fromHtml("الخدمة / الخدمات".concat("<br/>").concat("<br/>")
                .concat("<fonts color=#DAD93A>").concat("ر.س 144").concat("</fonts>")));

        percentageTextView.setText(Html.fromHtml("السعر الاجمالى".concat("<br/>").concat("<br/>")
                .concat("<fonts color=#DAD93A>").concat("1700").concat("</fonts>")));


    }
}
