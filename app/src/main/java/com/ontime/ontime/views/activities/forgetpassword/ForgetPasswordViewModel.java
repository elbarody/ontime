package com.ontime.ontime.views.activities.forgetpassword;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import com.ontime.ontime.store.repo.AppRepo;

public class ForgetPasswordViewModel extends ViewModel {

    public MutableLiveData<Boolean>   forgetPasswordSuccess;
    private final AppRepo repo;

    public ForgetPasswordViewModel(AppRepo repo) {
        this.repo = repo;
        this.  forgetPasswordSuccess = repo.  forgetPasswordSuccess;
    }

    public void forgetPassword(String email) {
    repo.forgetPassword(email);
    }

    @Override
    protected void onCleared() {
        repo.dispose();
        super.onCleared();
    }



}
