package com.ontime.ontime.views.activities.contractSignature;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.ViewModelProviders;
import butterknife.BindView;
import butterknife.OnClick;
import com.bumptech.glide.Glide;
import com.google.android.material.textfield.TextInputEditText;
import com.ontime.ontime.OnTimeApplication;
import com.ontime.ontime.R;
import com.ontime.ontime.base.BaseActivity;
import com.ontime.ontime.di.activity.ActivityModule;
import com.ontime.ontime.utils.TextUtils;
import com.ontime.ontime.views.activities.signatureCode.SignatureCodeActivity;

import javax.inject.Inject;

public class ContractSignatureActivity extends BaseActivity {
    private static final int READ_STORAGE_CODE = 1001;
    private static final int WRITE_STORAGE_CODE = 1002;
    private static final int PICK_IMAGE = 1;

    @Inject
    ContractSignatureViewModelProvider provider;
    @BindView(R.id.name)
    TextInputEditText nameET;
    @BindView(R.id.email)
    TextInputEditText email;
    @BindView(R.id.phonne)
    TextInputEditText phonne;
    @BindView(R.id.id)
    TextInputEditText id;
    @BindView(R.id.photo)
    ImageView photo;

    private ContractSignatureViewModel viewModel;

    private Bitmap bitmap;
    private String encodedImage = "";

    private String uploadFilePath;
    @Override
    protected int getLayout() {
        return R.layout.activity_signature;
    }

    @Override
    protected void onCreateActivityComponent() {
        OnTimeApplication.getComponent(this)
                .plus(new ActivityModule(this))
                .inject(this);
        viewModel = ViewModelProviders.of(this, provider).get(ContractSignatureViewModel.class);

        initObservers();


    }

    private void initObservers() {
        viewModel.contractActionResponseMutableLiveData.observe(this, result -> {
            if (result.status == 0) {
                startActivity(new Intent(this,SignatureCodeActivity.class).putExtra("contract_id",result.contract_id));
            }
        });
    }


    @OnClick({R.id.backBtn, R.id.photo, R.id.accept})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.backBtn:
                break;
            case R.id.photo:
                openImagePickerIntent();

                break;
            case R.id.accept:
                String phone = phonne.getText().toString().trim();
                String emailString = email.getText().toString().trim();
                String nationalId = id.getText().toString().trim();
                String name= nameET.getText().toString().trim();
                if (!TextUtils.isValidPhone(phone)) {
                    showErrorMessage("Invalid phone number ");
                    return;
                }
                if (!TextUtils.isValidEmail(emailString)) {
                    showErrorMessage("Invalid email ");
                    return;
                }
                if (nationalId.equals("")){
                    showErrorMessage("Invalid national id ");
                    return;
                }
                if (name.equals("")){
                    showErrorMessage("Invalid name ");
                    return;
                }

                viewModel.contractAction(getIntent().getIntExtra("request_id",0),name,emailString,phone,nationalId,uploadFilePath);
                break;
        }
    }

    private void openImagePickerIntent() {
        if (isPermissionGranted(Manifest.permission.READ_EXTERNAL_STORAGE)) {
            Intent intent = new Intent(Intent.ACTION_PICK,
                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(intent, PICK_IMAGE);
        } else
            requestPermission(Manifest.permission.READ_EXTERNAL_STORAGE, READ_STORAGE_CODE);

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_IMAGE) {
            Uri selectedImage = data.getData();
            String[] filePathColumn = {MediaStore.Images.Media.DATA};
            Cursor cursor = getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            cursor.moveToFirst();
            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            uploadFilePath = cursor.getString(columnIndex);
            Glide.with(this)
                    .load(uploadFilePath)
                    .centerCrop()
                    .into(photo);
            cursor.close();

        } else {
            Toast.makeText(ContractSignatureActivity.this, "You haven't picked Image", Toast.LENGTH_LONG).show();
        }


    }

    private boolean isPermissionGranted(String permission) {
        //Getting the permission status
        int result = ContextCompat.checkSelfPermission(this, permission);

        //If permission is granted returning true
        if (result == PackageManager.PERMISSION_GRANTED)
            return true;

        //If permission is not granted returning false
        return false;
    }


    private void requestPermission(String permission, int code) {

        if (ActivityCompat.shouldShowRequestPermissionRationale((Activity) this, permission)) {
            //If the user has denied the permission previously your code will come to this block
            //Here you can explain why you need this permission
            //Explain here why you need this permission
        }
        //And finally ask for the permission
        ActivityCompat.requestPermissions((Activity) this, new String[]{permission}, code);
    }

    //This method will be called when the user will tap on allow or deny
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        //Checking the request code of our request
        if (requestCode == READ_STORAGE_CODE) {

            //If permission is granted
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {


                openImagePickerIntent();


            } else {

                //mContext.get.finish();
            }
        } else if (requestCode == WRITE_STORAGE_CODE) {


            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

            } else {

                //mContext.finish();
            }
        }
    }
}