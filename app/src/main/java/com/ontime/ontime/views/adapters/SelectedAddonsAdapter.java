package com.ontime.ontime.views.adapters;

import android.content.Context;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.marcinorlowski.fonty.Fonty;
import com.ontime.ontime.R;
import com.ontime.ontime.store.models.service.AddonModel;

import java.util.ArrayList;

public class SelectedAddonsAdapter extends RecyclerView.Adapter<SelectedAddonsAdapter.ViewHolder> {

    private final Context context;
    private final ArrayList<AddonModel> addonModels;

    public SelectedAddonsAdapter(Context context, ArrayList<AddonModel> addonModels) {
        this.context = context;
        this.addonModels = addonModels;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_selected_service, viewGroup, false);
        Fonty.setFonts((ViewGroup) view);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {

        viewHolder.bind(addonModels.get(i));
    }

    @Override
    public int getItemCount() {
        return addonModels.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.name)
        TextView nameTextView;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }

        public void bind(AddonModel addonModel) {
            nameTextView.setText(addonModel.getName());
        }
    }
}
