package com.ontime.ontime.views.activities.profileData;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import com.ontime.ontime.store.models.GetProfileResponse;
import com.ontime.ontime.store.models.user.LoginResponse;
import com.ontime.ontime.store.repo.AppRepo;

public class ProfileDataViewModel extends ViewModel {

    private final AppRepo repo;
    public final MutableLiveData<GetProfileResponse> getProfileResponseMutableLiveData;

    public ProfileDataViewModel(AppRepo repo) {
        this.repo = repo;
        this.getProfileResponseMutableLiveData= repo.getProfileData;
    }


    @Override
    protected void onCleared() {
        repo.dispose();
        super.onCleared();
    }

    public void getProfileData() {
            repo.getProfileData();

    }
}
