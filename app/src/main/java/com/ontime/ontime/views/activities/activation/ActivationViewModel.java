package com.ontime.ontime.views.activities.activation;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import com.ontime.ontime.store.repo.AppRepo;

public class ActivationViewModel extends ViewModel {

    public MutableLiveData<Boolean> codeResentLiveData;
    public MutableLiveData<Boolean> isActivationSuccess;

    private final AppRepo repo;

    public ActivationViewModel(AppRepo repo) {
        this.repo = repo;
        isActivationSuccess = repo.isActionSuccess;
        codeResentLiveData = repo.isCodeSent;
    }


    @Override
    protected void onCleared() {
        repo.dispose();
        super.onCleared();
    }


    public void checkCoded(String code) {
        repo.checkCoded(code);
    }
    public void checkPCoded(String code) {
        repo.checkPCoded(code);
    }



    public void resendCode() {
        repo.resendCodeCoded();

    }
}
