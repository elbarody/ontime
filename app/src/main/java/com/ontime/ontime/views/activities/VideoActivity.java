package com.ontime.ontime.views.activities;

import android.media.MediaPlayer;
import android.net.Uri;
import android.util.Log;
import android.widget.MediaController;
import android.widget.Toast;
import android.widget.VideoView;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import com.ontime.ontime.R;

import java.util.ArrayList;
import java.util.Arrays;

public class VideoActivity extends AppCompatActivity {

    VideoView videoView;
    ArrayList<String> arrayList = new ArrayList<>();

    int index = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video);

        videoView = findViewById(R.id.videoView);
        final MediaController mediacontroller = new MediaController(this);
        mediacontroller.setAnchorView(videoView);

        String url=getIntent().getStringExtra("file");
        arrayList.add(url);

        videoView.setMediaController(mediacontroller);
        videoView.setVideoURI(Uri.parse(arrayList.get(0)));
        videoView.requestFocus();

        videoView.setOnCompletionListener(mp -> {
            Toast.makeText(getApplicationContext(), "Video over", Toast.LENGTH_SHORT).show();
            if (index++ == arrayList.size()) {
                index = 0;
                mp.release();
                Toast.makeText(getApplicationContext(), "Video over", Toast.LENGTH_SHORT).show();
            } else {
                videoView.setVideoURI(Uri.parse(arrayList.get(index)));
                videoView.start();
            }


        });

        videoView.setOnErrorListener((mp, what, extra) -> {
            Log.d("API123", "What " + what + " extra " + extra);
            return false;
        });
    }
}