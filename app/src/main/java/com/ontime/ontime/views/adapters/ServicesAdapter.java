package com.ontime.ontime.views.adapters;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.ontime.ontime.R;
import com.ontime.ontime.store.models.service.ServicesModel;

import java.util.List;

public class ServicesAdapter extends RecyclerView.Adapter<ServicesAdapter.ViewHolder> {

    private final Context context;

    private List<ServicesModel> servicesModels;
    private OnServiceClickedListener onServiceClickedListener;

    public ServicesAdapter(Context context, List<ServicesModel> servicesModels, OnServiceClickedListener onServiceClickedListener) {
        this.context = context;
        this.onServiceClickedListener = onServiceClickedListener;
        this.servicesModels = servicesModels;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.item__service, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        viewHolder.bind(servicesModels.get(i));
    }

    @Override
    public int getItemCount() {
        return servicesModels.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.nameTextView)
        TextView nameTextView;

        private ServicesModel servicesModel;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            itemView.setOnClickListener(v -> onServiceClickedListener.onClick(servicesModel));

        }

        public void bind(ServicesModel addonModel) {
            this.servicesModel = addonModel;
            nameTextView.setText(addonModel.getName());
        }
    }

    public interface OnServiceClickedListener {

        void onClick(ServicesModel servicesModel);
    }
}
