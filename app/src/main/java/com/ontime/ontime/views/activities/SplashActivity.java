package com.ontime.ontime.views.activities;

import android.content.Intent;
import android.os.Handler;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import com.ontime.ontime.R;
import com.ontime.ontime.store.models.user.UserData;
import com.ontime.ontime.utils.SessionManager;
import com.ontime.ontime.views.activities.home.HomeActivity;
import com.ontime.ontime.views.activities.login.LoginActivity;
import com.readystatesoftware.chuck.internal.ui.MainActivity;

public class SplashActivity extends AppCompatActivity {

    UserData user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        user = new SessionManager(this).getUserDetails();
        new Handler().postDelayed(() -> {
            if (null == user) {
                startActivity(new Intent(SplashActivity.this, WelcomeActivity.class));
                this.finish();
            } else if (user.userSesstion.equals("welcome")) {
                startActivity(new Intent(SplashActivity.this, LoginActivity.class));
                SplashActivity.this.finish();
            }else {
                startActivity(new Intent(SplashActivity.this, HomeActivity.class));
                SplashActivity.this.finish();
            }

        }, 1500);
    }
}
