package com.ontime.ontime.views.activities.departments;

import androidx.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.BindView;
import com.bumptech.glide.Glide;
import com.ontime.ontime.OnTimeApplication;
import com.ontime.ontime.R;
import com.ontime.ontime.base.BaseActivity;
import com.ontime.ontime.di.activity.ActivityModule;
import com.ontime.ontime.di.scope.ActivityScope;
import com.ontime.ontime.store.models.department.DepartmentModel;
import com.ontime.ontime.views.activities.addproject.AddProjectActivity;

import javax.inject.Inject;
import java.util.List;

@ActivityScope
public class DepartmentsActivity extends BaseActivity {


    @Inject
    DepartmentsViewModelProvider provider;

    @BindView(R.id.backBtn)
    ImageView backBtn;
    @BindView(R.id.imageView)
    ImageView imageView;
    @BindView(R.id.text_view)
    TextView textView;
    @BindView(R.id.text_view3)
    TextView nameTextView;
    @BindView(R.id.text_view4)
    TextView descriptionTextView;
    @BindView(R.id.nextTextView)
    ImageView nextTextView;
    @BindView(R.id.lastTextView)
    ImageView lastTextView;

    private DepartmentsViewModel viewModel;
    private List<DepartmentModel> departmentModels;
    private int currentPos;

    @Override
    protected int getLayout() {
        return R.layout.activity_new_project;
    }

    @Override
    protected void onCreateActivityComponent() {

        OnTimeApplication.getComponent(this)
                .plus(new ActivityModule(this))
                .inject(this);
        viewModel = ViewModelProviders.of(this, provider).get(DepartmentsViewModel.class);

        initUi();
        initObservers();

        viewModel.getDepartments();

    }

    private void initUi() {
        imageView.setOnClickListener(v -> {
            if (departmentModels != null) {
                Intent intent = new Intent(this, AddProjectActivity.class);
                intent.putExtra("depId", departmentModels.get(currentPos).getId());
                startActivity(intent);
            }

        });
        backBtn.setOnClickListener(v -> finish());

        nextTextView.setOnClickListener(v -> {
            if (departmentModels != null) {
                if (departmentModels.size() > currentPos + 1) {
                    currentPos = currentPos + 1;
                    Glide.with(this).load(departmentModels.get(currentPos).getImg()).into(imageView);
                    nameTextView.setText(departmentModels.get(currentPos).getName());
                    descriptionTextView.setText(departmentModels.get(currentPos).getDescr());
                }
            }

        });

        lastTextView.setOnClickListener(v -> {

            if (departmentModels != null) {
                if (currentPos != 0) {
                    currentPos = currentPos - 1;
                    Glide.with(this).load(departmentModels.get(currentPos).getImg()).into(imageView);
                    nameTextView.setText(departmentModels.get(currentPos).getName());
                    descriptionTextView.setText(departmentModels.get(currentPos).getDescr());
                }
            }
        });


    }

    private void initObservers() {

        viewModel.departmentsLiveData.observe(this, departmentModels -> {

            if (departmentModels != null) {
                this.departmentModels = departmentModels;
                if (departmentModels.size() == 0) {
                    //TODO inflate emptyView
                } else {
                    currentPos = 0;
                    Glide.with(this).load(departmentModels.get(0).getImg()).into(imageView);
                    nameTextView.setText(departmentModels.get(0).getName());
                    descriptionTextView.setText(departmentModels.get(0).getDescr());
                }
            }
        });
    }

}
