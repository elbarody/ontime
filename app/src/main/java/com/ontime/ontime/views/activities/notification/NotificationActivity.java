package com.ontime.ontime.views.activities.notification;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.ontime.ontime.OnTimeApplication;
import com.ontime.ontime.R;
import com.ontime.ontime.base.BaseActivity;
import com.ontime.ontime.di.activity.ActivityModule;
import com.ontime.ontime.views.adapters.NotificationRecyclerViewAdapter;

import javax.inject.Inject;

public class NotificationActivity extends BaseActivity {
    @BindView(R.id.rv)
    RecyclerView rv;
    @BindView(R.id.unreaded_message)
    TextView unreadedMessage;
    @Inject
    NotificationRecyclerViewAdapter adapter;
    @Inject
    NotificationViewModelProvider provider;
    @BindView(R.id.constraintLayout_container)
    ConstraintLayout constraintLayoutContainer;

    private NotificationViewModel viewModel;


    @Override
    protected int getLayout() {
        return R.layout.activity_notifications;
    }

    @Override
    protected void onCreateActivityComponent() {
        OnTimeApplication.getComponent(this)
                .plus(new ActivityModule(this))
                .inject(this);
        viewModel = ViewModelProviders.of(this, provider).get(NotificationViewModel.class);
        initHomeUi();

        initObservers();

        viewModel.getNotification();

    }

    private void initHomeUi() {
        rv.setLayoutManager(new LinearLayoutManager(this));
        rv.setAdapter(adapter);
    }

    private void initObservers() {
        viewModel.notificationResponseModelMutableLiveData.observe(this, getNotificationResponseModel -> {
            assert getNotificationResponseModel != null;
            if (getNotificationResponseModel.getData().size() > 0) {
                constraintLayoutContainer.setVisibility(View.GONE);
                rv.setVisibility(View.VISIBLE);
                adapter.pushData(getNotificationResponseModel.getData());
            }else {
                constraintLayoutContainer.setVisibility(View.VISIBLE);
                rv.setVisibility(View.GONE);
            }
            unreadedMessage.setText(getString(R.string.you_have_1_new_message).replace("1", getNotificationResponseModel.getUnreadNum()));
        });
    }

}
