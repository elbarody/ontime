package com.ontime.ontime.views.activities;

import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import android.text.Html;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.ontime.ontime.R;

public class PaymentOptionsActivity extends AppCompatActivity {

    TextView timeTextView;
    TextView priceTextView;
    TextView percentageTextView;
    TextView payButton;
    LinearLayout creditLayout;
    LinearLayout bankLayout;
    LinearLayout paypalLayout;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_options);

        priceTextView = findViewById(R.id.priceTextView);
        timeTextView = findViewById(R.id.timeTextView);
        percentageTextView = findViewById(R.id.percentageTextView);
        creditLayout = findViewById(R.id.creditLayout);
        bankLayout = findViewById(R.id.bankLayout);
        paypalLayout = findViewById(R.id.paypalLayout);
        payButton = findViewById(R.id.payButton);

        creditLayout.setActivated(true);
        bankLayout.setActivated(false);
        paypalLayout.setActivated(false);

        creditLayout.setOnClickListener(v -> {
            creditLayout.setActivated(true);
            bankLayout.setActivated(false);
            paypalLayout.setActivated(false);

        });

        bankLayout.setOnClickListener(v -> {
            creditLayout.setActivated(false);
            bankLayout.setActivated(true);
            paypalLayout.setActivated(false);

        });

        paypalLayout.setOnClickListener(v -> {
            creditLayout.setActivated(false);
            bankLayout.setActivated(false);
            paypalLayout.setActivated(true);

        });

        payButton.setOnClickListener(v -> {
            startActivity(new Intent(this, AddCreditActivity.class));
        });

        creditLayout.setOnClickListener(v -> {
            creditLayout.setActivated(true);
            bankLayout.setActivated(false);
            paypalLayout.setActivated(false);

        });

        priceTextView.setText(Html.fromHtml("الضريبة".concat("<br/>").concat("<br/>")
                .concat("<fonts color=#DAD93A>").concat("%5").concat("</fonts>")));

        timeTextView.setText(Html.fromHtml("الخدمة / الخدمات".concat("<br/>").concat("<br/>")
                .concat("<fonts color=#DAD93A>").concat("ر.س 144").concat("</fonts>")));

        percentageTextView.setText(Html.fromHtml("السعر الاجمالى".concat("<br/>").concat("<br/>")
                .concat("<fonts color=#DAD93A>").concat("1700").concat("</fonts>")));


    }
}
