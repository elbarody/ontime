package com.ontime.ontime.views.activities.component;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import com.ontime.ontime.store.models.ContractActionResponse;
import com.ontime.ontime.store.models.GetComponentResponse;
import com.ontime.ontime.store.repo.AppRepo;

public class ComponentViewModel extends ViewModel {

    public MutableLiveData<GetComponentResponse> componentResponseMutableLiveData;
    public MutableLiveData<Boolean> actionSuccess;
    private final AppRepo repo;

    public ComponentViewModel(AppRepo repo) {
        this.repo = repo;
        this.componentResponseMutableLiveData = repo.componentResponseMutableLiveData;
        this.actionSuccess = repo.isActionSuccess;
    }

    public void getAcceptOrRefuseComponent(int request_id, String decision) {
        repo.getAcceptOrRefuseComponent(decision, request_id);
    }

    public void getComponent(int request_id) {
        repo.getComponent(request_id);
    }


    @Override
    protected void onCleared() {
        repo.dispose();
        super.onCleared();
    }

}
