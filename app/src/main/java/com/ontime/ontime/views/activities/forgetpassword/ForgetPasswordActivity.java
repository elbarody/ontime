package com.ontime.ontime.views.activities.forgetpassword;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.widget.EditText;
import butterknife.BindView;
import butterknife.OnClick;
import com.ontime.ontime.OnTimeApplication;
import com.ontime.ontime.R;
import com.ontime.ontime.base.BaseActivity;
import com.ontime.ontime.di.activity.ActivityModule;
import com.ontime.ontime.di.scope.ActivityScope;
import com.ontime.ontime.views.activities.activation.ActivationActivity;

import javax.inject.Inject;

@ActivityScope
public class ForgetPasswordActivity extends BaseActivity {

    @Inject
    ForgetPasswordViewModelProvider passwordViewModelProvider;

    @BindView(R.id.mail_et)
    EditText email;

    private ForgetPasswordViewModel viewModel;

    @Override
    protected int getLayout() {
        return R.layout.activity_forget_password_;
    }

    @Override
    protected void onCreateActivityComponent() {
        OnTimeApplication.getComponent(this)
                .plus(new ActivityModule(this))
                .inject(this);

        viewModel = ViewModelProviders.of(this, passwordViewModelProvider).get(ForgetPasswordViewModel.class);

        initObservers();
    }

    void initObservers() {
        viewModel.forgetPasswordSuccess.observe(this, aBoolean -> {
            if (aBoolean) {

                Intent intent = new Intent(this,ActivationActivity.class);
                intent.putExtra("isFP",true);
                startActivity(intent);
            }
        });
    }


    @OnClick({R.id.button_send})
    void onSendClicked() {
        if (!com.ontime.ontime.utils.TextUtils.isValidEmail(email.getText().toString())) {
            showErrorMessage("Invalid Email ");
            return;
        }
        viewModel.forgetPassword(email.getText().toString());
    }



}
