package com.ontime.ontime.views.fragments;

import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import com.ontime.ontime.R;
import com.ontime.ontime.base.BaseFragment;
import com.ontime.ontime.store.models.RequestsModel;
import com.ontime.ontime.views.activities.home.HomeActivity;
import com.ontime.ontime.views.activities.home.HomeViewModel;
import com.ontime.ontime.views.adapters.HomeAdapter;

import java.util.ArrayList;
import java.util.List;

public class HomeFragment extends BaseFragment {

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    private HomeViewModel viewModel;
    private HomeAdapter adapter;
    private List<RequestsModel> models = new ArrayList<>();
    private String type;


    @Override
    protected int getLayout() {
        return R.layout.fragment_home;
    }


    @Override
    protected void onCreateFragmentComponent() {


        switch (((HomeActivity)getActivity()).tabLayout.getSelectedTabPosition()){
            case 0:
                type="all";
                break;
            case 1:
                type="under_preview";
                break;
            case 2:
                type="in_progress";
                break;
            case 3:
                type="finished";
                break;

        }
        viewModel = ViewModelProviders.of(getActivity()).get(HomeViewModel.class);

        initUI();
        initObservers();

        viewModel.getRequest(type);
    }

    private void initUI() {
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        adapter = new HomeAdapter(models, getActivity());
        recyclerView.setAdapter(adapter);
    }

    private void initObservers() {
        viewModel.requestSuccess.observe(this, requestResponseModel -> {
            assert requestResponseModel != null;
            models.clear();
            models.addAll(requestResponseModel.data);
            adapter.notifyDataSetChanged();

        });

    }

    @Override
    public void finishScreen() {

    }
}
