package com.ontime.ontime.views.activities.requestdetails.fragments;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import com.ontime.ontime.R;
import com.ontime.ontime.base.BaseFragment;
import com.ontime.ontime.store.models.requestdetails.AttachmentsModel;
import com.ontime.ontime.store.models.service.AddonModel;
import com.ontime.ontime.views.activities.requestdetails.RequestDetailsViewModel;
import com.ontime.ontime.views.adapters.BelongAdapter;
import com.ontime.ontime.views.adapters.SelectedAddonsAdapter;

import java.util.ArrayList;

public class BelongFragment extends BaseFragment {
    BelongAdapter adapter;
    @BindView(R.id.rv)
    RecyclerView rv;
    private RequestDetailsViewModel viewModel;


    @Override
    protected int getLayout() {
        return R.layout.fragment_project_belongs;
    }

    @Override
    protected void onCreateFragmentComponent() {
        viewModel = ViewModelProviders.of(getActivity()).get(RequestDetailsViewModel.class);

        initObservers();
    }

    private void initObservers() {
        viewModel.requestDetailsLiveData.observe(this, requestDetailsData -> {
            if (requestDetailsData != null) {

                if (requestDetailsData.getAttachments().size() != 0) {
                    rv.setLayoutManager(new LinearLayoutManager(getActivity()));
                    rv.setAdapter(new BelongAdapter(getActivity(), (ArrayList<AttachmentsModel>) requestDetailsData.getAttachments(),(view, item) -> {
                        Uri uri = Uri.parse(item.getFile()); // missing 'http://' will cause crashed
                        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                        startActivity(intent);
                    }));
                }
            }
        });
    }



    @Override
    public void finishScreen() {

    }
}
