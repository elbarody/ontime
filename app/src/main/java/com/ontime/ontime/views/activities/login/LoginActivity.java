package com.ontime.ontime.views.activities.login;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import androidx.lifecycle.ViewModelProviders;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.ontime.ontime.OnTimeApplication;
import com.ontime.ontime.R;
import com.ontime.ontime.base.BaseActivity;
import com.ontime.ontime.di.activity.ActivityModule;
import com.ontime.ontime.di.scope.ActivityScope;
import com.ontime.ontime.store.models.user.UserData;
import com.ontime.ontime.store.socketIO.SocketIOInitialization;
import com.ontime.ontime.utils.SessionManager;
import com.ontime.ontime.utils.TextUtils;
import com.ontime.ontime.views.activities.activation.ActivationActivity;
import com.ontime.ontime.views.activities.forgetpassword.ForgetPasswordActivity;
import com.ontime.ontime.views.activities.home.HomeActivity;
import com.ontime.ontime.views.activities.register.RegistrationActivity;
import com.rilixtech.widget.countrycodepicker.CountryCodePicker;

import javax.inject.Inject;

@ActivityScope
public class LoginActivity extends BaseActivity {

    @Inject
    LoginViewModelProvider provider;

    @BindView(R.id.button_login)
    Button buttonLogin;
    @BindView(R.id.phone_number_et)
    EditText phoneNumberEditText;
    @BindView(R.id.password_et)
    EditText passwordEditText;
    @BindView(R.id.ccp)
    CountryCodePicker ccp;

    private LoginViewModel viewModel;
    private UserData user;
    private SessionManager sessionManager;

    @Override
    protected int getLayout() {
        return R.layout.activity_login;
    }

    @Override
    protected void onCreateActivityComponent() {
        sessionManager = new SessionManager(this);
        user = new UserData();
        user.userSesstion = "welcome";
        sessionManager.createLoginSession(user);
        OnTimeApplication.getComponent(this)
                .plus(new ActivityModule(this))
                .inject(this);
        viewModel = ViewModelProviders.of(this, provider).get(LoginViewModel.class);

        initObservers();
    }

    private void initObservers() {
        viewModel.loginResponseMutableLiveData.observe(this, state -> {
            if (state.getStatus() == 0) {
                SocketIOInitialization.getSocketInstance(state.getUserData().getToken());
                state.getUserData().userSesstion = "login";
                sessionManager.createLoginSession(state.getUserData());
                startActivity(new Intent(this, HomeActivity.class));
            } else if (state.getStatus() == 213) {
                user.setToken(state.getToken());
                sessionManager.createLoginSession(user);
                startActivity(new Intent(this, ActivationActivity.class).putExtra("from", "login"));
            }
        });

    }

    @OnClick({R.id.new_account, R.id.forget_password, R.id.button_login})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.new_account:
                startActivity(new Intent(this, RegistrationActivity.class));
                break;

            case R.id.forget_password:
                startActivity(new Intent(this, ForgetPasswordActivity.class));
                break;

            case R.id.button_login:
//                startActivity(new Intent(this, HomeActivity.class));
                processLogin();
                break;
        }
    }

    private void processLogin() {
        String phone = ccp.getSelectedCountryCode() + phoneNumberEditText.getText().toString().trim();
        String pass = passwordEditText.getText().toString().trim();
        if (!TextUtils.isValidPhone(phoneNumberEditText.getText().toString().trim())) {
            showErrorMessage("Invalid phone number ");
            return;
        }
        if (!TextUtils.isValidPassword(pass)) {
            showErrorMessage("Invalid password ");
            return;
        }
        viewModel.login(phone, pass);
        //viewModel.login("+2001016948526", "A12345");
    }
}
