package com.ontime.ontime.views.activities;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;
import com.codemybrainsout.onboarder.AhoyOnboarderActivity;
import com.codemybrainsout.onboarder.AhoyOnboarderCard;
import com.ontime.ontime.R;
import com.ontime.ontime.store.models.user.UserData;
import com.ontime.ontime.utils.SessionManager;
import com.ontime.ontime.views.activities.login.LoginActivity;
import com.xwady.core.models.User;

import java.util.ArrayList;
import java.util.List;

public class WelcomeActivity extends AhoyOnboarderActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        AhoyOnboarderCard ahoyOnboarderCard1 = new AhoyOnboarderCard(getString(R.string.programming_into), getString(R.string.programming_intro_desc), R.drawable.welcome_image_1);
        AhoyOnboarderCard ahoyOnboarderCard2 = new AhoyOnboarderCard(getString(R.string.desgin_intro), getString(R.string.desgin_intro_desc), R.drawable.welcome_image_2);
        AhoyOnboarderCard ahoyOnboarderCard3 = new AhoyOnboarderCard(getString(R.string.social_media_intro), getString(R.string.social_media_intro_desc), R.drawable.welcome_image_3);

      /*  ahoyOnboarderCard1.setBackgroundDrawable(R.drawable.enter);
        ahoyOnboarderCard2.setBackgroundDrawable(R.drawable.enter);
        ahoyOnboarderCard3.setBackgroundDrawable(R.drawable.enter);*/

        List<AhoyOnboarderCard> pages = new ArrayList<>();

        pages.add(ahoyOnboarderCard1);
        pages.add(ahoyOnboarderCard2);
        pages.add(ahoyOnboarderCard3);

        for (AhoyOnboarderCard page : pages) {
            page.setTitleColor(R.color.white);
            page.setDescriptionColor(R.color.white);
        }

        setFinishButtonTitle("ابدء");
        showNavigationControls(true);
        setColorBackground(R.color.white);

        setInactiveIndicatorColor(R.color.grey_600);
        setActiveIndicatorColor(R.color.black);

        setOnboardPages(pages);
        //Fonty.setFonts(this);
    }

    @Override
    public void onFinishButtonPressed() {
        startActivity(new Intent(this, LoginActivity.class));
        //Toast.makeText(this,"clicked",Toast.LENGTH_LONG).show();
        this.finish();
    }
}
