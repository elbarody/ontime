package com.ontime.ontime.views.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.bumptech.glide.Glide;
import com.marcinorlowski.fonty.Fonty;
import com.ontime.ontime.R;
import com.ontime.ontime.store.models.RequestsModel;
import com.ontime.ontime.views.activities.chatBox.MessageBoxActivity;
import de.hdodenhof.circleimageview.CircleImageView;

import java.util.List;

public class HomeAdapter extends RecyclerView.Adapter<HomeAdapter.ViewHolder> {

    private final Context context;


    private List<RequestsModel> models;

    public HomeAdapter(List<RequestsModel> models, Context context) {
        this.context = context;
        this.models = models;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_home, viewGroup, false);
        Fonty.setFonts((ViewGroup) view);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {
        RequestsModel model = models.get(i);
        Glide.with(context).load(model.img).placeholder(R.drawable.home_item_icon).into(viewHolder.imageView);
        viewHolder.titleTextView.setText(model.request_name);
        viewHolder.titleTextView2.setText(model.request_descr);
        viewHolder.progressBar.setProgress(model.percentage);
        viewHolder.itemView.setOnClickListener(v -> {
                    Intent intent = new Intent(context, MessageBoxActivity.class);
                    intent.putExtra("requestId", model.id);
                    context.startActivity(intent);
                }
        );
        viewHolder.dateTextView.setText(model.start_time +" "+model.end_time);
        viewHolder.statusTextView.setText(model.status_descr);
    }

    @Override
    public int getItemCount() {
        return models.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.imageView)
        CircleImageView imageView;
        @BindView(R.id.titleTextView)
        TextView titleTextView;
        @BindView(R.id.titleTextView2)
        TextView titleTextView2;
        @BindView(R.id.progressBar)
        ProgressBar progressBar;
        @BindView(R.id.statusTextView)
        TextView statusTextView;
        @BindView(R.id.dateTextView)
        TextView dateTextView;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }
}
