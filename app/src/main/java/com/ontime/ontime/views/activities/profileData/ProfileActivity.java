package com.ontime.ontime.views.activities.profileData;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProviders;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.bumptech.glide.Glide;
import com.ontime.ontime.OnTimeApplication;
import com.ontime.ontime.R;
import com.ontime.ontime.base.BaseActivity;
import com.ontime.ontime.di.activity.ActivityModule;
import com.ontime.ontime.store.models.user.UserData;
import com.ontime.ontime.utils.ProgressBarAnimation;
import com.ontime.ontime.utils.SessionManager;
import com.ontime.ontime.views.activities.editProfile.EditProfileActivity;
import de.hdodenhof.circleimageview.CircleImageView;

import javax.inject.Inject;

public class ProfileActivity extends BaseActivity {

    @Inject
    ProfileDataViewModelProvider provider;

    ImageView edit;
    @BindView(R.id.userImageView)
    CircleImageView userImageView;
    @BindView(R.id.nameTextview)
    TextView nameTextview;
    @BindView(R.id.phone_tv)
    TextView phoneTextview;
    @BindView(R.id.projects_number_tv)
    TextView projectsNumberTv;
    @BindView(R.id.money_number_tv)
    TextView moneyNumberTv;
    @BindView(R.id.points_number_tv)
    TextView pointsNumberTv;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    private ProfileDataViewModel viewModel;
    private UserData user;


    @Override
    protected int getLayout() {
        return R.layout.activity_profile;
    }

    @Override
    protected void onCreateActivityComponent() {
        OnTimeApplication.getComponent(this)
                .plus(new ActivityModule(this))
                .inject(this);

        viewModel = ViewModelProviders.of(this, provider).get(ProfileDataViewModel.class);

        initUi();

        initObservers();
    }


    private void initObservers() {
        viewModel.getProfileResponseMutableLiveData.observe(this, response -> {
            if (!(response.getStatus() == 0)) {
                showSuccessMessage(response.getMsg());

            } else {
                Glide.with(ProfileActivity.this).load(response.getData().getImg()).error(R.drawable.person_icon).into(userImageView);
                nameTextview.setText(response.getData().getName());
                phoneTextview.setText(response.getData().getPhone());
                projectsNumberTv.setText(Integer.toString(response.getData().getProjects()));
                moneyNumberTv.setText(Integer.toString(response.getData().getTotalPaid()));
                pointsNumberTv.setText(Integer.toString(response.getData().getPoints()));
            }

        });

    }

    void initUi() {

        user = new SessionManager(this).getUserDetails();

        viewModel.getProfileData();
        ProgressBarAnimation anim = new ProgressBarAnimation(progressBar, 0, 60);
        anim.setDuration(1000);
        progressBar.startAnimation(anim);
    }


    @OnClick({R.id.backBtn, R.id.edit})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.backBtn:
                break;
            case R.id.edit:
                startActivity(new Intent(this, EditProfileActivity.class));

                break;
        }
    }
}
