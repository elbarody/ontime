package com.ontime.ontime.views.activities.home;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import com.ontime.ontime.store.models.RequestResponseModel;
import com.ontime.ontime.store.repo.AppRepo;

public class HomeViewModel extends ViewModel {
    public MutableLiveData<RequestResponseModel> requestSuccess;
    private final AppRepo repo;

    public HomeViewModel(AppRepo repo) {
        this.repo = repo;
        this.requestSuccess = repo.requestSuccess;
    }

    public void getRequest(String type) {
        repo.getRequests(type);
    }

    @Override
    protected void onCleared() {
        repo.dispose();
        super.onCleared();
    }

}
