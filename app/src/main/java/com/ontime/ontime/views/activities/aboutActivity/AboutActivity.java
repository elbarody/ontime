package com.ontime.ontime.views.activities.aboutActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.text.Html;
import android.view.View;
import android.widget.TextView;
import androidx.lifecycle.ViewModelProviders;
import butterknife.BindView;
import butterknife.OnClick;
import com.ontime.ontime.OnTimeApplication;
import com.ontime.ontime.R;
import com.ontime.ontime.base.BaseActivity;
import com.ontime.ontime.di.activity.ActivityModule;
import com.ontime.ontime.views.activities.activation.ActivationViewModel;
import com.ontime.ontime.views.activities.home.HomeActivity;
import com.ontime.ontime.views.activities.newpassword.NewPasswordActivity;

import javax.inject.Inject;

public class AboutActivity extends BaseActivity {

    @BindView(R.id.text_who_are_we)
    TextView textWhoAreWe;


    private AboutUsViewModel viewModel;
    @Inject
    AboutUsViewModelProvider provider;
    private String fb_url, tw_url, ins_url;

    @Override
    protected int getLayout() {
        return R.layout.activity_about;
    }

    @Override
    protected void onCreateActivityComponent() {
        OnTimeApplication.getComponent(this)
                .plus(new ActivityModule(this))
                .inject(this);
        viewModel = ViewModelProviders.of(this, provider).get(AboutUsViewModel.class);

        initUI();

        initObservers();
    }

    private void initUI() {
        viewModel.getAboutUs();
    }

    private void initObservers() {

        viewModel.whoAreYouResponseMutableLiveData.observe(this, state -> {
            if (state.getStatus() == 0) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    textWhoAreWe.setText(Html.fromHtml(state.getWhoWeAreData().getWhoWeAre(), Html.FROM_HTML_MODE_COMPACT));
                } else {
                    textWhoAreWe.setText(Html.fromHtml(state.getWhoWeAreData().getWhoWeAre()));
                }
                fb_url = state.getWhoWeAreData().getFb();
                ins_url = state.getWhoWeAreData().getIns();
                tw_url = state.getWhoWeAreData().getTw();
            }

        });
    }


    @OnClick({R.id.backBtn, R.id.instagram_img, R.id.twitter_img, R.id.facebook_img})
    public void onViewClicked(View view) {
        String url="";

        switch (view.getId()) {
            case R.id.instagram_img:
                url = ins_url;
                break;
            case R.id.twitter_img:
                url = tw_url;
                break;
            case R.id.facebook_img:
                url = fb_url;
                break;

        }

        if (!url.startsWith("http://") && !url.startsWith("https://"))
            url = "http://" + url;
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        startActivity(browserIntent);
    }
}
