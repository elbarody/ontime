package com.ontime.ontime.views.activities.chatBox;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import com.jaiselrahman.filepicker.model.MediaFile;
import com.ontime.ontime.store.models.ContractResponse;
import com.ontime.ontime.store.models.GetChatResponse;
import com.ontime.ontime.store.models.SendMessageResponse;
import com.ontime.ontime.store.models.service.AddonModel;
import com.ontime.ontime.store.models.service.ServicesResponse;
import com.ontime.ontime.store.repo.AppRepo;

import java.util.ArrayList;
import java.util.List;

public class MessageBoxViewModel extends ViewModel {

    private final AppRepo repo;
    MutableLiveData<GetChatResponse> chatResponseMutableLiveData;
    MutableLiveData<SendMessageResponse> sendMessageResponseMutableLiveData;
    public MutableLiveData<ContractResponse> getContractResponseLiveData = new MutableLiveData<>();

    MessageBoxViewModel(AppRepo repo) {
        this.repo = repo;
        this.sendMessageResponseMutableLiveData = repo.sendMessageResponseMutableLiveData;
        this.chatResponseMutableLiveData = repo.chatResponseMutableLiveData;
        this.getContractResponseLiveData=repo.getContractResponseLiveData;

    }


    @Override
    protected void onCleared() {
        repo.dispose();
        super.onCleared();
    }

    void getMessages(int request_id) {
        repo.getMessageChat(request_id);
    }

    void sendMessage(int request_id, String msgTxt, String file_type, String file) {
        repo.sendMessage(request_id, msgTxt, file_type, file);
    }

    public void getContractDtails(int request_id){
        repo.getRequestContract(request_id);
    }

}
