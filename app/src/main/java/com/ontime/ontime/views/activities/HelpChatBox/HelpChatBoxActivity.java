package com.ontime.ontime.views.activities.HelpChatBox;

import android.content.Intent;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.LinearSmoothScroller;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.jaiselrahman.filepicker.activity.FilePickerActivity;
import com.jaiselrahman.filepicker.config.Configurations;
import com.jaiselrahman.filepicker.model.MediaFile;
import com.ontime.ontime.OnTimeApplication;
import com.ontime.ontime.R;
import com.ontime.ontime.base.BaseActivity;
import com.ontime.ontime.di.activity.ActivityModule;
import com.ontime.ontime.store.models.department.DepartmentModel;
import com.ontime.ontime.store.models.user.UserData;
import com.ontime.ontime.utils.SessionManager;
import com.ontime.ontime.views.adapters.MessageBoxRecyclerViewAdapter;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.widget.LinearLayout.VERTICAL;

public class HelpChatBoxActivity extends BaseActivity {

    private static final int FILE_REQUEST_CODE = 400;

    @Inject
    HelpChatBoxViewModelProvider provider;
    @Inject
    MessageBoxRecyclerViewAdapter adapter;

    @BindView(R.id.recyclerView_chat)
    RecyclerView recyclerViewChat;
    @BindView(R.id.send)
    ImageView send;
    @BindView(R.id.write_message)
    TextView writeMessage;
    @BindView(R.id.voice)
    ImageView voice;
    @BindView(R.id.add_message)
    ImageView addMessage;


    UserData user;
    @BindView(R.id.image_dept)
    ImageView imageDept;
    @BindView(R.id.text_dept)
    TextView textDept;

    private HelpChatBoxViewModel viewModel;
    private DepartmentModel dept_item;
    private ArrayList<MediaFile> files = new ArrayList<>();

    @Override
    protected int getLayout() {
        return R.layout.activity_help_chat_box;
    }

    @Override
    protected void onCreateActivityComponent() {
        OnTimeApplication.getComponent(this)
                .plus(new ActivityModule(this))
                .inject(this);

        viewModel = ViewModelProviders.of(this, provider).get(HelpChatBoxViewModel.class);
        initHomeUi();

        initObservers();

        viewModel.getMessages(dept_item.getId());
    }

    private void initHomeUi() {
        user = new SessionManager(this).getUserDetails();
        dept_item = (DepartmentModel) getIntent().getSerializableExtra("dept_item");
        textDept.setText(dept_item.getName());
        Glide.with(this).load(dept_item.getImg()).into(imageDept);
        recyclerViewChat.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, VERTICAL, true) {

            @Override
            public void smoothScrollToPosition(RecyclerView recyclerView, RecyclerView.State state, int position) {
                LinearSmoothScroller smoothScroller = new LinearSmoothScroller(HelpChatBoxActivity.this) {

                    private static final float SPEED = 3000f;

                    @Override
                    protected float calculateSpeedPerPixel(DisplayMetrics displayMetrics) {
                        return SPEED / displayMetrics.densityDpi;
                    }

                };
                smoothScroller.setTargetPosition(position);
                startSmoothScroll(smoothScroller);
            }

        };
        recyclerViewChat.setLayoutManager(linearLayoutManager);
        recyclerViewChat.setAdapter(adapter);
    }


    private void initObservers() {
        viewModel.chatResponseMutableLiveData.observe(this, getChatMessageResponse -> {
            assert getChatMessageResponse != null;
            if (getChatMessageResponse.getData().size() > 0) {
                //emptyView.setVisibility(View.GONE);
                adapter.pushData(getChatMessageResponse.getData());
            }

        });

        viewModel.sendMessageResponseMutableLiveData.observe(this, sendMessageResponse -> {
            if (sendMessageResponse.status == 0) {
                adapter.setMessage(sendMessageResponse.getSocket());
                adapter.notifyItemInserted(0);
                recyclerViewChat.scrollToPosition(0);
                writeMessage.setText("");
                recyclerViewChat.setVisibility(View.VISIBLE);

                /*MessageData data=sendMessageResponse.getSocket();
                JSONObject obj = new JSONObject();
                try {
                    obj.put("user_id", data.getUserId());
                    obj.put("room_id", data.getRoomId());
                    obj.put("time", data.getTime());
                    obj.put("file_type", data.getFileType());
                    obj.put("msg_type", data.getMsgType());
                    obj.put("msg_text", data.getMsgText());
                    obj.put("file_url", data.getFileUrl());
                    obj.put("request_id", data.getRequestId());


                } catch (JSONException e) {
                    e.printStackTrace();
                }
                socket.emit("user_chat_msg",obj);*/
            }
        });
    }

    private void openImagePickerIntent() {
        Intent intent = new Intent(this, FilePickerActivity.class);
        intent.putExtra(FilePickerActivity.CONFIGS, new Configurations.Builder()
                .setCheckPermission(true)
                .setShowImages(true)
                .enableVideoCapture(true)
                .setShowFiles(true)
                .setShowVideos(true)
                .enableImageCapture(true)
                .setMaxSelection(1)

                .setSkipZeroSizeFiles(true)
                .build());
        startActivityForResult(intent, FILE_REQUEST_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            if (requestCode == FILE_REQUEST_CODE) {
                files = data.getParcelableArrayListExtra(FilePickerActivity.MEDIA_FILES);
                viewModel.sendMessage(dept_item.getId(), "", "img", files.get(0).getPath());

                Log.d("minaLog", String.valueOf(files.size()));
            }
        }
    }


    @OnClick({R.id.backBtn, R.id.send, R.id.voice, R.id.add_message})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.backBtn:
                this.finish();
                break;
            case R.id.send:
                if (writeMessage.getText().toString().equals("")) {
                    showErrorMessage("please type your message");
                    return;
                }
                viewModel.sendMessage(dept_item.getId(), writeMessage.getText().toString(), "no_file", "");
                break;
            case R.id.voice:
                break;
            case R.id.add_message:
                openImagePickerIntent();
                break;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
    }
}
