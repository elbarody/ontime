package com.ontime.ontime.views.activities.contractSignature;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import com.jaiselrahman.filepicker.model.MediaFile;
import com.ontime.ontime.store.models.ContractActionResponse;
import com.ontime.ontime.store.models.service.AddonModel;
import com.ontime.ontime.store.repo.AppRepo;

import java.util.ArrayList;
import java.util.List;

public class ContractSignatureViewModel extends ViewModel {

    public MutableLiveData<ContractActionResponse> contractActionResponseMutableLiveData;
    private final AppRepo repo;

    public ContractSignatureViewModel(AppRepo repo) {
        this.repo = repo;
        this.contractActionResponseMutableLiveData = repo.contractActionResponseMutableLiveData;
    }

    public void contractAction(int request_id,String name,String email,String phone,String national_id,String img) {
        repo.contracttAction(request_id,national_id,name,email,phone,img);
    }


    @Override
    protected void onCleared() {
        repo.dispose();
        super.onCleared();
    }

}
