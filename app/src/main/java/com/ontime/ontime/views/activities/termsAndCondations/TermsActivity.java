package com.ontime.ontime.views.activities.termsAndCondations;

import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.widget.TextView;
import androidx.lifecycle.ViewModelProviders;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.ontime.ontime.OnTimeApplication;
import com.ontime.ontime.R;
import com.ontime.ontime.base.BaseActivity;
import com.ontime.ontime.di.activity.ActivityModule;

import javax.inject.Inject;

public class TermsActivity extends BaseActivity {

    @BindView(R.id.text_terms)
    TextView textTerms;


    private TermsViewModel viewModel;
    @Inject
    TermsViewModelProvider provider;

    @Override
    protected int getLayout() {
        return R.layout.activity_terms;
    }

    @Override
    protected void onCreateActivityComponent() {
        OnTimeApplication.getComponent(this)
                .plus(new ActivityModule(this))
                .inject(this);
        viewModel = ViewModelProviders.of(this, provider).get(TermsViewModel.class);

        initUI();

        initObservers();
    }

    private void initUI() {
        viewModel.getAboutUs();
    }

    private void initObservers() {

        viewModel.whoAreYouResponseMutableLiveData.observe(this, state -> {
            if (state.getStatus() == 0) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    textTerms.setText(Html.fromHtml(state.getWhoWeAreData().getTerms(), Html.FROM_HTML_MODE_COMPACT));
                } else {
                    textTerms.setText(Html.fromHtml(state.getWhoWeAreData().getTerms()));
                }
            }

        });
    }

}
