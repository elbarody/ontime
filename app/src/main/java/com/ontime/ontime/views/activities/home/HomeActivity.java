package com.ontime.ontime.views.activities.home;

import android.app.Dialog;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.view.Gravity;
import android.view.View;
import android.widget.*;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.lifecycle.ViewModelProviders;
import androidx.viewpager.widget.ViewPager;
import butterknife.BindView;

import com.bumptech.glide.Glide;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.tabs.TabLayout;
import com.marcinorlowski.fonty.Fonty;
import com.ontime.ontime.OnTimeApplication;
import com.ontime.ontime.R;
import com.ontime.ontime.base.BaseActivity;
import com.ontime.ontime.di.activity.ActivityModule;
import com.ontime.ontime.store.models.user.UserData;
import com.ontime.ontime.store.network.Endpoints;
import com.ontime.ontime.utils.DialogBuilder;
import com.ontime.ontime.utils.SessionManager;
import com.ontime.ontime.views.activities.helpCategories.HelpCategoriesActivity;
import com.ontime.ontime.views.activities.aboutActivity.AboutActivity;
import com.ontime.ontime.views.activities.departments.DepartmentsActivity;
import com.ontime.ontime.views.activities.login.LoginActivity;
import com.ontime.ontime.views.activities.notification.NotificationActivity;
import com.ontime.ontime.views.activities.profileData.ProfileActivity;
import com.ontime.ontime.views.activities.termsAndCondations.TermsActivity;
import com.ontime.ontime.views.adapters.ViewPagerAdapter;
import com.xwady.core.helpers.Utils;
import de.hdodenhof.circleimageview.CircleImageView;

import javax.inject.Inject;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class HomeActivity extends BaseActivity {

    @Inject
    HomeViewModelProvider provider;

    @BindView(R.id.addImageView)
    CircleImageView addImageView;

    public TabLayout tabLayout;
    ViewPager viewPager;
    ConstraintLayout emptyView;
    Button startButton;
    ImageView drawerButton;
    DrawerLayout drawerLayout;
    NavigationView navigationView;
    TextView profile;
    TextView help;
    TextView about;
    TextView notification;
    TextView terms, setting, logout;
    @BindView(R.id.userImageView)
    CircleImageView userImageView;
    @BindView(R.id.nameTextview)
    TextView nameTextview;
    @BindView(R.id.dateTextview)
    TextView dateTextview;
    @BindView(R.id.userNavImageView)
    CircleImageView userNavImageView;
    @BindView(R.id.nameNavTextview)
    TextView nameNavTextview;
    @BindView(R.id.dateNavTextview)
    TextView dateNavTextview;


    String[] counter = new String[]{"0", "0", "0", "0"};
    private HomeViewModel viewModel;
    public ViewPagerAdapter pagerAdapter;

    UserData user;
    private SessionManager sessionManager;


    @Override
    protected int getLayout() {
        return R.layout.activity_home;
    }

    @Override
    protected void onCreateActivityComponent() {
        OnTimeApplication.getComponent(this)
                .plus(new ActivityModule(this))
                .inject(this);
        viewModel = ViewModelProviders.of(this, provider).get(HomeViewModel.class);
        initHomeUi();


        initObservers();
    }

    private void initObservers() {
        viewModel.requestSuccess.observe(this, requestResponseModel -> {
            if (requestResponseModel.status == 0) {
                assert requestResponseModel != null;
                if (requestResponseModel.data.size() > 0)
                    emptyView.setVisibility(View.GONE);
            }

        });

    }

    private void initHomeUi() {

        emptyView = findViewById(R.id.emptyView);
        tabLayout = findViewById(R.id.tabLayout);
        viewPager = findViewById(R.id.viewPager);
        startButton = findViewById(R.id.startButton);
        drawerLayout = findViewById(R.id.drawerLayout);
        drawerButton = findViewById(R.id.drawerButton);
        notification = findViewById(R.id.notification);
        navigationView = findViewById(R.id.navigationView);
        profile = findViewById(R.id.profile);
        help = findViewById(R.id.help);
        about = findViewById(R.id.about);
        terms = findViewById(R.id.terms);
        setting = findViewById(R.id.setting);
        logout = findViewById(R.id.logout);

        sessionManager = new SessionManager(this);
        user = sessionManager.getUserDetails();
        Glide.with(this).load(Endpoints.IMAGE_BASE_URL + user.getImg()).placeholder(R.drawable.person_icon).into(userImageView);
        SimpleDateFormat formatter = new SimpleDateFormat("EEE, d MMM yyyy");
        Date date = new Date();
        nameTextview.setText(user.getName());
        dateTextview.setText(formatter.format(date));

        Glide.with(this).load(Endpoints.IMAGE_BASE_URL + user.getImg()).placeholder(R.drawable.person_icon).into(userNavImageView);
        nameNavTextview.setText(user.getName());
        dateNavTextview.setText(formatter.format(date));
        Fonty.setFonts(navigationView);
        drawerButton.setOnClickListener(v -> {

            if (drawerLayout.isDrawerOpen(navigationView)) {
                drawerLayout.closeDrawer(navigationView);
            } else {
                drawerLayout.openDrawer(navigationView);
            }
        });

        startButton.setOnClickListener(v -> {
            startActivity(new Intent(this, DepartmentsActivity.class));
            emptyView.setVisibility(View.GONE);
        });

        notification.setOnClickListener(v -> {
            startActivity(new Intent(this, NotificationActivity.class));

        });

        profile.setOnClickListener(v -> {
            startActivity(new Intent(this, ProfileActivity.class));
        });

        help.setOnClickListener(v -> {
            startActivity(new Intent(this, HelpCategoriesActivity.class));
        });
        about.setOnClickListener(v -> {
            startActivity(new Intent(this, AboutActivity.class));
        });
        terms.setOnClickListener(v -> {
            startActivity(new Intent(this, TermsActivity.class));
        });

        addImageView.setOnClickListener(v -> {
            startActivity(new Intent(this, DepartmentsActivity.class));
        });

        logout.setOnClickListener(v -> {
            new DialogBuilder(this, R.layout.custom_dialog_logout)
                    .clickListener(R.id.done, (dialog, view) -> {
                        dialog.dismiss();
                        sessionManager.logoutUser(LoginActivity.class);
                    })
                    .text(R.id.textView1, getString(R.string.logout_asking))
                    .gravity(Gravity.CENTER)
                    .build()
                    .show();
        });

        setting.setOnClickListener(v -> {
            intiateDialog();
        });
        tabLayout.setupWithViewPager(viewPager);
        viewModel.requestSuccess.observe(this, requestResponseModel -> {
            if (requestResponseModel.status == 0) {
                if (counter[0].equals(requestResponseModel.all))
                    return;
                counter[0] = requestResponseModel.all;
                counter[1] = requestResponseModel.under_preview;
                counter[2] = requestResponseModel.in_progress;
                counter[3] = requestResponseModel.finished;

            }
            setUpViewPager();

        });
        setUpViewPager();

        Fonty.setFonts(this);

    }

    private void setUpViewPager() {

        pagerAdapter = new ViewPagerAdapter(getSupportFragmentManager(), this, counter);
        viewPager.setAdapter(pagerAdapter);
        viewPager.setOffscreenPageLimit(4);
        for (int i = 0; i < tabLayout.getTabCount(); i++) {
            TabLayout.Tab tab = tabLayout.getTabAt(i);
            tab.setCustomView(pagerAdapter.getTabView(i));

        }

        viewPager.requestTransparentRegion(viewPager);
    }

    private void intiateDialog() {
        Typeface typeface = Utils.getTypeFace(this);
        Dialog dialog = new Dialog(this, R.style.Theme_AppCompat_DayNight_Dialog_Alert);
        dialog.setContentView(R.layout.dialog_choose_language);
        Button done = dialog.findViewById(R.id.btn_done);
        RadioButton radioButton_arabic = dialog.findViewById(R.id.radioButton_arabic);
        RadioButton radioButton_english = dialog.findViewById(R.id.radioButton_english);
        TextView title = dialog.findViewById(R.id.textView11);

        //done.setTypeface(Typeface.createFromAsset(getAssets(),"DIN Next LT W23 Regular.ttf"));
        RadioGroup radioGroup = (RadioGroup) dialog.findViewById(R.id.group_btn);

        done.setTypeface(typeface);
        radioButton_arabic.setTypeface(typeface);
        radioButton_english.setTypeface(typeface);
        title.setTypeface(typeface);

        dialog.show();
        done.setOnClickListener(v -> {

            // get selected radio button from radioGroup
            int selectedId = radioGroup.getCheckedRadioButtonId();

            // find the radiobutton by returned id
            RadioButton radioSexButton = (RadioButton) dialog.findViewById(selectedId);

            dialog.dismiss();
            if (radioSexButton.getText().equals(HomeActivity.this.getString(R.string.arabic))) {
                user.lang = "ar";
                sessionManager.createLoginSession(user);
            } else {
                user.lang = "en";
                sessionManager.createLoginSession(user);
            }
            Locale loc = new Locale(user.lang);
            Locale.setDefault(loc);
            Configuration config = new Configuration();
            config.locale = loc;
            getBaseContext().getResources().updateConfiguration(config,
                    getBaseContext().getResources().getDisplayMetrics());
            startActivity(new Intent(HomeActivity.this, HomeActivity.class));
            HomeActivity.this.finish();
        });
    }

}
