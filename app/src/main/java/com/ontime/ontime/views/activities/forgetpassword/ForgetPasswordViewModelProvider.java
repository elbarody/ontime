package com.ontime.ontime.views.activities.forgetpassword;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import androidx.annotation.NonNull;
import com.ontime.ontime.di.scope.ActivityScope;
import com.ontime.ontime.store.repo.AppRepo;

import javax.inject.Inject;

@ActivityScope
public class ForgetPasswordViewModelProvider extends ViewModelProvider.NewInstanceFactory{

    private final AppRepo repo;

    @Inject
    public ForgetPasswordViewModelProvider(AppRepo repo) {
        this.repo = repo;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        return (T) new ForgetPasswordViewModel(repo);
    }
}
