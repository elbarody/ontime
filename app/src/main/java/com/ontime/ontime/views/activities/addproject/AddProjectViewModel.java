package com.ontime.ontime.views.activities.addproject;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import com.jaiselrahman.filepicker.model.MediaFile;
import com.ontime.ontime.store.models.service.AddonModel;
import com.ontime.ontime.store.models.service.ServicesResponse;
import com.ontime.ontime.store.repo.AppRepo;

import java.util.ArrayList;
import java.util.List;

public class AddProjectViewModel extends ViewModel {

    public MutableLiveData<ServicesResponse> servicesLiveData;
    private final AppRepo repo;

    public AddProjectViewModel(AppRepo repo) {
        this.repo = repo;
        this.servicesLiveData = repo.servicesResponseLiveData;
    }

    public void getServices(int depId,int serviceId) {
        repo.getServices(depId,serviceId);
    }


    public void makeRequest(int depId, int serviceId, String name, String description, List<AddonModel> selectedAddons, ArrayList<MediaFile> files) {
        repo.makeRequest(depId,serviceId,name,description,selectedAddons,files);
    }

    @Override
    protected void onCleared() {
        repo.dispose();
        super.onCleared();
    }

}
