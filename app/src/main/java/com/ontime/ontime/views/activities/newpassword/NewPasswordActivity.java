package com.ontime.ontime.views.activities.newpassword;

import android.content.Intent;
import android.widget.EditText;
import androidx.lifecycle.ViewModelProviders;
import butterknife.BindView;
import butterknife.OnClick;
import com.ontime.ontime.OnTimeApplication;
import com.ontime.ontime.R;
import com.ontime.ontime.base.BaseActivity;
import com.ontime.ontime.di.activity.ActivityModule;
import com.ontime.ontime.di.scope.ActivityScope;
import com.ontime.ontime.views.activities.activation.ActivationActivity;
import com.ontime.ontime.views.activities.forgetpassword.ForgetPasswordViewModel;
import com.ontime.ontime.views.activities.forgetpassword.ForgetPasswordViewModelProvider;
import com.ontime.ontime.views.activities.home.HomeActivity;

import javax.inject.Inject;

@ActivityScope
public class NewPasswordActivity extends BaseActivity {

    @Inject
    NewPasswordViewModelProvider passwordViewModelProvider;

    @BindView(R.id.mail_et)
    EditText email;

    private NewPasswordViewModel viewModel;

    @Override
    protected int getLayout() {
        return R.layout.activity_forget_password;
    }

    @Override
    protected void onCreateActivityComponent() {
        OnTimeApplication.getComponent(this)
                .plus(new ActivityModule(this))
                .inject(this);

        viewModel = ViewModelProviders.of(this, passwordViewModelProvider).get(NewPasswordViewModel.class);

        initObservers();
    }

    void initObservers() {
        viewModel.NewPasswordSuccess.observe(this, aBoolean -> {
            if (aBoolean) {

                Intent intent = new Intent(this, HomeActivity.class);
                startActivity(intent);
            }
        });
    }


    @OnClick({R.id.button_send})
    void onSendClicked() {
        if (!com.ontime.ontime.utils.TextUtils.isValidPassword(email.getText().toString())) {
            showErrorMessage("Invalid Password ");
            return;
        }
        viewModel.newPassword(email.getText().toString());
    }



}
