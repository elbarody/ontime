package com.ontime.ontime.views.activities.addproject;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.afollestad.materialdialogs.MaterialDialog;
import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.jaiselrahman.filepicker.activity.FilePickerActivity;
import com.jaiselrahman.filepicker.config.Configurations;
import com.jaiselrahman.filepicker.model.MediaFile;
import com.ontime.ontime.OnTimeApplication;
import com.ontime.ontime.R;
import com.ontime.ontime.base.BaseActivity;
import com.ontime.ontime.di.activity.ActivityModule;
import com.ontime.ontime.di.scope.ActivityScope;
import com.ontime.ontime.store.models.service.AddonModel;
import com.ontime.ontime.views.activities.requestconfirmation.RequestConfirmationActivity;
import com.ontime.ontime.views.adapters.AddonsAdapter;
import com.ontime.ontime.views.adapters.ServiceToChooseRecyclerViewAdapter;
import com.ontime.ontime.views.adapters.ServicesAdapter;
import de.hdodenhof.circleimageview.CircleImageView;
import net.cachapa.expandablelayout.ExpandableLayout;

import javax.inject.Inject;
import java.util.ArrayList;

@ActivityScope
public class AddProjectActivity extends BaseActivity {
    private static final int FILE_REQUEST_CODE = 400;

    @Inject
    AddProjectViewModelProvider provider;
    @Inject
    AddonsAdapter addonsAdapter;
    @Inject
    ServiceToChooseRecyclerViewAdapter serviceToChooseRecyclerViewAdapter;

    @BindView(R.id.timeTextView)
    TextView timeTextView;
    @BindView(R.id.priceTextView)
    TextView priceTextView;
    @BindView(R.id.nextButton)
    Button nextButton;
    @BindView(R.id.servicesRecycler)
    RecyclerView servicesRecycler;
    @BindView(R.id.userImageView)
    CircleImageView selectedServiceImageView;
    @BindView(R.id.selectedServiceTextView)
    TextView selectedServiceTextView;
    @BindView(R.id.attachmentTextView)
    Button attachmentTextView;
    @BindView(R.id.nameEditText)
    EditText nameEditText;
    @BindView(R.id.descEditText)
    EditText descEditText;
    @BindView(R.id.recyclerView_services)
    RecyclerView recyclerViewServices;
    @BindView(R.id.service_container)
    ExpandableLayout serviceContainer;

    private AddProjectViewModel viewModel;
    private MaterialDialog materialDialog;
    private ArrayList<MediaFile> files = new ArrayList<>();
    private int totalPrice;

    @Override
    protected int getLayout() {
        return R.layout.activity_add_project;
    }

    @Override
    protected void onCreateActivityComponent() {

        OnTimeApplication.getComponent(this)
                .plus(new ActivityModule(this))
                .inject(this);
        viewModel = ViewModelProviders.of(this, provider).get(AddProjectViewModel.class);

        initUi();
        initObservers();

        if (getIntent() != null) {
            viewModel.getServices(getIntent().getIntExtra("depId", 0), 1);
            showErrorMessage(String.valueOf(getIntent().getIntExtra("depId", 0)));
        }

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            if (requestCode == FILE_REQUEST_CODE) {
                files = data.getParcelableArrayListExtra(FilePickerActivity.MEDIA_FILES);
                Log.d("minaLog", String.valueOf(files.size()));
            }
        }
    }

    private void initUi() {

        servicesRecycler.setLayoutManager(new GridLayoutManager(this, 2));
        servicesRecycler.setAdapter(addonsAdapter);

        recyclerViewServices.setLayoutManager(new LinearLayoutManager(this));
        recyclerViewServices.setAdapter(serviceToChooseRecyclerViewAdapter);

        addonsAdapter.setOnAddonsCheckedListener(selectedAddons -> {
            totalPrice = 0;

            if (viewModel.servicesLiveData.getValue().getData().getHasPrice() == 1) {
                totalPrice = totalPrice + viewModel.servicesLiveData.getValue().getData().getPrice();

                for (AddonModel model : selectedAddons) {
                    totalPrice = totalPrice + model.getPrice();
                }

                priceTextView.setText(String.valueOf(totalPrice));


            }


        });


        serviceToChooseRecyclerViewAdapter.setOnServiceClickedListener(servicesModel -> {
            selectedServiceTextView.setText(servicesModel.getName());
            Glide.with(this).load(servicesModel.getImg()).into(selectedServiceImageView);
            viewModel.getServices(getIntent().getIntExtra("depId", 0), servicesModel.getId());
            serviceContainer.collapse(true);
        });

        /*selectedServiceTextView.setOnClickListener(v -> {
            if (viewModel.servicesLiveData.getValue().getServices().size() != 0) {
                materialDialog = new MaterialDialog.Builder(this)
                        .adapter(new ServicesAdapter(this, viewModel.servicesLiveData.getValue().getServices(), servicesModel -> {

                            if (getIntent() != null) {
                                viewModel.getServices(getIntent().getIntExtra("depId", 0), servicesModel.getId());
                                materialDialog.dismiss();
                            }

                        }), new LinearLayoutManager(this))
                        .build();
                materialDialog.getRecyclerView().setBackground(getResources().getDrawable(R.drawable.shape_rounded_white_bg));
                materialDialog.show();

            }

        });*/


        attachmentTextView.setOnClickListener(v -> {
            Intent intent = new Intent(this, FilePickerActivity.class);
            intent.putExtra(FilePickerActivity.CONFIGS, new Configurations.Builder()
                    .setCheckPermission(true)
                    .setShowImages(true)
                    .setShowFiles(true)
                    .setShowVideos(true)
                    .enableImageCapture(true)
                    .setMaxSelection(-1)
                    .setSkipZeroSizeFiles(true)
                    .build());
            startActivityForResult(intent, FILE_REQUEST_CODE);

        });

        nextButton.setOnClickListener(v -> {

            String name = nameEditText.getText().toString();
            String description = descEditText.getText().toString();

            if (name.isEmpty()) {
                showErrorMessage(getString(R.string.type_project_name));
                return;
            }

            if (description.isEmpty()) {
                showErrorMessage(getString(R.string.type_project_description));
                return;
            }

            Intent intent = new Intent(this, RequestConfirmationActivity.class);
            Bundle bundle = new Bundle();
            bundle.putSerializable("servicesResponse", viewModel.servicesLiveData.getValue());
            bundle.putParcelableArrayList("files", files);
            bundle.putString("addons", new Gson().toJson(addonsAdapter.getSelectedAddons()));
            bundle.putString("name", nameEditText.getText().toString());
            bundle.putString("desc", descEditText.getText().toString());
            bundle.putInt("depId", getIntent().getIntExtra("depId", 0));

            intent.putExtras(bundle);
            startActivity(intent);

//            viewModel.makeRequest(getIntent().getIntExtra("depId", 0),
//                    viewModel.servicesLiveData.getValue().getData().getId(),
//                    name,
//                    description,
//                    addonsAdapter.getSelectedAddons(),
//                    files);

        });


    }

    private void initObservers() {

        viewModel.servicesLiveData.observe(this, servicesResponse -> {

            if (servicesResponse != null) {
                addonsAdapter.pushData(servicesResponse.getData().getAddons());
                serviceToChooseRecyclerViewAdapter.pushData(servicesResponse.getServices());
                selectedServiceTextView.setText(servicesResponse.getData().getName());
                Glide.with(this).load(servicesResponse.getData().getImg()).into(selectedServiceImageView);

                timeTextView.setText(" بعد اعتماد الطلب");

            }

            assert servicesResponse != null;
            if (servicesResponse.getData().getHasPrice() == 1) {
                priceTextView.setText(String.valueOf(servicesResponse.getData().getPrice()));
            } else {
                priceTextView.setText(" بعد اعتماد الطلب");
            }

            assert servicesResponse != null;
            if (servicesResponse.getData().getHasTime() == 1) {
                timeTextView.setText(String.valueOf(servicesResponse.getData()
                        .getAverageTime()).concat(" ساعات"));
            }
        });
    }


    @OnClick(R.id.projectsContainer)
    public void onViewClicked() {
        if (serviceContainer.isExpanded())
            serviceContainer.collapse(true);
        else
            serviceContainer.expand(true);
    }
}
