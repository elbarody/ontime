package com.ontime.ontime.views.activities.requestdetails.fragments;

import android.content.Intent;
import android.view.View;
import android.widget.TextView;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.OnClick;
import com.marcinorlowski.fonty.Fonty;
import com.ontime.ontime.R;
import com.ontime.ontime.base.BaseFragment;
import com.ontime.ontime.store.models.service.AddonModel;
import com.ontime.ontime.utils.DialogBuilder;
import com.ontime.ontime.views.activities.contractSignature.ContractSignatureActivity;
import com.ontime.ontime.views.activities.requestdetails.RequestDetailsViewModel;
import com.ontime.ontime.views.adapters.SelectedAddonsAdapter;

import java.util.ArrayList;

public class InfoFragment extends BaseFragment {


    @BindView(R.id.name_et)
    TextView nameEt;
    @BindView(R.id.project_description_et)
    TextView projectDescriptionEt;
    @BindView(R.id.service)
    TextView addonsTextview;
    @BindView(R.id.rv)
    RecyclerView servicesRecycler;
    private RequestDetailsViewModel viewModel;

    @Override
    protected int getLayout() {
        return R.layout.fragment_product_info;
    }

    @Override
    protected void onCreateFragmentComponent() {
        viewModel = ViewModelProviders.of(getActivity()).get(RequestDetailsViewModel.class);

        initObservers();
    }

    private void initObservers() {
        viewModel.requestDetailsLiveData.observe(this, requestDetailsData -> {
            if (requestDetailsData != null) {
                nameEt.setText(requestDetailsData.getRequestName());
                projectDescriptionEt.setText(requestDetailsData.getRequestDescr());

                if (requestDetailsData.getAddons().size() != 0) {
                    servicesRecycler.setLayoutManager(new LinearLayoutManager(getActivity()));
                    servicesRecycler.setAdapter(new SelectedAddonsAdapter(getActivity(), (ArrayList<AddonModel>) requestDetailsData.getAddons()));
                } else addonsTextview.setVisibility(View.GONE);
            }
        });
    }

    @OnClick(R.id.conditional)
    void OnTermsClicked() {
        new DialogBuilder(getActivity(), R.layout.dailog_terms)
                .text(R.id.conditional_content, viewModel.requestDetailsLiveData.getValue().getTerms())
                .clickListener(R.id.accept, ((dialog, view) -> dialog.dismiss()))
                .build()
                .show();
    }

    @Override
    public void finishScreen() {

    }
}
