package com.ontime.ontime.views.activities.activation;

import android.content.Intent;
import android.os.CountDownTimer;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.lifecycle.ViewModelProviders;
import butterknife.BindView;
import butterknife.OnClick;
import com.ontime.ontime.OnTimeApplication;
import com.ontime.ontime.R;
import com.ontime.ontime.base.BaseActivity;
import com.ontime.ontime.di.activity.ActivityModule;
import com.ontime.ontime.di.scope.ActivityScope;
import com.ontime.ontime.views.activities.home.HomeActivity;
import com.ontime.ontime.views.activities.newpassword.NewPasswordActivity;

import javax.inject.Inject;

@ActivityScope
public class ActivationActivity extends BaseActivity {

    private static final long TIME_OUT = 60000;
    private static final long INTERVAL = 1000;


    @Inject
    ActivationViewModelProvider provider;

    @BindView(R.id.imageView3)
    ImageView imageView3;
    @BindView(R.id.constraintLayout)
    ConstraintLayout constraintLayout;
    @BindView(R.id.textView)
    TextView textView;
    @BindView(R.id.mail_et)
    EditText codeEditText;
    @BindView(R.id.resend_code)
    TextView resendCode;
    @BindView(R.id.time)
    TextView time;
    @BindView(R.id.new_account)
    TextView codeResent;
    @BindView(R.id.button_send)
    Button buttonSend;

    private CountDownTimer countDownTimer;
    private ActivationViewModel viewModel;

    @Override
    protected int getLayout() {
        return R.layout.activity_send_code;
    }

    @Override
    protected void onCreateActivityComponent() {
        OnTimeApplication.getComponent(this)
                .plus(new ActivityModule(this))
                .inject(this);
        viewModel = ViewModelProviders.of(this, provider).get(ActivationViewModel.class);

        initUI();

        initObservers();
    }

    private void initObservers() {

        viewModel.isActivationSuccess.observe(this, state -> {
            if (state) {
                if (getIntent().getBooleanExtra("isFP", false)) {
                    startActivity(new Intent(this, NewPasswordActivity.class));
                    return;
                } else
                    this.finish();

            }

        });
    }

    private void initUI() {

        if (null != getIntent().getStringExtra("from")) {
            viewModel.resendCode();
        }
        countDownTimer = new CountDownTimer(TIME_OUT, INTERVAL) {
            @Override
            public void onTick(long millisUntilFinished) {
                time.setText("00:".concat(String.valueOf(millisUntilFinished / 1000)));
                resendCode.setVisibility(View.GONE);
            }

            @Override
            public void onFinish() {
                time.setText("00:00");
                resendCode.setVisibility(View.VISIBLE);
            }
        };
        countDownTimer.start();

    }

    @OnClick(R.id.resend_code)
    public void onResendClicked() {

        viewModel.resendCode();

    }

    @OnClick(R.id.button_send)
    public void onCheckCodeClicked() {
        if (getIntent().getBooleanExtra("isFP", false)) {
            viewModel.checkPCoded(codeEditText.getText().toString());
            // TODO remove it./
            startActivity(new Intent(this, NewPasswordActivity.class));
            return;
        }
        viewModel.checkCoded(codeEditText.getText().toString());

//        new DialogBuilder(this, R.layout.custom_dialog_resend_code)
//                .clickListener(R.id.button_send, ((dialog, view) -> dialog.dismiss()))
//                .gravity(Gravity.CENTER)
//                .build()
//                .show();

    }

}
