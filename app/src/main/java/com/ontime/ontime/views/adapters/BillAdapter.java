package com.ontime.ontime.views.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.marcinorlowski.fonty.Fonty;
import com.ontime.ontime.R;
import com.ontime.ontime.store.models.requestdetails.AttachmentsModel;
import com.ontime.ontime.store.models.requestdetails.ContractsModel;
import com.ontime.ontime.store.models.requestdetails.RequestCashModel;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class BillAdapter extends RecyclerView.Adapter<BillAdapter.ViewHolder> {


    private List<RequestCashModel> mData = new ArrayList<>();
    private LayoutInflater mInflater;
    private ItemClickListener mClickListener;

    public BillAdapter(Context context, ArrayList<RequestCashModel> data, ItemClickListener mClickListener) {
        this.mInflater = LayoutInflater.from(context);
        this.mData = data;
        this.mClickListener = mClickListener;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.item_project_details, parent, false);
        Fonty.setFonts((ViewGroup) view);
        return new ViewHolder(view);
    }


    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        RequestCashModel model = mData.get(position);
        holder.title.setText(model.getName());
        holder.size.setText(model.getFile_size());
        holder.date.setText(model.getCreated_at());
    }


    @Override
    public int getItemCount() {
        return mData.size();
    }


    public void pushData(List<RequestCashModel> models) {
        mData.clear();
        mData.addAll(models);
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        @BindView(R.id.share)
        ImageView share;
        @BindView(R.id.download)
        ImageView download;
        @BindView(R.id.title)
        TextView title;
        @BindView(R.id.date)
        TextView date;
        @BindView(R.id.size)
        TextView size;
        ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null)
                mClickListener.onItemClick(view, getItem(getAdapterPosition()));
        }
    }

    RequestCashModel getItem(int id) {
        return mData.get(id);
    }
    public interface ItemClickListener {
        void onItemClick(View view, RequestCashModel item);
    }

}
