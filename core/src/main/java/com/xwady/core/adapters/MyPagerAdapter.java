package com.xwady.core.adapters;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

public class MyPagerAdapter extends FragmentPagerAdapter {
    private Fragment[] mFragments;

    public MyPagerAdapter(FragmentManager fm, Fragment... fragments) {
        super(fm);
        this.mFragments = fragments;
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment = new Fragment();
        if (mFragments.length >= position) {
            fragment = mFragments[position];
//            Log.d("getItem: ",fragment.getTag());
        }

        return fragment;
    }

    @Override
    public int getCount() {
        return mFragments.length;
    }
}
