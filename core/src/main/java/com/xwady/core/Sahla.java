package com.xwady.core;

import android.content.Context;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.gsonparserfactory.GsonParserFactory;
import com.facebook.stetho.okhttp3.StethoInterceptor;

import okhttp3.OkHttpClient;

public class Sahla {
    private static Sahla ourInstance = null;

    public static Sahla getInstance(Context context) {
        if (ourInstance == null){
            ourInstance = new Sahla(context);
        }
        return ourInstance;
    }

    private Sahla(Context context) {
        OkHttpClient okHttpClient = new OkHttpClient() .newBuilder()
                .addNetworkInterceptor(new StethoInterceptor())
                .build();
        AndroidNetworking.initialize(context,okHttpClient);
        AndroidNetworking.setParserFactory(new GsonParserFactory());
    }
}
