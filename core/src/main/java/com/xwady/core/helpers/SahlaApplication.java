package com.xwady.core.helpers;

import android.app.Application;
import android.content.res.Configuration;
import androidx.appcompat.app.AppCompatDelegate;

import java.util.Locale;


public class SahlaApplication extends Application {

    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Locale loc = new Locale("ar");
        Locale.setDefault(loc);
        Configuration config = new Configuration();
        config.locale = loc;
        getBaseContext().getResources().updateConfiguration(config,
                getBaseContext().getResources().getDisplayMetrics());
    }


}
