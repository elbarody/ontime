package com.xwady.core.helpers;

import android.view.View;
import android.widget.CheckBox;

/**
 * Created by xwady on 10/24/2017.
 */

public final class CheckBoxCore {

    /**
     * RequiredCheckBox to check this CheckBox is Checked because this required field
     */
    public static void RequiredAllCheckBox(CheckBox... editTextCores) {
        for (CheckBox checkBoxes : editTextCores) {
            if (!checkBoxes.isChecked()) {
                checkBoxes.requestFocus();
                //checkBoxes.setTextColor(Color.RED);//just to highlight that this is an error
                checkBoxes.setError("");
            } else {
                checkBoxes.clearFocus();
                checkBoxes.setError(null);
            }

        }
    }

    public static boolean RequriedOneCheckBox(View view, String message , CheckBox... editTextCores ) {
        boolean check = false;
        for (CheckBox checkBoxes : editTextCores) {
            if (checkBoxes.isChecked()) {
                check = true;
                break;
            }
        }
        if (!check) {
            for (CheckBox checkBoxes : editTextCores) {
                checkBoxes.requestFocus();
                checkBoxes.setError(message);
                //EditTextCore.snackbar(view, message );
                break;
            }
        } else {
            for (CheckBox checkBoxes : editTextCores) {
                checkBoxes.clearFocus();
                checkBoxes.setError(null);
            }
        }
        return check;
    }


}
