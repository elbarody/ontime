package com.xwady.core.helpers;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.xwady.core.models.Driver;
import com.xwady.core.models.User;

public class SessionDriverManager {

    private static final String PREF_NAME = "driverapp";
    private static final String IS_FIRST_TIME_LAUNCH = "DriverIsFirstTimeLaunch";
    private SharedPreferences pref;
    private SharedPreferences.Editor editor;
    private Context _context;

    public SessionDriverManager(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        editor = pref.edit();
    }

    public boolean isFirstTimeLaunch() {
        return pref.getBoolean(IS_FIRST_TIME_LAUNCH, true);
    }

    public void setFirstTimeLaunch(boolean isFirstTime) {
        editor.putBoolean(IS_FIRST_TIME_LAUNCH, isFirstTime);
        editor.commit();
    }

    /**
     * Create login session
     */

    public void createLoginSession(Driver driver) {
        Gson gson = new Gson();
        String json = gson.toJson(driver);
        editor.putString("driver", json);
        editor.commit();
    }

    /**
     * Check login method wil check user login status
     * If false it will redirect user to welcomeActivity page
     * Else won't do anything
     */
    public void checkLogin(Class<?> welcomeActivity) {
        // Check login status
        if (!this.isLoggedIn()) {
            // user is not logged in redirect him to Login Activity
            Intent i = new Intent(_context, welcomeActivity);
            // Closing all the Activities
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            // Add new Flag to start new Activity
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            // Staring Login Activity
            _context.startActivity(i);
        }
    }

    /**
     * Get stored session data
     */

    public Driver getDriverDetails() {
        Gson gson = new Gson();
        String json = pref.getString("driver", "");
        return gson.fromJson(json, Driver.class);
    }


    /**
     * Clear session details
     */
    public void logoutDriver(Class<?> welcomeActivity) {
        // Clearing all data from Shared Preferences
        editor.clear();
        editor.commit();

        // After logout redirect user to Loing Activity
        Intent i = new Intent(_context, welcomeActivity);
        // Closing all the Activities
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        // Add new Flag to start new Activity
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        // Staring Login Activity
        _context.startActivity(i);
    }

    /**
     * Quick check for login
     **/
    // Get Login State
    public boolean isLoggedIn() {
        return getDriverDetails().getLoginStatus().equals(Driver.AccountStatus.LOGGED_IN_CLIENT) || getDriverDetails().getLoginStatus().equals(User.AccountStatus.LOGGED_IN_PROVIDER);
    }
}
